.class public final Lcom/google/android/youtube/app/ui/dh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/youtube/core/Analytics;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/ImageView;

.field private final g:Lcom/google/android/youtube/app/ui/dq;

.field private final h:Ljava/util/List;

.field private final i:Lcom/google/android/youtube/core/client/bc;

.field private final j:Lcom/google/android/youtube/core/async/au;

.field private final k:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final l:Lcom/google/android/youtube/core/async/by;

.field private final m:Lcom/google/android/youtube/app/k;

.field private final n:Lcom/google/android/youtube/core/e;

.field private final o:Lcom/google/android/youtube/app/ui/dr;

.field private final p:Lcom/google/android/youtube/app/ui/ac;

.field private final q:Lcom/google/android/youtube/app/ui/do;

.field private final r:Lcom/google/android/youtube/app/ui/du;

.field private s:Lcom/google/android/youtube/core/model/Video;

.field private t:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/async/by;Lcom/google/android/youtube/app/k;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/ui/dq;Lcom/google/android/youtube/core/e;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "activity can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    const-string v0, "analytics can not be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->b:Lcom/google/android/youtube/core/Analytics;

    const-string v0, "userAuthorizer can not be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/UserAuthorizer;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->k:Lcom/google/android/youtube/core/async/UserAuthorizer;

    const-string v0, "youTubeAuthorizer can not be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/by;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->l:Lcom/google/android/youtube/core/async/by;

    const-string v0, "config can not be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/k;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->m:Lcom/google/android/youtube/app/k;

    const-string v0, "likeDislikeCountTracker can not be null"

    invoke-static {p7, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/dq;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->g:Lcom/google/android/youtube/app/ui/dq;

    const-string v0, "gdataClient can not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->i:Lcom/google/android/youtube/core/client/bc;

    const-string v0, "errorHelper can not be null"

    invoke-static {p8, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/e;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->n:Lcom/google/android/youtube/core/e;

    invoke-interface {p2}, Lcom/google/android/youtube/core/client/bc;->o()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->j:Lcom/google/android/youtube/core/async/au;

    new-instance v0, Lcom/google/android/youtube/app/ui/dr;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/ui/dr;-><init>(Lcom/google/android/youtube/app/ui/dh;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->o:Lcom/google/android/youtube/app/ui/dr;

    new-instance v0, Lcom/google/android/youtube/app/ui/ac;

    invoke-direct {v0, p1, p3, p2, p8}, Lcom/google/android/youtube/app/ui/ac;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->p:Lcom/google/android/youtube/app/ui/ac;

    new-instance v0, Lcom/google/android/youtube/app/ui/do;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/ui/do;-><init>(Lcom/google/android/youtube/app/ui/dh;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->q:Lcom/google/android/youtube/app/ui/do;

    new-instance v0, Lcom/google/android/youtube/app/ui/du;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/du;-><init>(Lcom/google/android/youtube/app/ui/dh;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->r:Lcom/google/android/youtube/app/ui/du;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->t:Landroid/net/Uri;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->h:Ljava/util/List;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/async/UserAuthorizer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->k:Lcom/google/android/youtube/core/async/UserAuthorizer;

    return-object v0
.end method

.method private a(Landroid/net/Uri;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/dh;->t:Landroid/net/Uri;

    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/dh;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0b01e3

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dh;->e:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dh;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/app/ui/dh;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f020211

    :goto_1
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dh;->f:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dh;->f:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    :cond_1
    return-void

    :cond_2
    const v0, 0x7f0b01e2

    goto :goto_0

    :cond_3
    const v0, 0x7f02009b

    goto :goto_1
.end method

.method private static a(Landroid/view/View;Z)V
    .locals 1

    if-eqz p0, :cond_0

    invoke-virtual {p0, p1}, Landroid/view/View;->setSelected(Z)V

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setClickable(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/dh;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/ui/dh;->b(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/dh;Landroid/net/Uri;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/ui/dh;->a(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/dh;Landroid/view/View;Z)V
    .locals 0

    invoke-static {p1, p2}, Lcom/google/android/youtube/app/ui/dh;->a(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/dh;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lcom/google/android/youtube/core/utils/Util;->b(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    return-void
.end method

.method public static a(Lcom/google/android/youtube/core/model/Video;)Z
    .locals 2

    const-string v0, "video can\'t be null"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/ui/dj;->a:[I

    iget-object v1, p0, Lcom/google/android/youtube/core/model/Video;->state:Lcom/google/android/youtube/core/model/Video$State;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/model/Video$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/youtube/core/model/Video;->isLive()Z

    move-result v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic b(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->n:Lcom/google/android/youtube/core/e;

    return-object v0
.end method

.method private b(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;II)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/youtube/app/ui/dh;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->c:Landroid/view/View;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/ui/dh;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->d:Landroid/view/View;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->i:Lcom/google/android/youtube/core/client/bc;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/app/ui/dq;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->g:Lcom/google/android/youtube/app/ui/dq;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/model/Video;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->s:Lcom/google/android/youtube/core/model/Video;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->b:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method private h()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->t:Landroid/net/Uri;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic i(Lcom/google/android/youtube/app/ui/dh;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->h:Ljava/util/List;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/app/k;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->m:Lcom/google/android/youtube/app/k;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/async/by;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->l:Lcom/google/android/youtube/core/async/by;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/async/au;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->j:Lcom/google/android/youtube/core/async/au;

    return-object v0
.end method


# virtual methods
.method public final a(I)Landroid/app/Dialog;
    .locals 2

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :sswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->o:Lcom/google/android/youtube/app/ui/dr;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/dr;->a()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->p:Lcom/google/android/youtube/app/ui/ac;

    new-instance v1, Lcom/google/android/youtube/app/ui/di;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/ui/di;-><init>(Lcom/google/android/youtube/app/ui/dh;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/ac;->a(Lcom/google/android/youtube/app/ui/af;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->q:Lcom/google/android/youtube/app/ui/do;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/do;->a()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->r:Lcom/google/android/youtube/app/ui/du;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/du;->a()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x3e8 -> :sswitch_3
        0x3ed -> :sswitch_1
        0x3f4 -> :sswitch_0
        0x3ff -> :sswitch_2
    .end sparse-switch
.end method

.method public final a()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->b:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "Like"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->k:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/youtube/app/ui/dy;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/dh;->s:Lcom/google/android/youtube/core/model/Video;

    invoke-direct {v2, p0, v3, v4}, Lcom/google/android/youtube/app/ui/dy;-><init>(Lcom/google/android/youtube/app/ui/dh;ZLcom/google/android/youtube/core/model/Video;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;)V

    return-void
.end method

.method public final a(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->c:Landroid/view/View;

    invoke-static {p2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->d:Landroid/view/View;

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/ui/dn;)V
    .locals 1

    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;Landroid/net/Uri;)V
    .locals 2

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/dh;->s:Lcom/google/android/youtube/core/model/Video;

    invoke-direct {p0, p2}, Lcom/google/android/youtube/app/ui/dh;->a(Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->c:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/ui/dh;->a(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->d:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/ui/dh;->a(Landroid/view/View;Z)V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 5

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->b:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "Dislike"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->k:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    new-instance v2, Lcom/google/android/youtube/app/ui/dy;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/dh;->s:Lcom/google/android/youtube/core/model/Video;

    invoke-direct {v2, p0, v3, v4}, Lcom/google/android/youtube/app/ui/dy;-><init>(Lcom/google/android/youtube/app/ui/dh;ZLcom/google/android/youtube/core/model/Video;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;)V

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->b:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "Share"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dh;->s:Lcom/google/android/youtube/core/model/Video;

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/m;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/model/Video;)V

    return-void
.end method

.method public final d()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->b:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "CopyURL"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dh;->s:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->watchUri:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/m;->a(Landroid/content/Context;Ljava/lang/String;)V

    const v0, 0x7f0b00ca

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/dh;->b(I)V

    return-void
.end method

.method public final e()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->b:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "Flag"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->k:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/dh;->q:Lcom/google/android/youtube/app/ui/do;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;)V

    return-void
.end method

.method public final f()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->k:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/dh;->o:Lcom/google/android/youtube/app/ui/dr;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;)V

    return-void
.end method

.method public final g()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dh;->r:Lcom/google/android/youtube/app/ui/du;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/du;->c()V

    return-void
.end method
