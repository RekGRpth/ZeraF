.class public final Lcom/google/android/youtube/app/ui/as;
.super Lcom/google/android/youtube/core/a/l;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/app/ui/b;

.field private final c:Lcom/google/android/youtube/app/ui/aj;

.field private final d:Lcom/google/android/youtube/app/ui/es;

.field private final e:Lcom/google/android/youtube/app/ui/eg;

.field private final f:Lcom/google/android/youtube/app/ui/ej;

.field private final g:Lcom/google/android/youtube/app/ui/aw;


# direct methods
.method private constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/ui/ep;Lcom/google/android/youtube/app/ui/b;Lcom/google/android/youtube/app/ui/aj;Lcom/google/android/youtube/app/ui/es;Lcom/google/android/youtube/app/ui/ej;Lcom/google/android/youtube/app/ui/ef;Lcom/google/android/youtube/app/ui/eg;Lcom/google/android/youtube/app/ui/eq;)V
    .locals 8

    const/4 v1, 0x7

    new-array v1, v1, [Lcom/google/android/youtube/core/a/e;

    const/4 v2, 0x0

    aput-object p4, v1, v2

    const/4 v2, 0x1

    aput-object p5, v1, v2

    const/4 v2, 0x2

    aput-object p9, v1, v2

    const/4 v2, 0x3

    aput-object p10, v1, v2

    const/4 v2, 0x4

    aput-object p6, v1, v2

    const/4 v2, 0x5

    aput-object p7, v1, v2

    const/4 v2, 0x6

    aput-object p8, v1, v2

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    const-string v1, "brandingOutline cannot be null"

    invoke-static {p4, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/app/ui/b;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/as;->a:Lcom/google/android/youtube/app/ui/b;

    const-string v1, "descriptionOutline cannot be null"

    invoke-static {p5, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/app/ui/aj;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/as;->c:Lcom/google/android/youtube/app/ui/aj;

    const-string v1, "relatedVideosOutline cannot be null"

    invoke-static {p6, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/app/ui/es;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/as;->d:Lcom/google/android/youtube/app/ui/es;

    const-string v1, "artistTracksOutline cannot be null"

    move-object/from16 v0, p9

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/app/ui/eg;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/as;->e:Lcom/google/android/youtube/app/ui/eg;

    const-string v1, "commentsOutline cannot be null"

    invoke-static {p7, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/app/ui/ej;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/as;->f:Lcom/google/android/youtube/app/ui/ej;

    new-instance v1, Lcom/google/android/youtube/app/ui/aw;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p8

    move-object/from16 v6, p9

    move-object/from16 v7, p10

    invoke-direct/range {v1 .. v7}, Lcom/google/android/youtube/app/ui/aw;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/ui/ep;Lcom/google/android/youtube/app/ui/ef;Lcom/google/android/youtube/app/ui/eg;Lcom/google/android/youtube/app/ui/eq;)V

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/as;->g:Lcom/google/android/youtube/app/ui/aw;

    const/4 v1, 0x0

    invoke-virtual {p5, v1}, Lcom/google/android/youtube/app/ui/aj;->a(Z)V

    const/4 v1, 0x1

    invoke-virtual {p6, v1}, Lcom/google/android/youtube/app/ui/es;->a(Z)V

    const/4 v1, 0x1

    invoke-virtual {p7, v1}, Lcom/google/android/youtube/app/ui/ej;->a(Z)V

    const/4 v1, 0x0

    move-object/from16 v0, p8

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/ef;->a(Z)V

    const/4 v1, 0x1

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/eg;->a(Z)V

    const/4 v1, 0x1

    move-object/from16 v0, p10

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/eq;->a(Z)V

    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/app/ui/dh;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/async/by;Lcom/google/android/youtube/app/k;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/ui/ep;)Lcom/google/android/youtube/app/ui/as;
    .locals 22

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0018

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v11

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0019

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v12

    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0017

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v13

    invoke-static/range {p0 .. p1}, Lcom/google/android/youtube/app/ui/b;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;)Lcom/google/android/youtube/app/ui/b;

    move-result-object v17

    const/4 v1, 0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/b;->a(Z)V

    invoke-static/range {p0 .. p0}, Lcom/google/android/youtube/app/ui/aj;->a(Landroid/app/Activity;)Lcom/google/android/youtube/app/ui/aj;

    move-result-object v18

    invoke-interface/range {p3 .. p3}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v8

    sget-object v14, Lcom/google/android/youtube/app/ui/ee;->b:Lcom/google/android/youtube/core/a/g;

    sget-object v15, Lcom/google/android/youtube/app/ui/ee;->a:Lcom/google/android/youtube/core/a/g;

    sget-object v16, Lcom/google/android/youtube/app/ui/ee;->c:Lcom/google/android/youtube/core/a/g;

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p3

    move-object/from16 v4, p2

    move-object/from16 v5, p10

    move-object/from16 v6, p11

    move-object/from16 v7, p7

    move-object/from16 v9, p5

    move-object/from16 v10, p4

    invoke-static/range {v1 .. v16}, Lcom/google/android/youtube/app/ui/es;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/async/GDataRequestFactory;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;)Lcom/google/android/youtube/app/ui/es;

    move-result-object v16

    sget-object v14, Lcom/google/android/youtube/app/ui/ee;->d:Lcom/google/android/youtube/core/a/g;

    sget-object v15, Lcom/google/android/youtube/app/ui/ee;->a:Lcom/google/android/youtube/core/a/g;

    move-object/from16 v7, p0

    move-object/from16 v8, p3

    move-object/from16 v9, p11

    move-object/from16 v10, p6

    invoke-static/range {v7 .. v15}, Lcom/google/android/youtube/app/ui/ej;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/ui/dh;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;)Lcom/google/android/youtube/app/ui/ej;

    move-result-object v19

    sget-object v1, Lcom/google/android/youtube/app/ui/ee;->a:Lcom/google/android/youtube/core/a/g;

    move-object/from16 v0, p0

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/ui/ef;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/a/g;)Lcom/google/android/youtube/app/ui/ef;

    move-result-object v20

    sget-object v14, Lcom/google/android/youtube/app/ui/ee;->a:Lcom/google/android/youtube/core/a/g;

    sget-object v15, Lcom/google/android/youtube/app/ui/ee;->e:Lcom/google/android/youtube/core/a/g;

    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-object/from16 v8, p3

    move-object/from16 v9, p5

    move-object/from16 v10, p4

    invoke-static/range {v6 .. v15}, Lcom/google/android/youtube/app/ui/eg;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;)Lcom/google/android/youtube/app/ui/eg;

    move-result-object v21

    sget-object v14, Lcom/google/android/youtube/app/ui/ee;->a:Lcom/google/android/youtube/core/a/g;

    sget-object v15, Lcom/google/android/youtube/app/ui/ee;->f:Lcom/google/android/youtube/core/a/g;

    move-object/from16 v8, p0

    move-object/from16 v9, p5

    move-object/from16 v10, p4

    invoke-static/range {v8 .. v15}, Lcom/google/android/youtube/app/ui/eq;->a(Landroid/app/Activity;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;)Lcom/google/android/youtube/app/ui/eq;

    move-result-object v11

    new-instance v1, Lcom/google/android/youtube/app/ui/as;

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object/from16 v4, p12

    move-object/from16 v5, v17

    move-object/from16 v6, v18

    move-object/from16 v7, v16

    move-object/from16 v8, v19

    move-object/from16 v9, v20

    move-object/from16 v10, v21

    invoke-direct/range {v1 .. v11}, Lcom/google/android/youtube/app/ui/as;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/ui/ep;Lcom/google/android/youtube/app/ui/b;Lcom/google/android/youtube/app/ui/aj;Lcom/google/android/youtube/app/ui/es;Lcom/google/android/youtube/app/ui/ej;Lcom/google/android/youtube/app/ui/ef;Lcom/google/android/youtube/app/ui/eg;Lcom/google/android/youtube/app/ui/eq;)V

    return-object v1
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/Branding;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/as;->a:Lcom/google/android/youtube/app/ui/b;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/b;->a(Lcom/google/android/youtube/core/model/Branding;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/as;->d:Lcom/google/android/youtube/app/ui/es;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/es;->a(Lcom/google/android/youtube/core/model/Branding;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .locals 2

    const-string v0, "video cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/as;->c:Lcom/google/android/youtube/app/ui/aj;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->description:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/aj;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/as;->d:Lcom/google/android/youtube/app/ui/es;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/es;->a(Lcom/google/android/youtube/core/model/Video;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/as;->f:Lcom/google/android/youtube/app/ui/ej;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/ej;->a(Lcom/google/android/youtube/core/model/Video;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/as;->g:Lcom/google/android/youtube/app/ui/aw;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/aw;->a(Lcom/google/android/youtube/core/model/Video;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/as;->a:Lcom/google/android/youtube/app/ui/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/b;->c(Z)V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/as;->d:Lcom/google/android/youtube/app/ui/es;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/es;->i()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/as;->f:Lcom/google/android/youtube/app/ui/ej;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ej;->j()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/as;->g:Lcom/google/android/youtube/app/ui/aw;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/aw;->a()V

    return-void
.end method

.method public final r_()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/as;->d:Lcom/google/android/youtube/app/ui/es;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/es;->j()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/as;->e:Lcom/google/android/youtube/app/ui/eg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/eg;->b()V

    return-void
.end method
