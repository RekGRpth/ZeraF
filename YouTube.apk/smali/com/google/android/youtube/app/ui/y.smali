.class final Lcom/google/android/youtube/app/ui/y;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/v;

.field private final b:Landroid/util/SparseArray;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/ui/v;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/y;->a:Lcom/google/android/youtube/app/ui/v;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/y;->b:Landroid/util/SparseArray;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 4

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/y;->getCount()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/y;->b:Landroid/util/SparseArray;

    new-instance v2, Lcom/google/android/youtube/app/ui/z;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-direct {v2, p0, p1, p2, v3}, Lcom/google/android/youtube/app/ui/z;-><init>(Lcom/google/android/youtube/app/ui/y;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v1, v0, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/y;->notifyDataSetChanged()V

    return v0
.end method

.method public final getCount()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/y;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/y;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    const/16 v6, 0x8

    const/4 v5, 0x0

    const/4 v2, 0x0

    if-nez p2, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/y;->a:Lcom/google/android/youtube/app/ui/v;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/v;->d(Lcom/google/android/youtube/app/ui/v;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04002d

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/google/android/youtube/app/ui/aa;

    invoke-direct {v0, p0, p2}, Lcom/google/android/youtube/app/ui/aa;-><init>(Lcom/google/android/youtube/app/ui/y;Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/y;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/z;

    if-nez v0, :cond_2

    move-object p2, v2

    :cond_0
    :goto_1
    return-object p2

    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/aa;

    move-object v1, v0

    goto :goto_0

    :cond_2
    iget-object v3, v1, Lcom/google/android/youtube/app/ui/aa;->a:Landroid/widget/TextView;

    if-eqz v3, :cond_3

    iget-object v3, v0, Lcom/google/android/youtube/app/ui/z;->a:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v3, v1, Lcom/google/android/youtube/app/ui/aa;->a:Landroid/widget/TextView;

    iget-object v4, v0, Lcom/google/android/youtube/app/ui/z;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, v1, Lcom/google/android/youtube/app/ui/aa;->a:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_3
    :goto_2
    iget-object v3, v1, Lcom/google/android/youtube/app/ui/aa;->b:Landroid/widget/TextView;

    if-eqz v3, :cond_4

    iget-object v3, v0, Lcom/google/android/youtube/app/ui/z;->b:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, v1, Lcom/google/android/youtube/app/ui/aa;->b:Landroid/widget/TextView;

    iget-object v4, v0, Lcom/google/android/youtube/app/ui/z;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, v1, Lcom/google/android/youtube/app/ui/aa;->b:Landroid/widget/TextView;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_4
    :goto_3
    iget-object v3, v1, Lcom/google/android/youtube/app/ui/aa;->c:Landroid/widget/ImageView;

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/google/android/youtube/app/ui/z;->c:Ljava/lang/Integer;

    if-eqz v3, :cond_7

    iget-object v2, v1, Lcom/google/android/youtube/app/ui/aa;->c:Landroid/widget/ImageView;

    iget-object v0, v0, Lcom/google/android/youtube/app/ui/z;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, v1, Lcom/google/android/youtube/app/ui/aa;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :cond_5
    iget-object v3, v1, Lcom/google/android/youtube/app/ui/aa;->a:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, v1, Lcom/google/android/youtube/app/ui/aa;->a:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :cond_6
    iget-object v3, v1, Lcom/google/android/youtube/app/ui/aa;->b:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, v1, Lcom/google/android/youtube/app/ui/aa;->b:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_3

    :cond_7
    iget-object v0, v1, Lcom/google/android/youtube/app/ui/aa;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, v1, Lcom/google/android/youtube/app/ui/aa;->c:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method
