.class public final Lcom/google/android/youtube/app/ui/bz;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/youtube/app/remote/bq;

.field private final c:Lcom/google/android/youtube/app/remote/br;

.field private final d:Lcom/google/android/youtube/core/async/h;

.field private final e:Landroid/content/res/Resources;

.field private final f:Lcom/google/android/youtube/core/e;

.field private final g:Lcom/google/android/youtube/core/Analytics;

.field private final h:Landroid/widget/Button;

.field private final i:Landroid/widget/EditText;

.field private final j:Landroid/widget/ImageView;

.field private final k:Landroid/widget/ProgressBar;

.field private final l:Landroid/view/View;

.field private m:Ljava/lang/String;

.field private n:J

.field private o:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/app/remote/ad;Lcom/google/android/youtube/app/remote/bq;Lcom/google/android/youtube/app/remote/br;Lcom/google/android/youtube/app/remote/bf;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/ui/cj;Lcom/google/android/youtube/core/e;I)V
    .locals 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "actvity can not be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->a:Landroid/app/Activity;

    const-string v0, "screensClient can not be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/bq;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->b:Lcom/google/android/youtube/app/remote/bq;

    const-string v0, "screensMonitor cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/br;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->c:Lcom/google/android/youtube/app/remote/br;

    const-string v0, "remoteControl can not be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "listener can not be null"

    invoke-static {p7, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "analytics can not be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->g:Lcom/google/android/youtube/core/Analytics;

    const-string v0, "errorHelper can not be null"

    invoke-static {p8, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/e;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->f:Lcom/google/android/youtube/core/e;

    new-instance v0, Lcom/google/android/youtube/app/ui/ca;

    move-object v1, p0

    move-object v2, p6

    move-object v3, p2

    move-object v4, p7

    move-object v5, p1

    move-object v6, p8

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/ui/ca;-><init>(Lcom/google/android/youtube/app/ui/bz;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/remote/ad;Lcom/google/android/youtube/app/ui/cj;Landroid/app/Activity;Lcom/google/android/youtube/core/e;)V

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->d:Lcom/google/android/youtube/core/async/h;

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->e:Landroid/content/res/Resources;

    const v0, 0x7f040086

    invoke-virtual {p1, v0}, Landroid/app/Activity;->setContentView(I)V

    const v0, 0x7f070131

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->i:Landroid/widget/EditText;

    const v0, 0x7f07012e

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->l:Landroid/view/View;

    invoke-virtual {p1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.camera"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->l:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    const v0, 0x7f07012f

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->j:Landroid/widget/ImageView;

    const v0, 0x7f070130

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->k:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->i:Landroid/widget/EditText;

    new-instance v1, Lcom/google/android/youtube/app/ui/cb;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/ui/cb;-><init>(Lcom/google/android/youtube/app/ui/bz;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const v0, 0x7f07009d

    invoke-virtual {p1, v0}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->h:Landroid/widget/Button;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->h:Landroid/widget/Button;

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/Util;->g(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->i:Landroid/widget/EditText;

    new-instance v1, Lcom/google/android/youtube/app/ui/ci;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/youtube/app/ui/ci;-><init>(Lcom/google/android/youtube/app/ui/bz;B)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->i:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bz;->e:Landroid/content/res/Resources;

    const v2, 0x7f0b022b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/widget/EditText;->setImeActionLabel(Ljava/lang/CharSequence;I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->i:Landroid/widget/EditText;

    new-instance v1, Lcom/google/android/youtube/app/ui/cc;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/ui/cc;-><init>(Lcom/google/android/youtube/app/ui/bz;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->h:Landroid/widget/Button;

    new-instance v1, Lcom/google/android/youtube/app/ui/cd;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/ui/cd;-><init>(Lcom/google/android/youtube/app/ui/bz;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->j:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/youtube/app/ui/ce;

    invoke-direct {v1, p0, p1}, Lcom/google/android/youtube/app/ui/ce;-><init>(Lcom/google/android/youtube/app/ui/bz;Landroid/app/Activity;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/bz;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->i:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/bz;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->o:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/ui/bz;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->l:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/ui/bz;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    div-int/lit8 v3, v2, 0x3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    mul-int/lit8 v4, v0, 0x3

    mul-int/lit8 v5, v0, 0x3

    add-int/lit8 v5, v5, 0x3

    invoke-virtual {v1, p1, v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    rem-int/lit8 v0, v2, 0x3

    if-nez v0, :cond_1

    const/16 v3, 0x9

    if-lt v2, v3, :cond_1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->deleteCharAt(I)Ljava/lang/StringBuilder;

    :goto_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    sub-int v0, v2, v0

    invoke-virtual {v1, p1, v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    :try_start_0
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "pairingCode"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/net/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    if-eqz v0, :cond_0

    move-object p0, v0

    :cond_0
    :goto_0
    return-object p0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/ui/bz;)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->f:Lcom/google/android/youtube/core/e;

    const v1, 0x7f0b024b

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/e;->a(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bz;->g:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "RemoteAddScreenPressed"

    invoke-interface {v1, v2}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bz;->b:Lcom/google/android/youtube/app/remote/bq;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bz;->a:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/youtube/app/ui/ch;

    const-string v4, ""

    const/4 v5, 0x0

    invoke-direct {v3, p0, v4, v0, v5}, Lcom/google/android/youtube/app/ui/ch;-><init>(Lcom/google/android/youtube/app/ui/bz;Ljava/lang/String;Ljava/lang/String;B)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/youtube/app/remote/bq;->b(Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/ui/bz;)Landroid/widget/ProgressBar;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->k:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/ui/bz;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->j:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/ui/bz;)Lcom/google/android/youtube/core/async/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->d:Lcom/google/android/youtube/core/async/h;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/ui/bz;)Lcom/google/android/youtube/app/remote/br;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->c:Lcom/google/android/youtube/app/remote/br;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/ui/bz;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->a:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public final a(I)Landroid/app/Dialog;
    .locals 6

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bz;->a:Landroid/app/Activity;

    const v2, 0x7f0b0250

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, ""

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/core/ui/w;

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/bz;->a:Landroid/app/Activity;

    invoke-direct {v2, v3}, Lcom/google/android/youtube/core/ui/w;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0b022b

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/ui/w;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0b0251

    new-instance v3, Lcom/google/android/youtube/app/ui/cg;

    invoke-direct {v3, p0, v0}, Lcom/google/android/youtube/app/ui/cg;-><init>(Lcom/google/android/youtube/app/ui/bz;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0b0017

    new-instance v2, Lcom/google/android/youtube/app/ui/cf;

    invoke-direct {v2, p0}, Lcom/google/android/youtube/app/ui/cf;-><init>(Lcom/google/android/youtube/app/ui/bz;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x403
        :pswitch_0
    .end packed-switch
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->i:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method public final a(J)V
    .locals 0

    iput-wide p1, p0, Lcom/google/android/youtube/app/ui/bz;->n:J

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/bz;->m:Ljava/lang/String;

    return-void
.end method

.method public final a(IILandroid/content/Intent;)Z
    .locals 3

    const/4 v0, 0x0

    const/16 v1, 0x6b6

    if-ne p1, v1, :cond_1

    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    const-string v1, "SCAN_RESULT"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bz;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/bz;->o:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bz;->i:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/bz;->o:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bz;->k:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bz;->j:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->k:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->j:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/bz;->o:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bz;->i:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bz;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
