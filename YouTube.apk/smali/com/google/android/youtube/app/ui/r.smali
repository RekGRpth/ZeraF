.class final Lcom/google/android/youtube/app/ui/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/ui/s;


# instance fields
.field private final a:Lcom/google/android/youtube/app/d;

.field private final b:Lcom/google/android/youtube/core/Analytics;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/r;->a:Lcom/google/android/youtube/app/d;

    iput-object p2, p0, Lcom/google/android/youtube/app/ui/r;->b:Lcom/google/android/youtube/core/Analytics;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/Channel;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/r;->b:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "OpenChannelFromSearchResults"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/r;->a:Lcom/google/android/youtube/app/d;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Channel;->userProfileUri:Landroid/net/Uri;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/d;->a(Landroid/net/Uri;)V

    return-void
.end method
