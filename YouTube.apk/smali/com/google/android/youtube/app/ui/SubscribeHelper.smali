.class public final Lcom/google/android/youtube/app/ui/SubscribeHelper;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/youtube/app/YouTubeApplication;

.field private final c:Lcom/google/android/youtube/core/Analytics;

.field private final d:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final e:Lcom/google/android/youtube/core/client/bc;

.field private final f:Lcom/google/android/youtube/core/e;

.field private final g:Lcom/google/android/youtube/app/ui/cq;

.field private final h:Lcom/google/android/youtube/core/async/bk;

.field private final i:Lcom/google/android/youtube/core/async/n;

.field private final j:Lcom/google/android/youtube/core/async/n;

.field private k:Lcom/google/android/youtube/core/model/UserProfile;

.field private l:Lcom/google/android/youtube/core/model/Channel;

.field private m:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

.field private n:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

.field private o:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

.field private p:Lcom/google/android/youtube/core/model/Subscription;

.field private q:Lcom/google/android/youtube/app/ui/cr;

.field private r:Landroid/net/Uri;

.field private s:Lcom/google/android/youtube/core/model/UserAuth;

.field private t:Landroid/app/Dialog;

.field private u:Landroid/app/Dialog;

.field private v:Landroid/app/Dialog;

.field private w:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/ui/cq;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "activity can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a:Landroid/app/Activity;

    const-string v0, "analytics can\'t be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/Analytics;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->c:Lcom/google/android/youtube/core/Analytics;

    const-string v0, "userAuthorizer can\'t be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/UserAuthorizer;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    const-string v0, "gdataClient can\'t be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/bc;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->e:Lcom/google/android/youtube/core/client/bc;

    const-string v0, "errorHelper can\'t be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/e;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->f:Lcom/google/android/youtube/core/e;

    const-string v0, "listener can\'t be null"

    invoke-static {p6, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/cq;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->g:Lcom/google/android/youtube/app/ui/cq;

    const-string v0, "source can\'t be null"

    invoke-static {p7, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->w:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->b:Lcom/google/android/youtube/app/YouTubeApplication;

    new-instance v0, Lcom/google/android/youtube/app/ui/cn;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/cn;-><init>(Lcom/google/android/youtube/app/ui/SubscribeHelper;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->h:Lcom/google/android/youtube/core/async/bk;

    new-instance v0, Lcom/google/android/youtube/app/ui/cp;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/cp;-><init>(Lcom/google/android/youtube/app/ui/SubscribeHelper;B)V

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->i:Lcom/google/android/youtube/core/async/n;

    new-instance v0, Lcom/google/android/youtube/app/ui/co;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/co;-><init>(Lcom/google/android/youtube/app/ui/SubscribeHelper;B)V

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->j:Lcom/google/android/youtube/core/async/n;

    sget-object v0, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->WORKING:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->n:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/SubscribeHelper;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->r:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/SubscribeHelper;Lcom/google/android/youtube/app/ui/cr;)Lcom/google/android/youtube/app/ui/cr;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->q:Lcom/google/android/youtube/app/ui/cr;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/SubscribeHelper;Lcom/google/android/youtube/core/model/Channel;)Lcom/google/android/youtube/core/model/Channel;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->l:Lcom/google/android/youtube/core/model/Channel;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/SubscribeHelper;Lcom/google/android/youtube/core/model/Subscription;)Lcom/google/android/youtube/core/model/Subscription;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->p:Lcom/google/android/youtube/core/model/Subscription;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/SubscribeHelper;Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/model/UserAuth;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->s:Lcom/google/android/youtube/core/model/UserAuth;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Lcom/google/android/youtube/core/model/UserProfile;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->k:Lcom/google/android/youtube/core/model/UserProfile;

    return-object v0
.end method

.method private a(Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->n:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->o:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->n:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->g:Lcom/google/android/youtube/app/ui/cq;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->n:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/ui/cq;->a(Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/SubscribeHelper;Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V

    return-void
.end method

.method public static a(Lcom/google/android/youtube/core/Analytics;Ljava/lang/String;IZ)V
    .locals 1

    if-eqz p3, :cond_1

    const-string v0, "UnsubscribeFromChannel"

    :goto_0
    invoke-interface {p0, v0, p1}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    if-ltz p2, :cond_0

    if-nez p3, :cond_0

    const-string v0, "SubscribeDetails"

    invoke-interface {p0, v0, p1, p2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_0
    return-void

    :cond_1
    const-string v0, "SubscribeToChannel"

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/ui/SubscribeHelper;Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->m:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Lcom/google/android/youtube/core/async/n;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->i:Lcom/google/android/youtube/core/async/n;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Lcom/google/android/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->e:Lcom/google/android/youtube/core/client/bc;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Lcom/google/android/youtube/core/async/n;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->j:Lcom/google/android/youtube/core/async/n;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/ui/SubscribeHelper;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->l:Lcom/google/android/youtube/core/model/Channel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->m:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->m:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V

    :cond_0
    return-void
.end method

.method static synthetic f(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Lcom/google/android/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->c:Lcom/google/android/youtube/core/Analytics;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->w:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Lcom/google/android/youtube/app/YouTubeApplication;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->b:Lcom/google/android/youtube/app/YouTubeApplication;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Lcom/google/android/youtube/app/ui/cr;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->q:Lcom/google/android/youtube/app/ui/cr;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/youtube/app/ui/SubscribeHelper;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->o:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->n:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->o:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->g:Lcom/google/android/youtube/app/ui/cq;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->n:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/ui/cq;->a(Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V

    return-void
.end method

.method static synthetic l(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Lcom/google/android/youtube/core/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->f:Lcom/google/android/youtube/core/e;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->r:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic n(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Lcom/google/android/youtube/core/model/UserAuth;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->s:Lcom/google/android/youtube/core/model/UserAuth;

    return-object v0
.end method

.method static synthetic o(Lcom/google/android/youtube/app/ui/SubscribeHelper;)Lcom/google/android/youtube/core/async/UserAuthorizer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    sget-object v0, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->WORKING:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->k:Lcom/google/android/youtube/core/model/UserProfile;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/youtube/core/model/UserProfile;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserProfile;)V
    .locals 2

    const/4 v1, 0x0

    const-string v0, "channelProfile can\'t be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/UserProfile;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->k:Lcom/google/android/youtube/core/model/UserProfile;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->l:Lcom/google/android/youtube/core/model/Channel;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->m:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->o:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    sget-object v0, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->WORKING:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->n:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->r:Landroid/net/Uri;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->q:Lcom/google/android/youtube/app/ui/cr;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->h:Lcom/google/android/youtube/core/async/bk;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bk;)V

    return-void
.end method

.method public final b()V
    .locals 8

    const/4 v7, 0x0

    const v5, 0x104000a

    const/4 v4, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->n:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    sget-object v1, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->SUBSCRIBED:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    if-ne v0, v1, :cond_3

    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->c()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->v:Landroid/app/Dialog;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/youtube/core/ui/w;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/ui/w;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a:Landroid/app/Activity;

    const v2, 0x7f0b019e

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->k:Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v4, v4, Lcom/google/android/youtube/core/model/UserProfile;->displayUsername:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/w;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v5, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->v:Landroid/app/Dialog;

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->v:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->t:Landroid/app/Dialog;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/youtube/app/ui/cm;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/ui/cm;-><init>(Lcom/google/android/youtube/app/ui/SubscribeHelper;)V

    new-instance v1, Lcom/google/android/youtube/core/ui/w;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a:Landroid/app/Activity;

    invoke-direct {v1, v2}, Lcom/google/android/youtube/core/ui/w;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a:Landroid/app/Activity;

    const v3, 0x7f0b019c

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->k:Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v5, v5, Lcom/google/android/youtube/core/model/UserProfile;->displayUsername:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/ui/w;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1040013

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x1040009

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->t:Landroid/app/Dialog;

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->t:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->c()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->u:Landroid/app/Dialog;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/youtube/core/ui/w;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/ui/w;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a:Landroid/app/Activity;

    const v2, 0x7f0b019d

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->k:Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v4, v4, Lcom/google/android/youtube/core/model/UserProfile;->displayUsername:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/w;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v5, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->u:Landroid/app/Dialog;

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->u:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    goto :goto_0

    :cond_5
    sget-object v0, Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;->WORKING:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a(Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;)V

    new-instance v0, Lcom/google/android/youtube/app/ui/cr;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->f:Lcom/google/android/youtube/core/e;

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/ui/cr;-><init>(Lcom/google/android/youtube/app/ui/SubscribeHelper;Lcom/google/android/youtube/core/e;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->q:Lcom/google/android/youtube/app/ui/cr;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->q:Lcom/google/android/youtube/app/ui/cr;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;)V

    goto/16 :goto_0
.end method

.method public final c()Z
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->l:Lcom/google/android/youtube/core/model/Channel;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->l:Lcom/google/android/youtube/core/model/Channel;

    iget-boolean v2, v2, Lcom/google/android/youtube/core/model/Channel;->paidContent:Z

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final d()Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->n:Lcom/google/android/youtube/app/ui/SubscribeHelper$SubscriptionStatus;

    return-object v0
.end method

.method public final e()Lcom/google/android/youtube/core/model/Subscription;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/SubscribeHelper;->p:Lcom/google/android/youtube/core/model/Subscription;

    return-object v0
.end method
