.class public final Lcom/google/android/youtube/app/ui/ag;
.super Lcom/google/android/youtube/app/ui/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/youtube/app/d;

.field private final g:Lcom/google/android/youtube/core/Analytics;

.field private final h:Lcom/google/android/youtube/core/Analytics$VideoCategory;

.field private final i:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;Z)V
    .locals 8

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v1 .. v7}, Lcom/google/android/youtube/app/ui/a;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;Z)V

    const-string v1, "navigation cannot be null"

    invoke-static {p2, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/app/d;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/ag;->a:Lcom/google/android/youtube/app/d;

    const-string v1, "analytics may not be null"

    invoke-static {p7, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/Analytics;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/ag;->g:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "logCategory may not be null"

    move-object/from16 v0, p8

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/Analytics$VideoCategory;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/ag;->h:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    const-string v1, "feature cannot be null"

    move-object/from16 v0, p9

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/ag;->i:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    instance-of v1, p4, Lcom/google/android/youtube/app/ui/bx;

    if-eqz v1, :cond_0

    check-cast p4, Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {p4, p0}, Lcom/google/android/youtube/app/ui/bx;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ag;->d:Lcom/google/android/youtube/core/ui/PagedView;

    invoke-interface {v1, p0}, Lcom/google/android/youtube/core/ui/PagedView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/Event;I)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/Event;->targetIsVideo()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Event;->targetVideo:Lcom/google/android/youtube/core/model/Video;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ag;->g:Lcom/google/android/youtube/core/Analytics;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ag;->h:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    invoke-interface {v0, v1, p2}, Lcom/google/android/youtube/core/Analytics;->a(Lcom/google/android/youtube/core/Analytics$VideoCategory;I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ag;->a:Lcom/google/android/youtube/app/d;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Event;->target:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/ui/ag;->i:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/youtube/app/d;->a(Ljava/lang/String;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ag;->a:Lcom/google/android/youtube/app/d;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Event;->target:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/d;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
