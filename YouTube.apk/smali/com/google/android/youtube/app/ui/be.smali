.class final Lcom/google/android/youtube/app/ui/be;
.super Lcom/google/android/youtube/core/a/a;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/PrivacySpinner;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/ui/PrivacySpinner;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/be;->a:Lcom/google/android/youtube/app/ui/PrivacySpinner;

    invoke-direct {p0}, Lcom/google/android/youtube/core/a/a;-><init>()V

    invoke-static {}, Lcom/google/android/youtube/core/model/Video$Privacy;->values()[Lcom/google/android/youtube/core/model/Video$Privacy;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/ui/be;->b(Ljava/lang/Iterable;)V

    return-void
.end method


# virtual methods
.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/be;->a:Lcom/google/android/youtube/app/ui/PrivacySpinner;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/PrivacySpinner;->a(Lcom/google/android/youtube/app/ui/PrivacySpinner;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040073

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/google/android/youtube/app/ui/bf;

    invoke-direct {v0, p0, p2}, Lcom/google/android/youtube/app/ui/bf;-><init>(Lcom/google/android/youtube/app/ui/be;Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v0

    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/ui/be;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video$Privacy;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/bf;->a(Lcom/google/android/youtube/core/model/Video$Privacy;)V

    return-object p2

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/bf;

    move-object v1, v0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/be;->a:Lcom/google/android/youtube/app/ui/PrivacySpinner;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/PrivacySpinner;->a(Lcom/google/android/youtube/app/ui/PrivacySpinner;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040074

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    new-instance v0, Lcom/google/android/youtube/app/ui/bg;

    invoke-direct {v0, p0, p2}, Lcom/google/android/youtube/app/ui/bg;-><init>(Lcom/google/android/youtube/app/ui/be;Landroid/view/View;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v1, v0

    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/ui/be;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video$Privacy;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/bg;->a(Lcom/google/android/youtube/core/model/Video$Privacy;)V

    return-object p2

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/bg;

    move-object v1, v0

    goto :goto_0
.end method
