.class final Lcom/google/android/youtube/app/ui/dk;
.super Lcom/google/android/youtube/core/ui/e;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/dh;

.field private final b:Lcom/google/android/youtube/core/model/Video;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/ui/dh;Lcom/google/android/youtube/core/model/Video;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/dk;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {p1}, Lcom/google/android/youtube/app/ui/dh;->b(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/e;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/ui/e;-><init>(Lcom/google/android/youtube/core/e;)V

    const-string v0, "video may not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/dk;->b:Lcom/google/android/youtube/core/model/Video;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dk;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/dh;->h(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    const-string v1, "Favorite"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dk;->a:Lcom/google/android/youtube/app/ui/dh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/dh;->e(Lcom/google/android/youtube/app/ui/dh;)Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/dk;->b:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/dk;->a:Lcom/google/android/youtube/app/ui/dh;

    iget-object v2, v2, Lcom/google/android/youtube/app/ui/dh;->a:Landroid/app/Activity;

    invoke-static {v2, p0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v2

    invoke-interface {v0, v1, p1, v2}, Lcom/google/android/youtube/core/client/bc;->d(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 4

    const-string v0, "GData"

    const-string v1, "InvalidEntryException"

    const/4 v2, 0x0

    const-string v3, "Video already in favorite list."

    invoke-static {p2, v0, v1, v2, v3}, Lcom/google/android/youtube/core/e;->a(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dk;->a:Lcom/google/android/youtube/app/ui/dh;

    const v1, 0x7f0b01eb

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/ui/dh;->a(Lcom/google/android/youtube/app/ui/dh;I)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "Error adding to favorites"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dk;->c:Lcom/google/android/youtube/core/e;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/e;->b(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 2

    check-cast p2, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dk;->a:Lcom/google/android/youtube/app/ui/dh;

    iget-object v1, p2, Lcom/google/android/youtube/core/model/Video;->editUri:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/ui/dh;->a(Lcom/google/android/youtube/app/ui/dh;Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/dk;->a:Lcom/google/android/youtube/app/ui/dh;

    const v1, 0x7f0b01ea

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/ui/dh;->a(Lcom/google/android/youtube/app/ui/dh;I)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/core/ui/e;->a(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public final g_()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/youtube/core/ui/e;->g_()V

    return-void
.end method
