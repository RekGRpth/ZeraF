.class final Lcom/google/android/youtube/app/ui/bk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/ui/bh;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/ui/bh;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/bk;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 3

    const-string v0, "error retrieving subtitle tracks"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bk;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bh;->i(Lcom/google/android/youtube/app/ui/bh;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b0031

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;II)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    check-cast p2, Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bk;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bh;->i(Lcom/google/android/youtube/app/ui/bh;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0b0031

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;II)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bk;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bh;->g(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/core/model/Video;

    move-result-object v1

    iget-boolean v1, v1, Lcom/google/android/youtube/core/model/Video;->showSubtitlesAlways:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bk;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bh;->i(Lcom/google/android/youtube/app/ui/bh;)Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0b0030

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/youtube/core/model/SubtitleTrack;->createDisableSubtitleOption(Ljava/lang/String;)Lcom/google/android/youtube/core/model/SubtitleTrack;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/bk;->a:Lcom/google/android/youtube/app/ui/bh;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bh;->j(Lcom/google/android/youtube/app/ui/bh;)Lcom/google/android/youtube/app/ui/RemoteControlView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/RemoteControlView;->a(Ljava/util/List;)V

    goto :goto_0
.end method
