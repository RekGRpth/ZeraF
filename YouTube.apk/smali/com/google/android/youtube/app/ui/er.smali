.class final Lcom/google/android/youtube/app/ui/er;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/ch;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/bg;

.field final synthetic b:Lcom/google/android/youtube/core/Analytics;

.field final synthetic c:Lcom/google/android/youtube/app/d;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/adapter/bg;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/d;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/er;->a:Lcom/google/android/youtube/app/adapter/bg;

    iput-object p2, p0, Lcom/google/android/youtube/app/ui/er;->b:Lcom/google/android/youtube/core/Analytics;

    iput-object p3, p0, Lcom/google/android/youtube/app/ui/er;->c:Lcom/google/android/youtube/app/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/adapter/cg;I)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/er;->a:Lcom/google/android/youtube/app/adapter/bg;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/app/adapter/bg;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/ArtistSnippet;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/er;->b:Lcom/google/android/youtube/core/Analytics;

    sget-object v2, Lcom/google/android/youtube/core/Analytics$VideoCategory;->RelatedArtist:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    invoke-interface {v1, v2, p2}, Lcom/google/android/youtube/core/Analytics;->a(Lcom/google/android/youtube/core/Analytics$VideoCategory;I)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/er;->c:Lcom/google/android/youtube/app/d;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/ArtistSnippet;->id:Ljava/lang/String;

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->RELATED_ARTIST:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface {v1, v0, v2}, Lcom/google/android/youtube/app/d;->a(Ljava/lang/String;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;)V

    :cond_0
    return-void
.end method
