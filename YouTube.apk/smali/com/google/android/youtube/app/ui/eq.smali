.class public final Lcom/google/android/youtube/app/ui/eq;
.super Lcom/google/android/youtube/core/a/l;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/ui/f;


# instance fields
.field private final a:Lcom/google/android/youtube/app/adapter/bg;

.field private final c:Lcom/google/android/youtube/app/adapter/az;

.field private final d:Lcom/google/android/youtube/app/ui/e;

.field private final e:Lcom/google/android/youtube/core/a/l;

.field private final f:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/app/adapter/az;Lcom/google/android/youtube/app/adapter/ac;Lcom/google/android/youtube/core/a/l;Lcom/google/android/youtube/app/ui/e;I)V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/youtube/core/a/e;

    const/4 v1, 0x0

    aput-object p3, v0, v1

    const/4 v1, 0x1

    aput-object p4, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-static {v2, p6}, Lcom/google/android/youtube/app/adapter/cn;->a(Landroid/view/LayoutInflater;I)Lcom/google/android/youtube/app/adapter/cn;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    const-string v0, "pagingAdapter cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/az;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eq;->c:Lcom/google/android/youtube/app/adapter/az;

    invoke-virtual {p2}, Lcom/google/android/youtube/app/adapter/az;->a()Landroid/widget/BaseAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/bg;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eq;->a:Lcom/google/android/youtube/app/adapter/bg;

    const-string v0, "buttonStatusOutline cannot be null"

    invoke-static {p5, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/e;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eq;->d:Lcom/google/android/youtube/app/ui/e;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eq;->d:Lcom/google/android/youtube/app/ui/e;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/ui/e;->a(Lcom/google/android/youtube/app/ui/f;)V

    const-string v0, "bodyOutline cannot be null"

    invoke-static {p4, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/a/l;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eq;->e:Lcom/google/android/youtube/core/a/l;

    invoke-virtual {p1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b01cf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eq;->f:Ljava/lang/String;

    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;)Lcom/google/android/youtube/app/ui/eq;
    .locals 12

    const v8, 0x7f040027

    const v9, 0x7f040077

    const v10, 0x7f04001a

    const v11, 0x7f0400b4

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    invoke-static/range {v0 .. v11}, Lcom/google/android/youtube/app/ui/eq;->a(Landroid/app/Activity;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;IIII)Lcom/google/android/youtube/app/ui/eq;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/app/Activity;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;IIII)Lcom/google/android/youtube/app/ui/eq;
    .locals 14

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0a0014

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v7

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f080084

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b01ce

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b01e9

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    new-instance v6, Lcom/google/android/youtube/app/adapter/bg;

    move/from16 v0, p9

    invoke-direct {v6, p0, v0}, Lcom/google/android/youtube/app/adapter/bg;-><init>(Landroid/content/Context;I)V

    new-instance v13, Lcom/google/android/youtube/app/adapter/az;

    move/from16 v0, p3

    move/from16 v1, p4

    move/from16 v2, p5

    invoke-direct {v13, v6, v0, v1, v2}, Lcom/google/android/youtube/app/adapter/az;-><init>(Landroid/widget/BaseAdapter;III)V

    new-instance v4, Lcom/google/android/youtube/core/a/c;

    const/4 v3, 0x1

    const/4 v5, 0x0

    new-array v5, v5, [Lcom/google/android/youtube/core/a/g;

    invoke-direct {v4, v13, v3, v5}, Lcom/google/android/youtube/core/a/c;-><init>(Landroid/widget/ListAdapter;Z[Lcom/google/android/youtube/core/a/g;)V

    new-instance v3, Lcom/google/android/youtube/app/adapter/cg;

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    new-instance v9, Lcom/google/android/youtube/app/ui/er;

    move-object/from16 v0, p2

    invoke-direct {v9, v6, v0, p1}, Lcom/google/android/youtube/app/ui/er;-><init>(Lcom/google/android/youtube/app/adapter/bg;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/d;)V

    const/4 v10, 0x0

    move-object/from16 v6, p7

    invoke-direct/range {v3 .. v10}, Lcom/google/android/youtube/app/adapter/cg;-><init>(Lcom/google/android/youtube/core/a/e;Landroid/view/LayoutInflater;Lcom/google/android/youtube/core/a/g;IILcom/google/android/youtube/app/adapter/ch;Lcom/google/android/youtube/app/adapter/ci;)V

    new-instance v9, Lcom/google/android/youtube/app/ui/e;

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v4

    const/4 v5, 0x0

    move/from16 v0, p10

    invoke-direct {v9, v4, v0, v5, v12}, Lcom/google/android/youtube/app/ui/e;-><init>(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;Ljava/lang/String;)V

    new-instance v8, Lcom/google/android/youtube/core/a/l;

    const/4 v4, 0x2

    new-array v4, v4, [Lcom/google/android/youtube/core/a/e;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    const/4 v3, 0x1

    aput-object v9, v4, v3

    invoke-direct {v8, v4}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    new-instance v3, Lcom/google/android/youtube/app/adapter/ac;

    move-object v4, p0

    move-object/from16 v5, p6

    move-object v6, v11

    move/from16 v7, p8

    invoke-direct/range {v3 .. v8}, Lcom/google/android/youtube/app/adapter/ac;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/a/g;Ljava/lang/String;ILcom/google/android/youtube/core/a/e;)V

    new-instance v4, Lcom/google/android/youtube/app/ui/eq;

    move-object v5, p0

    move-object v6, v13

    move-object v7, v3

    move/from16 v10, p11

    invoke-direct/range {v4 .. v10}, Lcom/google/android/youtube/app/ui/eq;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/app/adapter/az;Lcom/google/android/youtube/app/adapter/ac;Lcom/google/android/youtube/core/a/l;Lcom/google/android/youtube/app/ui/e;I)V

    return-object v4
.end method

.method public static b(Landroid/app/Activity;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;)Lcom/google/android/youtube/app/ui/eq;
    .locals 12

    const v8, 0x7f040026

    const v9, 0x7f040076

    const v10, 0x7f040019

    const v11, 0x7f0400b3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    invoke-static/range {v0 .. v11}, Lcom/google/android/youtube/app/ui/eq;->a(Landroid/app/Activity;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;IIII)Lcom/google/android/youtube/app/ui/eq;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eq;->a:Lcom/google/android/youtube/app/adapter/bg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/bg;->clear()V

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eq;->d:Lcom/google/android/youtube/app/ui/e;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eq;->f:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/ui/e;->a(Ljava/lang/String;Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eq;->d:Lcom/google/android/youtube/app/ui/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/e;->d()V

    return-void

    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/ArtistSnippet;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/eq;->a:Lcom/google/android/youtube/app/adapter/bg;

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/app/adapter/bg;->add(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eq;->e:Lcom/google/android/youtube/core/a/l;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/a/l;->c(Z)V

    return-void
.end method

.method public final d(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final g()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eq;->c:Lcom/google/android/youtube/app/adapter/az;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/az;->c()Z

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eq;->c:Lcom/google/android/youtube/app/adapter/az;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/az;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eq;->d:Lcom/google/android/youtube/app/ui/e;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/e;->c(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/ui/eq;->k()V

    return-void
.end method

.method public final h()V
    .locals 0

    return-void
.end method
