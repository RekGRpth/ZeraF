.class final Lcom/google/android/youtube/app/ui/eh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/ch;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/adapter/bm;

.field final synthetic b:Lcom/google/android/youtube/core/Analytics;

.field final synthetic c:Lcom/google/android/youtube/app/d;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/adapter/bm;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/d;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/eh;->a:Lcom/google/android/youtube/app/adapter/bm;

    iput-object p2, p0, Lcom/google/android/youtube/app/ui/eh;->b:Lcom/google/android/youtube/core/Analytics;

    iput-object p3, p0, Lcom/google/android/youtube/app/ui/eh;->c:Lcom/google/android/youtube/app/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/adapter/cg;I)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eh;->a:Lcom/google/android/youtube/app/adapter/bm;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/app/adapter/bm;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    if-eqz v0, :cond_0

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/youtube/core/model/MusicVideo;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eh;->b:Lcom/google/android/youtube/core/Analytics;

    sget-object v2, Lcom/google/android/youtube/core/Analytics$VideoCategory;->ArtistTracks:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    invoke-interface {v1, v2, p2}, Lcom/google/android/youtube/core/Analytics;->a(Lcom/google/android/youtube/core/Analytics$VideoCategory;I)V

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eh;->c:Lcom/google/android/youtube/app/d;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/MusicVideo;->videoId:Ljava/lang/String;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->ARTIST_VIDEOS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/youtube/app/d;->a(Ljava/lang/String;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;)V

    :cond_0
    return-void
.end method
