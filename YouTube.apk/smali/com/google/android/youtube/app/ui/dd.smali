.class final Lcom/google/android/youtube/app/ui/dd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/converter/c;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a_(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    check-cast p1, Lcom/google/android/youtube/core/model/Page;

    new-instance v6, Ljava/util/ArrayList;

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/cz;->a(Lcom/google/android/youtube/core/model/Video;)Lcom/google/android/youtube/app/ui/cz;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/android/youtube/core/model/Page;

    iget v1, p1, Lcom/google/android/youtube/core/model/Page;->totalResults:I

    iget v2, p1, Lcom/google/android/youtube/core/model/Page;->elementsPerPage:I

    iget v3, p1, Lcom/google/android/youtube/core/model/Page;->startIndex:I

    iget-object v4, p1, Lcom/google/android/youtube/core/model/Page;->previousUri:Landroid/net/Uri;

    iget-object v5, p1, Lcom/google/android/youtube/core/model/Page;->nextUri:Landroid/net/Uri;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/core/model/Page;-><init>(IIILandroid/net/Uri;Landroid/net/Uri;Ljava/util/List;)V

    return-object v0
.end method
