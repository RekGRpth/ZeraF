.class final Lcom/google/android/youtube/app/ui/ca;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/core/Analytics;

.field final synthetic b:Lcom/google/android/youtube/app/remote/ad;

.field final synthetic c:Lcom/google/android/youtube/app/ui/cj;

.field final synthetic d:Landroid/app/Activity;

.field final synthetic e:Lcom/google/android/youtube/core/e;

.field final synthetic f:Lcom/google/android/youtube/app/ui/bz;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/ui/bz;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/remote/ad;Lcom/google/android/youtube/app/ui/cj;Landroid/app/Activity;Lcom/google/android/youtube/core/e;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/ca;->f:Lcom/google/android/youtube/app/ui/bz;

    iput-object p2, p0, Lcom/google/android/youtube/app/ui/ca;->a:Lcom/google/android/youtube/core/Analytics;

    iput-object p3, p0, Lcom/google/android/youtube/app/ui/ca;->b:Lcom/google/android/youtube/app/remote/ad;

    iput-object p4, p0, Lcom/google/android/youtube/app/ui/ca;->c:Lcom/google/android/youtube/app/ui/cj;

    iput-object p5, p0, Lcom/google/android/youtube/app/ui/ca;->d:Landroid/app/Activity;

    iput-object p6, p0, Lcom/google/android/youtube/app/ui/ca;->e:Lcom/google/android/youtube/core/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ca;->a:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "RemoteError"

    const-string v2, "INVALID_PAIRING_CODE"

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ca;->e:Lcom/google/android/youtube/core/e;

    const v1, 0x7f0b024c

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/e;->a(I)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    const/4 v2, 0x0

    check-cast p2, Lcom/google/android/youtube/app/remote/bp;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ca;->a:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "RemoteControlPaired"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ca;->b:Lcom/google/android/youtube/app/remote/ad;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/app/remote/ad;->a(Lcom/google/android/youtube/app/remote/bp;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ca;->c:Lcom/google/android/youtube/app/ui/cj;

    invoke-interface {v0}, Lcom/google/android/youtube/app/ui/cj;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ca;->f:Lcom/google/android/youtube/app/ui/bz;

    invoke-static {v0}, Lcom/google/android/youtube/app/ui/bz;->a(Lcom/google/android/youtube/app/ui/bz;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ca;->f:Lcom/google/android/youtube/app/ui/bz;

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/ui/bz;->a(Lcom/google/android/youtube/app/ui/bz;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/ca;->d:Landroid/app/Activity;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/google/android/youtube/app/ui/ca;->f:Lcom/google/android/youtube/app/ui/bz;

    invoke-static {v1}, Lcom/google/android/youtube/app/ui/bz;->a(Lcom/google/android/youtube/app/ui/bz;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    return-void
.end method
