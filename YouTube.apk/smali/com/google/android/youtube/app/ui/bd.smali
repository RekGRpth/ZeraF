.class public final Lcom/google/android/youtube/app/ui/bd;
.super Lcom/google/android/youtube/core/a/l;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/ui/dq;


# instance fields
.field private final a:Lcom/google/android/youtube/app/ui/b;

.field private final c:Lcom/google/android/youtube/app/ui/eo;

.field private final d:Lcom/google/android/youtube/app/ui/es;

.field private final e:Lcom/google/android/youtube/app/ui/eg;

.field private final f:Lcom/google/android/youtube/app/ui/ej;

.field private final g:Lcom/google/android/youtube/app/ui/aw;


# direct methods
.method private constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/ui/ep;Lcom/google/android/youtube/app/adapter/cn;Lcom/google/android/youtube/app/ui/b;Lcom/google/android/youtube/app/ui/eo;Lcom/google/android/youtube/app/ui/es;Lcom/google/android/youtube/app/ui/ej;Lcom/google/android/youtube/app/ui/ef;Lcom/google/android/youtube/app/ui/eg;Lcom/google/android/youtube/app/ui/eq;)V
    .locals 8

    const/16 v1, 0x8

    new-array v1, v1, [Lcom/google/android/youtube/core/a/e;

    const/4 v2, 0x0

    aput-object p4, v1, v2

    const/4 v2, 0x1

    aput-object p5, v1, v2

    const/4 v2, 0x2

    aput-object p6, v1, v2

    const/4 v2, 0x3

    aput-object p10, v1, v2

    const/4 v2, 0x4

    aput-object p11, v1, v2

    const/4 v2, 0x5

    aput-object p7, v1, v2

    const/4 v2, 0x6

    aput-object p8, v1, v2

    const/4 v2, 0x7

    aput-object p9, v1, v2

    invoke-direct {p0, v1}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    const-string v1, "brandingOutline cannot be null"

    invoke-static {p5, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/app/ui/b;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/bd;->a:Lcom/google/android/youtube/app/ui/b;

    const-string v1, "infoOutline cannot be null"

    invoke-static {p6, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/app/ui/eo;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/bd;->c:Lcom/google/android/youtube/app/ui/eo;

    const-string v1, "relatedVideosOutline cannot be null"

    invoke-static {p7, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/app/ui/es;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/bd;->d:Lcom/google/android/youtube/app/ui/es;

    const-string v1, "artistTracksOutline cannot be null"

    move-object/from16 v0, p10

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/app/ui/eg;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/bd;->e:Lcom/google/android/youtube/app/ui/eg;

    const-string v1, "commentsOutline cannot be null"

    move-object/from16 v0, p8

    invoke-static {v0, v1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/app/ui/ej;

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/bd;->f:Lcom/google/android/youtube/app/ui/ej;

    new-instance v1, Lcom/google/android/youtube/app/ui/aw;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p9

    move-object/from16 v6, p10

    move-object/from16 v7, p11

    invoke-direct/range {v1 .. v7}, Lcom/google/android/youtube/app/ui/aw;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/ui/ep;Lcom/google/android/youtube/app/ui/ef;Lcom/google/android/youtube/app/ui/eg;Lcom/google/android/youtube/app/ui/eq;)V

    iput-object v1, p0, Lcom/google/android/youtube/app/ui/bd;->g:Lcom/google/android/youtube/app/ui/aw;

    const/4 v1, 0x0

    invoke-virtual {p6, v1}, Lcom/google/android/youtube/app/ui/eo;->b(Z)V

    const/4 v1, 0x1

    invoke-virtual {p7, v1}, Lcom/google/android/youtube/app/ui/es;->a(Z)V

    const/4 v1, 0x1

    move-object/from16 v0, p8

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/ej;->a(Z)V

    const/4 v1, 0x0

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/ef;->a(Z)V

    const/4 v1, 0x1

    move-object/from16 v0, p10

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/eg;->a(Z)V

    const/4 v1, 0x1

    move-object/from16 v0, p11

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/eq;->a(Z)V

    return-void
.end method

.method public static a(Lvedroid/support/v4/app/FragmentActivity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/app/ui/dh;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/ui/bh;Lcom/google/android/youtube/app/ui/ep;)Lcom/google/android/youtube/app/ui/bd;
    .locals 23

    invoke-virtual/range {p0 .. p0}, Lvedroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0018

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v12

    invoke-virtual/range {p0 .. p0}, Lvedroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0019

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v13

    invoke-virtual/range {p0 .. p0}, Lvedroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0017

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v14

    invoke-static/range {p0 .. p1}, Lcom/google/android/youtube/app/ui/b;->b(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;)Lcom/google/android/youtube/app/ui/b;

    move-result-object v18

    move-object/from16 v2, p0

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p7

    move-object/from16 v6, p3

    move-object/from16 v7, p1

    invoke-static/range {v2 .. v7}, Lcom/google/android/youtube/app/ui/eo;->a(Lvedroid/support/v4/app/FragmentActivity;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;)Lcom/google/android/youtube/app/ui/eo;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p6

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/eo;->a(Lcom/google/android/youtube/app/ui/dh;)V

    invoke-interface/range {p3 .. p3}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v9

    sget-object v15, Lcom/google/android/youtube/app/ui/ee;->b:Lcom/google/android/youtube/core/a/g;

    sget-object v16, Lcom/google/android/youtube/app/ui/ee;->a:Lcom/google/android/youtube/core/a/g;

    sget-object v17, Lcom/google/android/youtube/app/ui/ee;->c:Lcom/google/android/youtube/core/a/g;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p3

    move-object/from16 v5, p2

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    move-object/from16 v8, p7

    move-object/from16 v10, p5

    move-object/from16 v11, p4

    invoke-static/range {v2 .. v17}, Lcom/google/android/youtube/app/ui/es;->b(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/prefetch/d;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/async/GDataRequestFactory;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;)Lcom/google/android/youtube/app/ui/es;

    move-result-object v17

    sget-object v15, Lcom/google/android/youtube/app/ui/ee;->d:Lcom/google/android/youtube/core/a/g;

    sget-object v16, Lcom/google/android/youtube/app/ui/ee;->a:Lcom/google/android/youtube/core/a/g;

    move-object/from16 v8, p0

    move-object/from16 v9, p3

    move-object/from16 v10, p9

    move-object/from16 v11, p6

    invoke-static/range {v8 .. v16}, Lcom/google/android/youtube/app/ui/ej;->b(Landroid/app/Activity;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/ui/dh;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;)Lcom/google/android/youtube/app/ui/ej;

    move-result-object v20

    sget-object v2, Lcom/google/android/youtube/app/ui/ee;->a:Lcom/google/android/youtube/core/a/g;

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/ui/ef;->b(Landroid/app/Activity;Lcom/google/android/youtube/core/a/g;)Lcom/google/android/youtube/app/ui/ef;

    move-result-object v21

    sget-object v15, Lcom/google/android/youtube/app/ui/ee;->a:Lcom/google/android/youtube/core/a/g;

    sget-object v16, Lcom/google/android/youtube/app/ui/ee;->e:Lcom/google/android/youtube/core/a/g;

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p3

    move-object/from16 v10, p5

    move-object/from16 v11, p4

    invoke-static/range {v7 .. v16}, Lcom/google/android/youtube/app/ui/eg;->b(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;)Lcom/google/android/youtube/app/ui/eg;

    move-result-object v22

    sget-object v15, Lcom/google/android/youtube/app/ui/ee;->a:Lcom/google/android/youtube/core/a/g;

    sget-object v16, Lcom/google/android/youtube/app/ui/ee;->f:Lcom/google/android/youtube/core/a/g;

    move-object/from16 v9, p0

    move-object/from16 v10, p5

    move-object/from16 v11, p4

    invoke-static/range {v9 .. v16}, Lcom/google/android/youtube/app/ui/eq;->b(Landroid/app/Activity;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;IIILcom/google/android/youtube/core/a/g;Lcom/google/android/youtube/core/a/g;)Lcom/google/android/youtube/app/ui/eq;

    move-result-object v13

    new-instance v2, Lcom/google/android/youtube/app/ui/bd;

    invoke-virtual/range {p10 .. p10}, Lcom/google/android/youtube/app/ui/bh;->v()Lcom/google/android/youtube/app/adapter/cn;

    move-result-object v6

    move-object/from16 v3, p0

    move-object/from16 v4, p2

    move-object/from16 v5, p11

    move-object/from16 v7, v18

    move-object/from16 v8, v19

    move-object/from16 v9, v17

    move-object/from16 v10, v20

    move-object/from16 v11, v21

    move-object/from16 v12, v22

    invoke-direct/range {v2 .. v13}, Lcom/google/android/youtube/app/ui/bd;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/app/ui/ep;Lcom/google/android/youtube/app/adapter/cn;Lcom/google/android/youtube/app/ui/b;Lcom/google/android/youtube/app/ui/eo;Lcom/google/android/youtube/app/ui/es;Lcom/google/android/youtube/app/ui/ej;Lcom/google/android/youtube/app/ui/ef;Lcom/google/android/youtube/app/ui/eg;Lcom/google/android/youtube/app/ui/eq;)V

    return-object v2
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/Branding;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bd;->a:Lcom/google/android/youtube/app/ui/b;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/b;->a(Lcom/google/android/youtube/core/model/Branding;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bd;->d:Lcom/google/android/youtube/app/ui/es;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/es;->a(Lcom/google/android/youtube/core/model/Branding;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .locals 2

    const-string v0, "video cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bd;->c:Lcom/google/android/youtube/app/ui/eo;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/eo;->a(Lcom/google/android/youtube/core/model/Video;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bd;->d:Lcom/google/android/youtube/app/ui/es;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/es;->a(Lcom/google/android/youtube/core/model/Video;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bd;->f:Lcom/google/android/youtube/app/ui/ej;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/ej;->a(Lcom/google/android/youtube/core/model/Video;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bd;->g:Lcom/google/android/youtube/app/ui/aw;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/aw;->a(Lcom/google/android/youtube/core/model/Video;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bd;->a:Lcom/google/android/youtube/app/ui/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/b;->c(Z)V

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bd;->d:Lcom/google/android/youtube/app/ui/es;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/es;->i()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bd;->f:Lcom/google/android/youtube/app/ui/ej;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ej;->j()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bd;->g:Lcom/google/android/youtube/app/ui/aw;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/aw;->a()V

    return-void
.end method

.method public final c()Lcom/google/android/youtube/app/adapter/cn;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bd;->c:Lcom/google/android/youtube/app/ui/eo;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/eo;->b()Lcom/google/android/youtube/app/adapter/cn;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bd;->d:Lcom/google/android/youtube/app/ui/es;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/es;->j()V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bd;->e:Lcom/google/android/youtube/app/ui/eg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/eg;->b()V

    return-void
.end method

.method public final j(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/bd;->c:Lcom/google/android/youtube/app/ui/eo;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/eo;->a(Z)V

    return-void
.end method
