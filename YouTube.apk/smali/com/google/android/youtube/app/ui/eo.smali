.class public final Lcom/google/android/youtube/app/ui/eo;
.super Lcom/google/android/youtube/core/a/l;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Lcom/google/android/youtube/app/ui/ek;

.field private final c:Lcom/google/android/youtube/app/adapter/cn;

.field private final d:Landroid/widget/TextView;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/adapter/cn;Lcom/google/android/youtube/app/adapter/cn;Lvedroid/support/v4/app/FragmentActivity;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;)V
    .locals 9

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/youtube/core/a/e;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    const/4 v1, 0x1

    aput-object p2, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p3}, Lvedroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0400b3

    invoke-static {v2, v3}, Lcom/google/android/youtube/app/adapter/cn;->a(Landroid/view/LayoutInflater;I)Lcom/google/android/youtube/app/adapter/cn;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    new-instance v0, Lcom/google/android/youtube/app/ui/ek;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/adapter/cn;->b()Landroid/view/View;

    move-result-object v7

    invoke-virtual {p2}, Lcom/google/android/youtube/app/adapter/cn;->b()Landroid/view/View;

    move-result-object v8

    move-object v1, p3

    move-object v2, p4

    move-object v3, p5

    move-object v4, p6

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/app/ui/ek;-><init>(Lvedroid/support/v4/app/FragmentActivity;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Landroid/view/View;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->a:Lcom/google/android/youtube/app/ui/ek;

    iput-object p2, p0, Lcom/google/android/youtube/app/ui/eo;->c:Lcom/google/android/youtube/app/adapter/cn;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/adapter/cn;->b()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f07002c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->d:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->d:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/eo;->d(Z)V

    return-void
.end method

.method public static a(Lvedroid/support/v4/app/FragmentActivity;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;)Lcom/google/android/youtube/app/ui/eo;
    .locals 9

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0}, Lvedroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400af

    invoke-virtual {v0, v1, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v1, 0x7f0400ac

    invoke-virtual {v0, v1, v4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/app/adapter/cn;

    invoke-direct {v1, v2}, Lcom/google/android/youtube/app/adapter/cn;-><init>(Landroid/view/View;)V

    new-instance v2, Lcom/google/android/youtube/app/adapter/cn;

    invoke-direct {v2, v0}, Lcom/google/android/youtube/app/adapter/cn;-><init>(Landroid/view/View;)V

    new-instance v0, Lcom/google/android/youtube/app/ui/eo;

    move-object v3, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    move-object v7, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/youtube/app/ui/eo;-><init>(Lcom/google/android/youtube/app/adapter/cn;Lcom/google/android/youtube/app/adapter/cn;Lvedroid/support/v4/app/FragmentActivity;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;)V

    return-object v0
.end method

.method private d(Z)V
    .locals 3

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    const v0, 0x7f0200cd

    :goto_0
    iget-object v1, p0, Lcom/google/android/youtube/app/ui/eo;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v2, v2, v0, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->c:Lcom/google/android/youtube/app/adapter/cn;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/adapter/cn;->c(Z)V

    return-void

    :cond_0
    const v0, 0x7f0200cb

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/app/ui/dh;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->a:Lcom/google/android/youtube/app/ui/ek;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/ek;->a(Lcom/google/android/youtube/app/ui/dh;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Video;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->a:Lcom/google/android/youtube/app/ui/ek;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/ek;->a(Lcom/google/android/youtube/core/model/Video;)V

    return-void
.end method

.method public final a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->a:Lcom/google/android/youtube/app/ui/ek;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/ek;->a(Z)V

    return-void
.end method

.method public final b()Lcom/google/android/youtube/app/adapter/cn;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->c:Lcom/google/android/youtube/app/adapter/cn;

    return-object v0
.end method

.method public final b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->c:Lcom/google/android/youtube/app/adapter/cn;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/cn;->c(Z)V

    return-void
.end method

.method public final d(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->d:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/eo;->c:Lcom/google/android/youtube/app/adapter/cn;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/adapter/cn;->l()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/ui/eo;->d(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
