.class public final Lcom/google/android/youtube/app/ui/b;
.super Lcom/google/android/youtube/core/a/l;
.source "SourceFile"


# instance fields
.field private final a:Landroid/app/Activity;

.field private final c:Lcom/google/android/youtube/core/client/be;

.field private final d:Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

.field private final e:Landroid/widget/ImageView;

.field private final f:Lcom/google/android/youtube/app/adapter/cn;


# direct methods
.method private constructor <init>(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/app/adapter/cn;Lcom/google/android/youtube/app/adapter/cn;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/youtube/core/a/e;

    aput-object p3, v0, v2

    const/4 v1, 0x1

    aput-object p4, v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/youtube/core/a/l;-><init>([Lcom/google/android/youtube/core/a/e;)V

    iput-object p1, p0, Lcom/google/android/youtube/app/ui/b;->a:Landroid/app/Activity;

    iput-object p2, p0, Lcom/google/android/youtube/app/ui/b;->c:Lcom/google/android/youtube/core/client/be;

    invoke-virtual {p3}, Lcom/google/android/youtube/app/adapter/cn;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/b;->d:Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->d:Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

    const v1, 0x7f070173

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/ui/b;->e:Landroid/widget/ImageView;

    iput-object p4, p0, Lcom/google/android/youtube/app/ui/b;->f:Lcom/google/android/youtube/app/adapter/cn;

    invoke-virtual {p4, v2}, Lcom/google/android/youtube/app/adapter/cn;->c(Z)V

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/app/ui/b;->c(Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/ui/b;)Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->d:Lcom/google/android/youtube/app/ui/FixedAspectRatioFrameLayout;

    return-object v0
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;)Lcom/google/android/youtube/app/ui/b;
    .locals 2

    const v0, 0x7f0400ae

    const v1, 0x7f0400b4

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/youtube/app/ui/b;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;II)Lcom/google/android/youtube/app/ui/b;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;II)Lcom/google/android/youtube/app/ui/b;
    .locals 3

    const-string v0, "activity cannot be null"

    invoke-static {p0, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "imageClient cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p2, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/app/adapter/cn;

    invoke-direct {v1, v0}, Lcom/google/android/youtube/app/adapter/cn;-><init>(Landroid/view/View;)V

    new-instance v0, Lcom/google/android/youtube/app/ui/b;

    invoke-virtual {p0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-static {v2, p3}, Lcom/google/android/youtube/app/adapter/cn;->a(Landroid/view/LayoutInflater;I)Lcom/google/android/youtube/app/adapter/cn;

    move-result-object v2

    invoke-direct {v0, p0, p1, v1, v2}, Lcom/google/android/youtube/app/ui/b;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/app/adapter/cn;Lcom/google/android/youtube/app/adapter/cn;)V

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/ui/b;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->e:Landroid/widget/ImageView;

    return-object v0
.end method

.method public static b(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;)Lcom/google/android/youtube/app/ui/b;
    .locals 2

    const v0, 0x7f0400ad

    const v1, 0x7f0400b3

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/youtube/app/ui/b;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;II)Lcom/google/android/youtube/app/ui/b;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/ui/b;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->a:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/Branding;)V
    .locals 4

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Branding;->bannerUri:Landroid/net/Uri;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->c:Lcom/google/android/youtube/core/client/be;

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Branding;->bannerUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/ui/b;->a:Landroid/app/Activity;

    new-instance v3, Lcom/google/android/youtube/app/ui/c;

    invoke-direct {v3, p0, p1}, Lcom/google/android/youtube/app/ui/c;-><init>(Lcom/google/android/youtube/app/ui/b;Lcom/google/android/youtube/core/model/Branding;)V

    invoke-static {v2, v3}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/be;->a(Landroid/net/Uri;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/ui/b;->f:Lcom/google/android/youtube/app/adapter/cn;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/cn;->c(Z)V

    return-void
.end method
