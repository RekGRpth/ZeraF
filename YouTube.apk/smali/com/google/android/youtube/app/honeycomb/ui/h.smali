.class final Lcom/google/android/youtube/app/honeycomb/ui/h;
.super Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;
.source "SourceFile"


# instance fields
.field protected a:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/d;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/d;Ljava/lang/String;Lcom/google/android/youtube/app/compat/r;)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/h;->a:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    return-object v0
.end method

.method public final a(Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/h;->a:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/h;->g:Landroid/app/Activity;

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->l_()V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/honeycomb/ui/i;)V
    .locals 0

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 0

    return-void
.end method

.method public final a(Z)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)Z
    .locals 3

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/compat/m;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/h;->a:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    sget-object v2, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->DISABLED:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    if-ne v1, v2, :cond_0

    const v1, 0x7f070185

    invoke-virtual {p1, v1}, Lcom/google/android/youtube/app/compat/m;->f(I)V

    :cond_0
    return v0
.end method

.method public final b()V
    .locals 0

    return-void
.end method

.method public final c()V
    .locals 0

    return-void
.end method

.method protected final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/h;->g:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->onSearchRequested()Z

    return-void
.end method
