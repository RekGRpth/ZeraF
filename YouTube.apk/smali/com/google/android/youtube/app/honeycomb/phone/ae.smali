.class final Lcom/google/android/youtube/app/honeycomb/phone/ae;
.super Lcom/google/android/youtube/core/ui/j;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/phone/ab;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;)V
    .locals 6

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ae;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/core/ui/j;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;)V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ae;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Lcom/google/android/youtube/app/honeycomb/phone/ab;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ae;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->c(Lcom/google/android/youtube/app/honeycomb/phone/ab;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ae;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->e(Lcom/google/android/youtube/app/honeycomb/phone/ab;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ae;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    const-string v1, "TRENDING_GUIDE_ITEM"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Ljava/lang/String;Z)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/model/Page;)V
    .locals 4

    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/core/ui/j;->a(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/model/Page;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ae;->f()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ae;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->f(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Lcom/google/android/youtube/core/Analytics;

    move-result-object v1

    const-string v2, "GuideSubscriptionsInit"

    const-string v3, "-"

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ae;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->g(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Lcom/google/android/youtube/app/YouTubeApplication;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/YouTubeApplication;->b(I)V

    return-void
.end method

.method protected final a(Lcom/google/android/youtube/core/async/GDataRequest;Ljava/util/List;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ae;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->c(Lcom/google/android/youtube/app/honeycomb/phone/ab;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ae;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Lcom/google/android/youtube/app/honeycomb/phone/ab;Z)Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ae;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->d(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ae;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    const-string v1, "WHAT_TO_WATCH"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/ui/PagedView;III)V
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/youtube/core/ui/j;->a(Lcom/google/android/youtube/core/ui/PagedView;III)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ae;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->h(Lcom/google/android/youtube/app/honeycomb/phone/ab;)V

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequest;

    check-cast p2, Lcom/google/android/youtube/core/model/Page;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/phone/ae;->a(Lcom/google/android/youtube/core/async/GDataRequest;Lcom/google/android/youtube/core/model/Page;)V

    return-void
.end method

.method protected final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ae;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->e(Lcom/google/android/youtube/app/honeycomb/phone/ab;)V

    return-void
.end method
