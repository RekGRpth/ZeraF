.class public final Lcom/google/android/youtube/app/honeycomb/phone/ay;
.super Lcom/google/android/youtube/app/honeycomb/phone/bd;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/youtube/core/client/be;

.field private final c:Lcom/google/android/youtube/core/e;

.field private final d:Lcom/google/android/youtube/core/utils/p;

.field private final e:Lcom/google/android/youtube/core/client/bc;

.field private f:Lcom/google/android/youtube/app/ui/bx;

.field private g:Lcom/google/android/youtube/app/ui/bx;

.field private h:Lcom/google/android/youtube/core/ui/j;

.field private i:Lcom/google/android/youtube/core/ui/j;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/bd;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V

    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->b:Lcom/google/android/youtube/core/client/be;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->l()Lcom/google/android/youtube/core/utils/p;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->d:Lcom/google/android/youtube/core/utils/p;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->e:Lcom/google/android/youtube/core/client/bc;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->c:Lcom/google/android/youtube/core/e;

    return-void
.end method

.method private n()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->f:Lcom/google/android/youtube/app/ui/bx;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->f:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/bx;->a(I)V

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->g:Lcom/google/android/youtube/app/ui/bx;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->g:Lcom/google/android/youtube/app/ui/bx;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/bx;->a(I)V

    :cond_1
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/youtube/core/ui/PagedListView;I)Lcom/google/android/youtube/core/a/a;
    .locals 11

    const/4 v10, 0x1

    const/4 v9, 0x0

    packed-switch p2, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->e:Lcom/google/android/youtube/core/client/bc;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->b:Lcom/google/android/youtube/core/client/be;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->d:Lcom/google/android/youtube/core/utils/p;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/youtube/app/adapter/bn;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/utils/p;)Lcom/google/android/youtube/app/adapter/bm;

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v0, v8}, Lcom/google/android/youtube/app/ui/bx;->a(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)Lcom/google/android/youtube/app/ui/bx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->f:Lcom/google/android/youtube/app/ui/bx;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ay;->n()V

    new-instance v0, Lcom/google/android/youtube/app/ui/au;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->f:Lcom/google/android/youtube/app/ui/bx;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->e:Lcom/google/android/youtube/core/client/bc;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->c:Lcom/google/android/youtube/core/e;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v6

    new-instance v7, Lcom/google/android/youtube/app/honeycomb/phone/az;

    invoke-direct {v7, p0}, Lcom/google/android/youtube/app/honeycomb/phone/az;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ay;)V

    move-object v2, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/youtube/app/ui/au;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/app/ui/bx;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/utils/t;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->h:Lcom/google/android/youtube/core/ui/j;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->h:Lcom/google/android/youtube/core/ui/j;

    new-array v1, v10, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->e:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v2}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->b()Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v9

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/j;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    move-object v0, v8

    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->e:Lcom/google/android/youtube/core/client/bc;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->b:Lcom/google/android/youtube/core/client/be;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->d:Lcom/google/android/youtube/core/utils/p;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/youtube/app/adapter/bn;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/utils/p;)Lcom/google/android/youtube/app/adapter/bm;

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v0, v8}, Lcom/google/android/youtube/app/ui/bx;->a(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)Lcom/google/android/youtube/app/ui/bx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->g:Lcom/google/android/youtube/app/ui/bx;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ay;->n()V

    new-instance v0, Lcom/google/android/youtube/app/ui/au;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->g:Lcom/google/android/youtube/app/ui/bx;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->e:Lcom/google/android/youtube/core/client/bc;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->c:Lcom/google/android/youtube/core/e;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v6

    new-instance v7, Lcom/google/android/youtube/app/honeycomb/phone/ba;

    invoke-direct {v7, p0}, Lcom/google/android/youtube/app/honeycomb/phone/ba;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ay;)V

    move-object v2, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/youtube/app/ui/au;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/app/ui/bx;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/utils/t;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->i:Lcom/google/android/youtube/core/ui/j;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->i:Lcom/google/android/youtube/core/ui/j;

    new-array v1, v10, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->e:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v2}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->c()Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v9

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/j;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    move-object v0, v8

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bd;->a()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ay;->d(I)V

    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/bd;->a(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ay;->n()V

    return-void
.end method

.method protected final c(I)Ljava/lang/String;
    .locals 2

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0226

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ay;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0b0228

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
