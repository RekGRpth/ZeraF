.class public final Lcom/google/android/youtube/app/honeycomb/phone/bb;
.super Lcom/google/android/youtube/a/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/compat/v;
.implements Lcom/google/android/youtube/app/honeycomb/ui/i;


# instance fields
.field private final a:Lcom/google/android/youtube/app/remote/ad;

.field private b:Lcom/google/android/youtube/app/compat/t;

.field private c:Lcom/google/android/youtube/app/compat/t;

.field private d:Lcom/google/android/youtube/app/compat/t;

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Ljava/lang/Object;

.field private i:Landroid/view/View;

.field private j:Lcom/google/android/youtube/app/honeycomb/phone/bc;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/youtube/app/remote/ad;)V
    .locals 2

    const/4 v1, 0x0

    const v0, 0x800001

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/a/a;-><init>(Landroid/content/Context;I)V

    iput-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->e:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->f:Z

    iput-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->g:Z

    const-string v0, "mediaRouteManager can not be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/ad;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-static {p1}, Lcom/android/athome/picker/media/k;->a(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->h:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final a()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->i:Landroid/view/View;

    return-object v0
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)V
    .locals 2

    const v1, 0x800001

    const v0, 0x7f070182

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->b:Lcom/google/android/youtube/app/compat/t;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->b:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/app/compat/t;->a(Lcom/google/android/youtube/app/compat/v;)Lcom/google/android/youtube/app/compat/t;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->b:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->d()Landroid/view/View;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/e;->a(Landroid/view/View;I)V

    const v0, 0x7f070183

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->d:Lcom/google/android/youtube/app/compat/t;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->d:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/app/compat/t;->a(Lcom/google/android/youtube/app/compat/v;)Lcom/google/android/youtube/app/compat/t;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->d:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->d()Landroid/view/View;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/android/athome/picker/media/e;->a(Landroid/view/View;I)V

    const v0, 0x7f070184

    invoke-virtual {p1, v0}, Lcom/google/android/youtube/app/compat/m;->c(I)Lcom/google/android/youtube/app/compat/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->c:Lcom/google/android/youtube/app/compat/t;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->c:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0, p0}, Lcom/google/android/youtube/app/compat/t;->a(Lcom/google/android/youtube/app/compat/v;)Lcom/google/android/youtube/app/compat/t;

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/honeycomb/phone/bc;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->j:Lcom/google/android/youtube/app/honeycomb/phone/bc;

    return-void
.end method

.method public final a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->g:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bb;->d()V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/compat/t;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bb;->g()V

    const/4 v0, 0x1

    return v0
.end method

.method public final b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->f:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bb;->d()V

    return-void
.end method

.method protected final c(Z)V
    .locals 7

    const/4 v6, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->b:Lcom/google/android/youtube/app/compat/t;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->c:Lcom/google/android/youtube/app/compat/t;

    if-nez v0, :cond_2

    :cond_0
    iput-object v6, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->i:Landroid/view/View;

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->h:Ljava/lang/Object;

    invoke-static {v0}, Lcom/android/athome/picker/media/k;->a(Ljava/lang/Object;)I

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->e:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->f:Z

    if-nez v0, :cond_4

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->g:Z

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bb;->e()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_5

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->a:Lcom/google/android/youtube/app/remote/ad;

    invoke-virtual {v4}, Lcom/google/android/youtube/app/remote/ad;->f()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bb;->f()I

    move-result v3

    if-gt v3, v1, :cond_5

    move v3, v1

    :goto_2
    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->b:Lcom/google/android/youtube/app/compat/t;

    if-eqz v0, :cond_6

    if-eqz p1, :cond_6

    if-nez v3, :cond_6

    move v4, v1

    :goto_3
    invoke-interface {v5, v4}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->b:Lcom/google/android/youtube/app/compat/t;

    if-eqz v0, :cond_7

    if-eqz p1, :cond_7

    if-nez v3, :cond_7

    move v4, v1

    :goto_4
    invoke-interface {v5, v4}, Lcom/google/android/youtube/app/compat/t;->a(Z)Lcom/google/android/youtube/app/compat/t;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->c:Lcom/google/android/youtube/app/compat/t;

    if-nez v0, :cond_8

    if-eqz p1, :cond_8

    move v4, v1

    :goto_5
    invoke-interface {v5, v4}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->c:Lcom/google/android/youtube/app/compat/t;

    if-nez v0, :cond_9

    if-eqz p1, :cond_9

    move v4, v1

    :goto_6
    invoke-interface {v5, v4}, Lcom/google/android/youtube/app/compat/t;->a(Z)Lcom/google/android/youtube/app/compat/t;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->d:Lcom/google/android/youtube/app/compat/t;

    if-eqz v0, :cond_a

    if-eqz p1, :cond_a

    if-eqz v3, :cond_a

    move v4, v1

    :goto_7
    invoke-interface {v5, v4}, Lcom/google/android/youtube/app/compat/t;->b(Z)Lcom/google/android/youtube/app/compat/t;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->d:Lcom/google/android/youtube/app/compat/t;

    if-eqz v0, :cond_b

    if-eqz p1, :cond_b

    if-eqz v3, :cond_b

    :goto_8
    invoke-interface {v4, v1}, Lcom/google/android/youtube/app/compat/t;->a(Z)Lcom/google/android/youtube/app/compat/t;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->i:Landroid/view/View;

    if-eqz v0, :cond_d

    if-eqz p1, :cond_d

    if-eqz v3, :cond_c

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->d:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->d()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->i:Landroid/view/View;

    :goto_9
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->i:Landroid/view/View;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->j:Lcom/google/android/youtube/app/honeycomb/phone/bc;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->j:Lcom/google/android/youtube/app/honeycomb/phone/bc;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->i:Landroid/view/View;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/bc;->a(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto/16 :goto_1

    :cond_5
    move v3, v2

    goto :goto_2

    :cond_6
    move v4, v2

    goto :goto_3

    :cond_7
    move v4, v2

    goto :goto_4

    :cond_8
    move v4, v2

    goto :goto_5

    :cond_9
    move v4, v2

    goto :goto_6

    :cond_a
    move v4, v2

    goto :goto_7

    :cond_b
    move v1, v2

    goto :goto_8

    :cond_c
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->b:Lcom/google/android/youtube/app/compat/t;

    invoke-interface {v0}, Lcom/google/android/youtube/app/compat/t;->d()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->i:Landroid/view/View;

    goto :goto_9

    :cond_d
    iput-object v6, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->i:Landroid/view/View;

    goto :goto_9
.end method

.method public final s()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->e:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bb;->d()V

    return-void
.end method

.method public final t()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bb;->e:Z

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/bb;->d()V

    return-void
.end method
