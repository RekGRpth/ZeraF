.class final Lcom/google/android/youtube/app/honeycomb/ui/x;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/ui/t;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/honeycomb/ui/t;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    check-cast p1, [Ljava/util/List;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-static {v2, v0}, Lcom/google/android/youtube/app/honeycomb/ui/t;->a(Lcom/google/android/youtube/app/honeycomb/ui/t;Landroid/net/Uri;)Lcom/google/android/youtube/app/honeycomb/ui/ad;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-static {v2}, Lcom/google/android/youtube/app/honeycomb/ui/t;->f(Lcom/google/android/youtube/app/honeycomb/ui/t;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/t;->f(Lcom/google/android/youtube/app/honeycomb/ui/t;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 13

    const-wide/16 v3, 0x0

    const/16 v9, 0x8

    const/4 v8, 0x1

    const/4 v6, 0x0

    check-cast p1, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "nothing to upload"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/t;->g(Lcom/google/android/youtube/app/honeycomb/ui/t;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move-wide v1, v3

    move v5, v6

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/ui/ad;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->a(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->b(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_1

    move v7, v8

    :goto_2
    or-int/2addr v5, v7

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->c(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    add-long v0, v1, v11

    move-wide v1, v0

    goto :goto_1

    :cond_1
    move v7, v6

    goto :goto_2

    :cond_2
    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/ui/ad;

    iget-object v7, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-static {v7}, Lcom/google/android/youtube/app/honeycomb/ui/t;->h(Lcom/google/android/youtube/app/honeycomb/ui/t;)Landroid/widget/ImageView;

    move-result-object v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-static {v7}, Lcom/google/android/youtube/app/honeycomb/ui/t;->h(Lcom/google/android/youtube/app/honeycomb/ui/t;)Landroid/widget/ImageView;

    move-result-object v7

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->d(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Landroid/graphics/Bitmap;

    move-result-object v10

    invoke-virtual {v7, v10}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_3
    iget-object v7, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-static {v7}, Lcom/google/android/youtube/app/honeycomb/ui/t;->i(Lcom/google/android/youtube/app/honeycomb/ui/t;)Landroid/widget/TextView;

    move-result-object v7

    if-eqz v7, :cond_4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    if-ne v7, v8, :cond_8

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->e(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_7

    iget-object v7, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-static {v7}, Lcom/google/android/youtube/app/honeycomb/ui/t;->i(Lcom/google/android/youtube/app/honeycomb/ui/t;)Landroid/widget/TextView;

    move-result-object v7

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->e(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    :goto_3
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/t;->k(Lcom/google/android/youtube/app/honeycomb/ui/t;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_5

    cmp-long v0, v1, v3

    if-ltz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/t;->k(Lcom/google/android/youtube/app/honeycomb/ui/t;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/t;->k(Lcom/google/android/youtube/app/honeycomb/ui/t;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-static {v3}, Lcom/google/android/youtube/app/honeycomb/ui/t;->g(Lcom/google/android/youtube/app/honeycomb/ui/t;)Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3, v1, v2}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    :goto_4
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/t;->l(Lcom/google/android/youtube/app/honeycomb/ui/t;)Landroid/widget/CheckBox;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/t;->l(Lcom/google/android/youtube/app/honeycomb/ui/t;)Landroid/widget/CheckBox;

    move-result-object v0

    if-eqz v5, :cond_a

    :goto_5
    invoke-virtual {v0, v6}, Landroid/widget/CheckBox;->setVisibility(I)V

    :cond_6
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/t;->e(Lcom/google/android/youtube/app/honeycomb/ui/t;)V

    goto/16 :goto_0

    :cond_7
    iget-object v7, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-static {v7}, Lcom/google/android/youtube/app/honeycomb/ui/t;->i(Lcom/google/android/youtube/app/honeycomb/ui/t;)Landroid/widget/TextView;

    move-result-object v7

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/ad;->f(Lcom/google/android/youtube/app/honeycomb/ui/ad;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_8
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/t;->i(Lcom/google/android/youtube/app/honeycomb/ui/t;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v7, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-static {v7}, Lcom/google/android/youtube/app/honeycomb/ui/t;->j(Lcom/google/android/youtube/app/honeycomb/ui/t;)Landroid/content/res/Resources;

    move-result-object v7

    const v10, 0x7f0b0163

    new-array v8, v8, [Ljava/lang/Object;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v8, v6

    invoke-virtual {v7, v10, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_9
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/x;->a:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/t;->k(Lcom/google/android/youtube/app/honeycomb/ui/t;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    :cond_a
    move v6, v9

    goto :goto_5
.end method
