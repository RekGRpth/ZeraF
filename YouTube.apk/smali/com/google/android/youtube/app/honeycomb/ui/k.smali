.class final Lcom/google/android/youtube/app/honeycomb/ui/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/d;

.field final synthetic b:Lcom/google/android/youtube/core/Analytics;

.field final synthetic c:Lcom/google/android/youtube/app/honeycomb/ui/j;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/honeycomb/ui/j;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/Analytics;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/k;->c:Lcom/google/android/youtube/app/honeycomb/ui/j;

    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/ui/k;->a:Lcom/google/android/youtube/app/d;

    iput-object p3, p0, Lcom/google/android/youtube/app/honeycomb/ui/k;->b:Lcom/google/android/youtube/core/Analytics;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/k;->c:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/j;->a(Lcom/google/android/youtube/app/honeycomb/ui/j;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/k;->c:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/j;->a(Lcom/google/android/youtube/app/honeycomb/ui/j;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/k;->c:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/ui/j;->b(Lcom/google/android/youtube/app/honeycomb/ui/j;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/k;->a:Lcom/google/android/youtube/app/d;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/k;->c:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/ui/j;->a(Lcom/google/android/youtube/app/honeycomb/ui/j;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->REMOTE_QUEUE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/youtube/app/d;->a(Ljava/lang/String;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/k;->b:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "RemoteControlBarGoToWatch"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    goto :goto_0
.end method
