.class final Lcom/google/android/youtube/app/honeycomb/phone/bs;
.super Lcom/google/android/youtube/core/ui/e;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;

.field private final b:Lcom/google/android/youtube/core/model/Video;

.field private final d:Landroid/app/Activity;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;Lcom/google/android/youtube/core/model/Video;Landroid/app/Activity;Lcom/google/android/youtube/core/e;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bs;->a:Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;

    invoke-direct {p0, p4}, Lcom/google/android/youtube/core/ui/e;-><init>(Lcom/google/android/youtube/core/e;)V

    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bs;->b:Lcom/google/android/youtube/core/model/Video;

    iput-object p3, p0, Lcom/google/android/youtube/app/honeycomb/phone/bs;->d:Landroid/app/Activity;

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/bs;)Lcom/google/android/youtube/core/model/Video;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bs;->b:Lcom/google/android/youtube/core/model/Video;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bs;->a:Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->a(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;)Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bs;->b:Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->editUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bs;->d:Landroid/app/Activity;

    invoke-static {v2, p0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v2

    invoke-interface {v0, v1, p1, v2}, Lcom/google/android/youtube/core/client/bc;->f(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    const-string v0, "Error deleting from playlist"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bs;->c:Lcom/google/android/youtube/core/e;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/e;->b(Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bs;->a:Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->d(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;)Lcom/google/android/youtube/app/ui/v;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/v;->a(Z)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bs;->d:Landroid/app/Activity;

    const v1, 0x7f0b01fd

    invoke-static {v0, v1, v2}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;II)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bs;->a:Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->b(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;)Lcom/google/android/youtube/app/adapter/bm;

    move-result-object v0

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/bt;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/bt;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/bs;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/adapter/bm;->a(Lcom/google/android/youtube/core/utils/t;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bs;->a:Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bs;->a:Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->c(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->a(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bs;->a:Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;->d(Lcom/google/android/youtube/app/honeycomb/phone/PlaylistActivity;)Lcom/google/android/youtube/app/ui/v;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/ui/v;->a(Z)V

    return-void
.end method
