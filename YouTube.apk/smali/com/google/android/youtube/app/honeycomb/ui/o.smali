.class final Lcom/google/android/youtube/app/honeycomb/ui/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bk;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/ui/j;

.field private final b:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/honeycomb/ui/j;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/o;->a:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/ui/o;->b:Ljava/lang/String;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/honeycomb/ui/j;Ljava/lang/String;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/ui/o;-><init>(Lcom/google/android/youtube/app/honeycomb/ui/j;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/o;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/o;->a:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/ui/j;->a(Lcom/google/android/youtube/app/honeycomb/ui/j;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/o;->a:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/j;->g(Lcom/google/android/youtube/app/honeycomb/ui/j;)Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/o;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/o;->a:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-static {v2}, Lcom/google/android/youtube/app/honeycomb/ui/j;->f(Lcom/google/android/youtube/app/honeycomb/ui/j;)Lcom/google/android/youtube/core/async/n;

    move-result-object v2

    invoke-interface {v0, v1, p1, v2}, Lcom/google/android/youtube/core/client/bc;->a(Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 3

    const-string v0, "Error while authenticating"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/o;->a:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/j;->g(Lcom/google/android/youtube/app/honeycomb/ui/j;)Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/o;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/o;->a:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-static {v2}, Lcom/google/android/youtube/app/honeycomb/ui/j;->f(Lcom/google/android/youtube/app/honeycomb/ui/j;)Lcom/google/android/youtube/core/async/n;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/bc;->a(Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method

.method public final g_()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/ui/o;->a:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/ui/j;->g(Lcom/google/android/youtube/app/honeycomb/ui/j;)Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/o;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/o;->a:Lcom/google/android/youtube/app/honeycomb/ui/j;

    invoke-static {v2}, Lcom/google/android/youtube/app/honeycomb/ui/j;->f(Lcom/google/android/youtube/app/honeycomb/ui/j;)Lcom/google/android/youtube/core/async/n;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/bc;->a(Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V

    return-void
.end method
