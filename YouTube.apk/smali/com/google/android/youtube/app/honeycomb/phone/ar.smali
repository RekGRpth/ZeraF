.class final Lcom/google/android/youtube/app/honeycomb/phone/ar;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/by;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/ImageView;

.field private final d:Landroid/widget/TextView;

.field private e:Lcom/google/android/youtube/app/remote/bb;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;)V
    .locals 3

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ar;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->z(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040045

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ar;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ar;->b:Landroid/view/View;

    const v1, 0x7f070035

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ar;->c:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ar;->b:Landroid/view/View;

    const v1, 0x7f07002c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ar;->d:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public final a(I)Landroid/view/View;
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ar;->d:Landroid/widget/TextView;

    const v1, 0x7f0b023d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ar;->c:Landroid/widget/ImageView;

    const v1, 0x7f020125

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ar;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ar;->b:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ar;->d:Landroid/widget/TextView;

    const-string v3, "REMOTE"

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Lcom/google/android/youtube/app/honeycomb/phone/ab;Landroid/view/View;Landroid/widget/TextView;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ar;->b:Landroid/view/View;

    return-object v0
.end method

.method public final a(Lcom/google/android/youtube/app/remote/bb;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ar;->e:Lcom/google/android/youtube/app/remote/bb;

    return-void
.end method

.method public final a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
