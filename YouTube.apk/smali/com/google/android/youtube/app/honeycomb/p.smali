.class public final Lcom/google/android/youtube/app/honeycomb/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Landroid/content/SharedPreferences;

.field private final c:Lcom/google/android/youtube/core/Analytics;

.field private final d:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final e:Lcom/google/android/youtube/core/e;

.field private final f:Landroid/preference/CheckBoxPreference;

.field private final g:Landroid/preference/CheckBoxPreference;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private j:Lcom/google/android/youtube/app/honeycomb/q;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Landroid/preference/CheckBoxPreference;Landroid/preference/CheckBoxPreference;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "activity cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/p;->a:Landroid/app/Activity;

    const-string v0, "prefetchSubscriptionsPreference cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/p;->f:Landroid/preference/CheckBoxPreference;

    const-string v0, "prefetchWatchLaterPreference cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/p;->g:Landroid/preference/CheckBoxPreference;

    const v0, 0x7f0b00be

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/p;->h:Ljava/lang/String;

    const v0, 0x7f0b00bf

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/p;->i:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->S()Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/p;->b:Landroid/content/SharedPreferences;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/p;->c:Lcom/google/android/youtube/core/Analytics;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/p;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/p;->e:Lcom/google/android/youtube/core/e;

    invoke-virtual {p2, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    invoke-virtual {p2, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-virtual {p3, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    invoke-virtual {p3, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-void
.end method

.method private a(Landroid/preference/CheckBoxPreference;)V
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/p;->h:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/p;->c:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "PrefetchSubscriptionsOn"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/p;->d:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/p;->e:Lcom/google/android/youtube/core/e;

    const v1, 0x7f0b014f

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/e;->a(I)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/p;->i:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/p;->c:Lcom/google/android/youtube/core/Analytics;

    const-string v1, "PrefetchWatchLaterOn"

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/p;Landroid/preference/CheckBoxPreference;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/p;->a(Landroid/preference/CheckBoxPreference;)V

    return-void
.end method


# virtual methods
.method public final a()Landroid/app/Dialog;
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/p;->j:Lcom/google/android/youtube/app/honeycomb/q;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/q;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/youtube/app/honeycomb/q;-><init>(Lcom/google/android/youtube/app/honeycomb/p;B)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/p;->j:Lcom/google/android/youtube/app/honeycomb/q;

    :cond_0
    new-instance v0, Lcom/google/android/youtube/core/ui/w;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/p;->a:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/ui/w;-><init>(Landroid/content/Context;)V

    const v1, 0x1040014

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/w;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0b0149

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/p;->j:Lcom/google/android/youtube/app/honeycomb/q;

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 3

    const-string v0, "prefetch_pref_key"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/p;->j:Lcom/google/android/youtube/app/honeycomb/q;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/p;->h:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/p;->f:Landroid/preference/CheckBoxPreference;

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/honeycomb/q;->a(Landroid/preference/CheckBoxPreference;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/p;->g:Landroid/preference/CheckBoxPreference;

    goto :goto_0
.end method

.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    check-cast p1, Landroid/preference/CheckBoxPreference;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/p;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/p;->a:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/youtube/app/prefetch/PrefetchService;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/p;->a(Landroid/preference/CheckBoxPreference;)V

    :cond_0
    :goto_1
    return v1

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "prefetch_pref_key"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/p;->a:Landroid/app/Activity;

    const/16 v3, 0x401

    invoke-virtual {v2, v3, v0}, Landroid/app/Activity;->showDialog(ILandroid/os/Bundle;)Z

    goto :goto_1

    :cond_3
    invoke-virtual {p1, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/p;->h:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/p;->c:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "PrefetchSubscriptionsOff"

    invoke-interface {v0, v2}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/p;->i:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/p;->c:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "PrefetchWatchLaterOff"

    invoke-interface {v0, v2}, Lcom/google/android/youtube/core/Analytics;->b(Ljava/lang/String;)V

    goto :goto_1
.end method
