.class public final Lcom/google/android/youtube/app/honeycomb/phone/w;
.super Lcom/google/android/youtube/app/honeycomb/phone/x;
.source "SourceFile"


# instance fields
.field private b:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private final c:Landroid/view/LayoutInflater;

.field private final d:Landroid/view/View;

.field private final e:Lcom/google/android/youtube/app/ui/cw;

.field private final f:Lcom/google/android/youtube/core/client/bc;

.field private final g:Lcom/google/android/youtube/core/client/be;

.field private final h:Lcom/google/android/youtube/app/d;

.field private final i:Lcom/google/android/youtube/core/e;

.field private final j:Lcom/google/android/youtube/core/Analytics;

.field private final k:Lcom/google/android/youtube/core/a/i;

.field private final l:Landroid/widget/ListView;

.field private m:Lcom/google/android/youtube/app/ui/ChannelStoreOutline;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/app/ui/cw;)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/x;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->c:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->c:Landroid/view/LayoutInflater;

    const v2, 0x7f040023

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/w;->o()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->d:Landroid/view/View;

    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->e:Lcom/google/android/youtube/app/ui/cw;

    new-instance v0, Lcom/google/android/youtube/core/a/i;

    invoke-direct {v0}, Lcom/google/android/youtube/core/a/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->k:Lcom/google/android/youtube/core/a/i;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->d:Landroid/view/View;

    const v1, 0x7f07007e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->l:Landroid/widget/ListView;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->f:Lcom/google/android/youtube/core/client/bc;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->b:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->h:Lcom/google/android/youtube/app/d;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->g:Lcom/google/android/youtube/core/client/be;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->i:Lcom/google/android/youtube/core/e;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->j:Lcom/google/android/youtube/core/Analytics;

    return-void
.end method

.method private n()V
    .locals 10

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v4

    check-cast v4, Lcom/google/android/youtube/app/YouTubeApplication;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->j:Lcom/google/android/youtube/core/Analytics;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->f:Lcom/google/android/youtube/core/client/bc;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->b:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->h:Lcom/google/android/youtube/app/d;

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->i:Lcom/google/android/youtube/core/e;

    iget-object v7, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->g:Lcom/google/android/youtube/core/client/be;

    iget-object v8, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->e:Lcom/google/android/youtube/app/ui/cw;

    iget-object v9, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->d:Landroid/view/View;

    invoke-static/range {v0 .. v9}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/app/YouTubeApplication;Lcom/google/android/youtube/app/d;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/app/ui/cw;Landroid/view/View;)Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->m:Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->k:Lcom/google/android/youtube/core/a/i;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->m:Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/a/i;->b(Lcom/google/android/youtube/core/a/e;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->l:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->k:Lcom/google/android/youtube/core/a/i;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/w;->n()V

    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/w;->n()V

    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->m:Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->m:Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->a(Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->m:Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->m:Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->a(Landroid/net/Uri;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/Subscription;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->m:Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->m:Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->a(Lcom/google/android/youtube/core/model/Subscription;)V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->m:Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->m:Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->b()V

    :cond_0
    return-void
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->m:Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/w;->m:Lcom/google/android/youtube/app/ui/ChannelStoreOutline;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/ChannelStoreOutline;->b(Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method public final g()V
    .locals 0

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/x;->g()V

    return-void
.end method
