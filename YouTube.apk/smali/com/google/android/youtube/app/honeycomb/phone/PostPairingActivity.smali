.class public Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/bk;


# static fields
.field private static final n:Ljava/util/Map;


# instance fields
.field private A:Lcom/google/android/youtube/app/YouTubeApplication;

.field private B:Landroid/view/View;

.field private o:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private p:Lcom/google/android/youtube/core/model/UserAuth;

.field private q:Lcom/google/android/youtube/core/client/bc;

.field private r:Lcom/google/android/youtube/core/client/be;

.field private s:Lcom/google/android/youtube/core/e;

.field private t:Lcom/google/android/youtube/core/utils/p;

.field private u:Landroid/view/View;

.field private v:Landroid/widget/TextView;

.field private w:Landroid/widget/ImageView;

.field private x:Lcom/google/android/youtube/app/adapter/k;

.field private y:Lcom/google/android/youtube/app/remote/ad;

.field private z:Lcom/google/android/youtube/app/remote/RemoteControl;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->n:Ljava/util/Map;

    invoke-static {}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->values()[Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    sget-object v4, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->n:Ljava/util/Map;

    iget v5, v3, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->position:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;)Lcom/google/android/youtube/app/remote/RemoteControl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->z:Lcom/google/android/youtube/app/remote/RemoteControl;

    return-object v0
.end method

.method private a(Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;)V
    .locals 6

    const/4 v2, 0x0

    const/16 v3, 0xf

    new-instance v5, Lcom/google/android/youtube/app/honeycomb/phone/bw;

    invoke-direct {v5, p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/bw;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;)V

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/bv;->a:[I

    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->q:Lcom/google/android/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->p:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-static {p0, v5}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v2

    invoke-interface {v0, v1, v3, v2}, Lcom/google/android/youtube/core/client/bc;->a(Lcom/google/android/youtube/core/model/UserAuth;ILcom/google/android/youtube/core/async/n;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->q:Lcom/google/android/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->p:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-static {p0, v5}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v2

    invoke-interface {v0, v1, v3, v2}, Lcom/google/android/youtube/core/client/bc;->d(Lcom/google/android/youtube/core/model/UserAuth;ILcom/google/android/youtube/core/async/n;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->q:Lcom/google/android/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->p:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-static {p0, v5}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v2

    invoke-interface {v0, v1, v3, v2}, Lcom/google/android/youtube/core/client/bc;->b(Lcom/google/android/youtube/core/model/UserAuth;ILcom/google/android/youtube/core/async/n;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->q:Lcom/google/android/youtube/core/client/bc;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->p:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-static {p0, v5}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v2

    invoke-interface {v0, v1, v3, v2}, Lcom/google/android/youtube/core/client/bc;->c(Lcom/google/android/youtube/core/model/UserAuth;ILcom/google/android/youtube/core/async/n;)V

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->q:Lcom/google/android/youtube/core/client/bc;

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;->MOST_POPULAR:Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->A:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;->TODAY:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/youtube/core/client/bc;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->q:Lcom/google/android/youtube/core/client/bc;

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;->MOST_POPULAR:Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;

    const-string v2, "Music"

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->A:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;->TODAY:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/youtube/core/client/bc;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->q:Lcom/google/android/youtube/core/client/bc;

    sget-object v1, Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;->MOST_POPULAR:Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;

    sget-object v4, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;->TODAY:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    move-object v3, v2

    invoke-interface/range {v0 .. v5}, Lcom/google/android/youtube/core/client/bc;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;)V
    .locals 2

    iget v0, p1, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->position:I

    invoke-static {}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->values()[Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    move-result-object v1

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    const-string v0, "We run out of feeds! How?"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->n:Ljava/util/Map;

    iget v1, p1, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->position:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->p:Lcom/google/android/youtube/core/model/UserAuth;

    if-nez v1, :cond_1

    iget-boolean v1, v0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->isAccountFeed:Z

    if-eqz v1, :cond_1

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->TRENDING:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->a(Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;)Lcom/google/android/youtube/app/adapter/k;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->x:Lcom/google/android/youtube/app/adapter/k;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->v:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->w:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->B:Landroid/view/View;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->p:Lcom/google/android/youtube/core/model/UserAuth;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->WATCH_LATER:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->a(Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;)V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    const-string v0, "Authentication error"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->TRENDING:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->a(Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;)V

    return-void
.end method

.method public final g_()V
    .locals 1

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->TRENDING:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->a(Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f04006c

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->setContentView(I)V

    const v0, 0x7f0b0236

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->b(I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->A:Lcom/google/android/youtube/app/YouTubeApplication;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->A:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->r:Lcom/google/android/youtube/core/client/be;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->A:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->q:Lcom/google/android/youtube/core/client/bc;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->A:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->s:Lcom/google/android/youtube/core/e;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->A:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->o:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->A:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->l()Lcom/google/android/youtube/core/utils/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->t:Lcom/google/android/youtube/core/utils/p;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->A:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->K()Lcom/google/android/youtube/app/remote/ad;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->y:Lcom/google/android/youtube/app/remote/ad;

    const v0, 0x7f070105

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->u:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->u:Landroid/view/View;

    const v1, 0x7f07002c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->v:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->u:Landroid/view/View;

    const v1, 0x7f070035

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->w:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->u:Landroid/view/View;

    const v1, 0x7f070106

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->B:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->u:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0800af

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0800ae

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0800b0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v1, v6, v2, v3}, Landroid/view/View;->setPadding(IIII)V

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/bu;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->r:Lcom/google/android/youtube/core/client/be;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->t:Lcom/google/android/youtube/core/utils/p;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/honeycomb/phone/bu;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory$ThumbnailSize;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->w:Landroid/widget/ImageView;

    invoke-virtual {v0, v1, v5}, Lcom/google/android/youtube/app/adapter/ThumbnailRendererFactory;->a(Landroid/view/View;Landroid/view/ViewGroup;)Lcom/google/android/youtube/app/adapter/bl;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/adapter/k;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->x:Lcom/google/android/youtube/app/adapter/k;

    const v0, 0x7f070103

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/Util;->g(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    const v0, 0x7f070104

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/Util;->g(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    invoke-virtual {p0, v6}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->k(Z)V

    return-void
.end method

.method public onStart()V
    .locals 5

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStart()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->y:Lcom/google/android/youtube/app/remote/ad;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/ad;->b()Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->z:Lcom/google/android/youtube/app/remote/RemoteControl;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->z:Lcom/google/android/youtube/app/remote/RemoteControl;

    if-nez v0, :cond_1

    const-string v0, "Ooops! We should be connected a route but that\'s not the case!"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->o:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bk;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->z:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v0}, Lcom/google/android/youtube/app/remote/RemoteControl;->t()Lcom/google/android/youtube/app/remote/bb;

    move-result-object v0

    if-eqz v0, :cond_0

    const v0, 0x7f070103

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0b0233

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->z:Lcom/google/android/youtube/app/remote/RemoteControl;

    invoke-interface {v4}, Lcom/google/android/youtube/app/remote/RemoteControl;->t()Lcom/google/android/youtube/app/remote/bb;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/youtube/app/remote/bb;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected final w()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method
