.class public abstract Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/youtube/app/compat/h;
.implements Lcom/google/android/youtube/app/honeycomb/ui/i;
.implements Lcom/google/android/youtube/app/remote/ah;
.implements Lcom/google/android/youtube/app/ui/bp;
.implements Lcom/google/android/youtube/app/ui/bq;
.implements Lcom/google/android/youtube/app/ui/dq;
.implements Lcom/google/android/youtube/app/ui/ep;
.implements Lcom/google/android/youtube/core/async/bk;
.implements Lcom/google/android/youtube/core/player/v;
.implements Lcom/google/android/youtube/core/utils/i;
.implements Lcom/google/android/youtube/core/utils/k;
.implements Lcom/google/android/youtube/core/utils/o;
.implements Lcom/google/android/youtube/core/v11/ui/c;


# instance fields
.field private A:Lcom/google/android/youtube/core/player/PlayerView;

.field private B:Lcom/google/android/youtube/core/utils/HdmiReceiver;

.field private C:Lcom/google/android/youtube/core/utils/DockReceiver;

.field private D:Z

.field private E:Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;

.field private F:Landroid/content/SharedPreferences;

.field private G:Lcom/google/android/youtube/core/async/UserAuthorizer;

.field private H:Lcom/google/android/youtube/core/player/Director;

.field private I:Lcom/google/android/youtube/core/async/a/c;

.field private J:Lcom/google/android/youtube/core/player/Director$DirectorState;

.field private K:I

.field private L:Lcom/google/android/youtube/app/honeycomb/h;

.field private M:Lcom/google/android/youtube/app/honeycomb/j;

.field private N:Lcom/google/android/youtube/core/v11/ui/b;

.field private O:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;

.field private P:Lcom/google/android/youtube/core/Analytics;

.field private Q:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

.field private R:Lcom/google/android/youtube/core/player/overlay/a;

.field private S:Z

.field private T:Z

.field private U:Ljava/lang/String;

.field private V:Ljava/lang/String;

.field private W:Z

.field private X:Ljava/lang/String;

.field private Y:Z

.field private Z:Landroid/widget/Toast;

.field private aa:Z

.field private ab:Z

.field private ac:Z

.field private ad:Z

.field private ae:Lcom/google/android/youtube/core/client/bc;

.field private af:Lcom/google/android/youtube/core/client/bo;

.field private ag:Lcom/google/android/youtube/core/client/bg;

.field private ah:Lcom/google/android/youtube/gmsplus1/f;

.field private ai:Lcom/google/android/youtube/gmsplus1/i;

.field private aj:Lcom/google/android/youtube/app/remote/ad;

.field private ak:Lcom/google/android/youtube/app/remote/bf;

.field private al:Lcom/google/android/youtube/app/YouTubeApplication;

.field private am:Lcom/google/android/youtube/app/k;

.field private an:Lcom/google/android/youtube/app/honeycomb/phone/dg;

.field private ao:Lcom/google/android/youtube/app/remote/br;

.field private ap:Landroid/app/KeyguardManager;

.field private aq:Z

.field private ar:I

.field private as:I

.field private at:I

.field private au:I

.field private av:I

.field private aw:Lcom/google/android/youtube/core/async/o;

.field protected n:Lcom/google/android/youtube/app/ui/bh;

.field private o:Landroid/media/AudioManager;

.field private p:Landroid/view/ViewGroup;

.field private q:Landroid/widget/ProgressBar;

.field private r:Landroid/widget/ImageView;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/widget/Button;

.field private u:Lcom/google/android/youtube/app/ui/dh;

.field private v:Lcom/google/android/youtube/core/e;

.field private w:Lcom/google/android/youtube/core/utils/p;

.field private x:Lcom/google/android/youtube/app/d;

.field private y:Lcom/google/android/youtube/app/compat/SupportActionBar;

.field private z:Lcom/google/android/youtube/core/player/as;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->U:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->V:Ljava/lang/String;

    return-void
.end method

.method private N()V
    .locals 2

    new-instance v0, Lcom/google/android/youtube/core/async/o;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/async/o;-><init>(Lcom/google/android/youtube/core/async/bk;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->aw:Lcom/google/android/youtube/core/async/o;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->O()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->G:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->aw:Lcom/google/android/youtube/core/async/o;

    invoke-virtual {v0, p0, v1}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->G:Lcom/google/android/youtube/core/async/UserAuthorizer;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->aw:Lcom/google/android/youtube/core/async/o;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bk;)V

    goto :goto_0
.end method

.method private O()Z
    .locals 3

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "authenticate"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private P()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->j()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->u()Lcom/google/android/youtube/core/player/Director$DirectorState;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->J:Lcom/google/android/youtube/core/player/Director$DirectorState;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->I:Lcom/google/android/youtube/core/async/a/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->I:Lcom/google/android/youtube/core/async/a/c;

    invoke-interface {v0}, Lcom/google/android/youtube/core/async/a/c;->h()I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->K:I

    return-void

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private Q()V
    .locals 2

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->T:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->S:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ac:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ad:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->n:Lcom/google/android/youtube/app/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bh;->r()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->h()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->N:Lcom/google/android/youtube/core/v11/ui/b;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/v11/ui/b;->b(Z)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->M()Lcom/google/android/youtube/app/honeycomb/ui/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/ui/r;->a()V

    :cond_0
    return-void
.end method

.method private R()V
    .locals 5

    const v0, 0x7f0700f1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->as:I

    iget v2, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->at:I

    iget v3, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ar:I

    add-int/2addr v2, v3

    iget v3, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->au:I

    iget v4, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->av:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    return-void
.end method

.method private S()I
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [I

    const v2, 0x10102eb

    aput v2, v1, v3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0d0024

    sget-object v1, Lcom/google/android/youtube/c;->i:[I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->obtainStyledAttributes(I[I)Landroid/content/res/TypedArray;

    move-result-object v1

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0
.end method

.method private T()V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->W:Z

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/android/youtube/core/player/Director;->h(Z)V

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->Q:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->n()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->C:Lcom/google/android/youtube/core/utils/DockReceiver;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/DockReceiver;->c()I

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setLockedInFullscreen(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->B:Lcom/google/android/youtube/core/utils/HdmiReceiver;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/HdmiReceiver;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->C:Lcom/google/android/youtube/core/utils/DockReceiver;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/DockReceiver;->c()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    if-eqz v2, :cond_5

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->O:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->disable()V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->setRequestedOrientation(I)V

    :cond_2
    :goto_2
    return-void

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->W:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->setRequestedOrientation(I)V

    goto :goto_2
.end method

.method private U()V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->p:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->q:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->r:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->t:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->f(Z)V

    return-void
.end method

.method public static a(Landroid/app/Activity;Landroid/net/Uri;IZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;)Landroid/content/Intent;
    .locals 3

    new-instance v1, Landroid/content/Intent;

    invoke-static {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(Landroid/app/Activity;)Ljava/lang/Class;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "playlist_uri"

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "playlist_start_position"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "authenticate"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "feature"

    invoke-virtual {p4}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/app/Activity;Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;)Landroid/content/Intent;
    .locals 3

    new-instance v1, Landroid/content/Intent;

    invoke-static {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(Landroid/app/Activity;)Ljava/lang/Class;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "video_id"

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "unfavorite_uri"

    invoke-static {p2}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "feature"

    invoke-virtual {p3}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;)Landroid/content/Intent;
    .locals 3

    new-instance v1, Landroid/content/Intent;

    invoke-static {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(Landroid/app/Activity;)Ljava/lang/Class;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "video_id"

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "feature"

    invoke-virtual {p2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/app/Activity;Ljava/lang/String;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;)Landroid/content/Intent;
    .locals 2

    invoke-static {p0, p1, p3}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "authenticate"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;)Lcom/google/android/youtube/app/remote/ad;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->aj:Lcom/google/android/youtube/app/remote/ad;

    return-object v0
.end method

.method private a(Ljava/util/List;ILcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/a/c;
    .locals 2

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ae:Lcom/google/android/youtube/core/client/bc;

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0, p3}, Lcom/google/android/youtube/core/async/a/b;->a(Lcom/google/android/youtube/core/client/bc;Ljava/lang/String;Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ae:Lcom/google/android/youtube/core/client/bc;

    invoke-static {v0, p1, p3, p2}, Lcom/google/android/youtube/core/async/a/b;->a(Lcom/google/android/youtube/core/client/bc;Ljava/util/List;Lcom/google/android/youtube/core/model/UserAuth;I)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Landroid/app/Activity;)Ljava/lang/Class;
    .locals 1

    invoke-virtual {p0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    invoke-static {p0}, Lcom/google/android/youtube/core/utils/j;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/google/android/youtube/app/honeycomb/tablet/WatchActivity;

    :goto_0
    return-object v0

    :cond_0
    const-class v0, Lcom/google/android/youtube/app/honeycomb/phone/WatchActivity;

    goto :goto_0
.end method

.method private a(Landroid/content/Intent;Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 1

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/a;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/a;-><init>(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;Landroid/content/Intent;Lcom/google/android/youtube/core/model/UserAuth;)V

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;Landroid/content/Intent;Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 6

    const v2, 0x7f0b0013

    invoke-static {}, Lcom/google/android/youtube/core/utils/s;->a()V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.media.action.MEDIA_PLAY_FROM_SEARCH"

    if-ne v0, v1, :cond_1

    const-string v0, "query"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->X:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->X:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "empty search query"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->v:Lcom/google/android/youtube/core/e;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/e;->a(I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ae:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bc;->e()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ae:Lcom/google/android/youtube/core/client/bc;

    invoke-interface {v1}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->X:Ljava/lang/String;

    sget-object v3, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;->ALL_TIME:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/i;

    invoke-direct {v2, p0, p2}, Lcom/google/android/youtube/app/honeycomb/i;-><init>(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;Lcom/google/android/youtube/core/model/UserAuth;)V

    invoke-static {p0, v2}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/async/au;->a(Ljava/lang/Object;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0

    :cond_1
    const-string v0, "artist_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v0, "empty artistId"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->v:Lcom/google/android/youtube/core/e;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/core/e;->a(I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->finish()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ag:Lcom/google/android/youtube/core/client/bg;

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/g;

    invoke-direct {v2, p0, p2}, Lcom/google/android/youtube/app/honeycomb/g;-><init>(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;Lcom/google/android/youtube/core/model/UserAuth;)V

    invoke-static {p0, v2}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/youtube/core/client/bg;->b(Ljava/lang/String;Lcom/google/android/youtube/core/async/n;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    const-string v0, "video_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "playlist_uri"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    const-string v1, "playlist_start_position"

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    const/4 v3, 0x0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(Lcom/google/android/youtube/core/model/UserAuth;Landroid/net/Uri;Ljava/util/List;Landroid/net/Uri;I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;Lcom/google/android/youtube/core/model/UserAuth;Landroid/net/Uri;Ljava/util/List;Landroid/net/Uri;I)V
    .locals 6

    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move-object v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(Lcom/google/android/youtube/core/model/UserAuth;Landroid/net/Uri;Ljava/util/List;Landroid/net/Uri;I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->c(Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/google/android/youtube/core/model/UserAuth;Landroid/net/Uri;Ljava/util/List;Landroid/net/Uri;I)V
    .locals 9

    const/4 v2, 0x0

    const v8, 0x7f0b0013

    const/4 v3, 0x0

    invoke-static {}, Lcom/google/android/youtube/core/utils/s;->a()V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->p()V

    iget v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->K:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget p5, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->K:I

    :cond_0
    if-eqz p3, :cond_2

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0, p3, p5, p1}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(Ljava/util/List;ILcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->I:Lcom/google/android/youtube/core/async/a/c;

    move v4, v3

    move-object v5, v2

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->J:Lcom/google/android/youtube/core/player/Director$DirectorState;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->I:Lcom/google/android/youtube/core/async/a/c;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->J:Lcom/google/android/youtube/core/player/Director$DirectorState;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;Lcom/google/android/youtube/core/player/Director$DirectorState;)V

    iput-object v2, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->J:Lcom/google/android/youtube/core/player/Director$DirectorState;

    :goto_1
    return-void

    :cond_2
    if-eqz p4, :cond_4

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ae:Lcom/google/android/youtube/core/client/bc;

    if-nez p1, :cond_3

    invoke-interface {v1}, Lcom/google/android/youtube/core/client/bc;->b()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    invoke-interface {v1}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v1

    invoke-virtual {v1, p4}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->f(Landroid/net/Uri;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    :goto_2
    new-instance v4, Lcom/google/android/youtube/core/async/a/g;

    invoke-direct {v4, v0, v1, p5}, Lcom/google/android/youtube/core/async/a/g;-><init>(Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/async/GDataRequest;I)V

    iput-object v4, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->I:Lcom/google/android/youtube/core/async/a/c;

    move v4, v3

    move-object v5, v2

    goto :goto_0

    :cond_3
    invoke-interface {v1}, Lcom/google/android/youtube/core/client/bc;->j()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    invoke-interface {v1}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v1

    invoke-virtual {v1, p4, p1}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->c(Landroid/net/Uri;Lcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v1

    goto :goto_2

    :cond_4
    if-eqz p2, :cond_5

    :try_start_0
    invoke-static {p2}, Lcom/google/android/youtube/core/utils/ak;->a(Landroid/net/Uri;)Lcom/google/android/youtube/core/utils/ak;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/youtube/core/utils/ak;->a:Ljava/util/List;

    invoke-direct {p0, v1, p5, p1}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(Ljava/util/List;ILcom/google/android/youtube/core/model/UserAuth;)Lcom/google/android/youtube/core/async/a/c;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->I:Lcom/google/android/youtube/core/async/a/c;

    iget v4, v0, Lcom/google/android/youtube/core/utils/ak;->c:I

    iget-object v5, v0, Lcom/google/android/youtube/core/utils/ak;->b:Ljava/util/Map;

    const-string v1, "youtube_tv_uid"

    invoke-virtual {v6, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/google/android/youtube/core/utils/ak;->a:Ljava/util/List;

    const/4 v7, 0x0

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/ytremote/model/SsdpId;

    invoke-direct {v0, v1}, Lcom/google/android/ytremote/model/SsdpId;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ao:Lcom/google/android/youtube/app/remote/br;

    new-instance v7, Lcom/google/android/youtube/app/honeycomb/b;

    invoke-direct {v7, p0}, Lcom/google/android/youtube/app/honeycomb/b;-><init>(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;)V

    invoke-virtual {v1, v0, v7}, Lcom/google/android/youtube/app/remote/br;->a(Lcom/google/android/ytremote/model/SsdpId;Lcom/google/android/youtube/app/remote/cd;)V
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "invalid intercepted Uri: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->v:Lcom/google/android/youtube/core/e;

    invoke-virtual {v0, v8}, Lcom/google/android/youtube/core/e;->a(I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->finish()V

    goto :goto_1

    :cond_5
    const-string v0, "invalid intent format"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->v:Lcom/google/android/youtube/core/e;

    invoke-virtual {v0, v8}, Lcom/google/android/youtube/core/e;->a(I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->finish()V

    goto/16 :goto_1

    :cond_6
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->I:Lcom/google/android/youtube/core/async/a/c;

    invoke-virtual {v6}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_9

    const-string v7, "feature"

    invoke-virtual {v2, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, "feature"

    sget-object v7, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->NO_FEATURE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-virtual {v7}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->ordinal()I

    move-result v7

    invoke-virtual {v6, v2, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    if-ltz v2, :cond_7

    invoke-static {}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->values()[Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    move-result-object v6

    array-length v6, v6

    if-lt v2, v6, :cond_8

    :cond_7
    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->NO_FEATURE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->ordinal()I

    move-result v2

    :cond_8
    invoke-static {}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->values()[Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    move-result-object v6

    aget-object v2, v6, v2

    :goto_3
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/async/a/a;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;ZILjava/util/Map;)V

    goto/16 :goto_1

    :cond_9
    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->EXTERNAL_URL:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    goto :goto_3
.end method

.method private a(Lcom/google/android/youtube/core/model/Video;Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->u:Lcom/google/android/youtube/app/ui/dh;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/app/ui/dh;->a(Lcom/google/android/youtube/core/model/Video;Landroid/net/Uri;)V

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/google/android/youtube/app/ui/dh;->a(Lcom/google/android/youtube/core/model/Video;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ab:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;)Landroid/content/Intent;
    .locals 3

    new-instance v1, Landroid/content/Intent;

    invoke-static {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(Landroid/app/Activity;)Ljava/lang/Class;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "artist_id"

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "feature"

    invoke-virtual {p2}, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;)Lcom/google/android/youtube/app/remote/bf;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ak:Lcom/google/android/youtube/app/remote/bf;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;)Lcom/google/android/youtube/app/honeycomb/phone/dg;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->an:Lcom/google/android/youtube/app/honeycomb/phone/dg;

    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->p:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->q:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->r:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->s:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->t:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->f(Z)V

    return-void
.end method

.method static synthetic d(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;)Lcom/google/android/youtube/core/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->v:Lcom/google/android/youtube/core/e;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;)Lcom/google/android/youtube/core/player/Director;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;)Lcom/google/android/youtube/core/player/as;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->z:Lcom/google/android/youtube/core/player/as;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;)Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->Q:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->X:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;)Lcom/google/android/youtube/app/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->x:Lcom/google/android/youtube/app/d;

    return-object v0
.end method

.method private l(Z)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->S()I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ar:I

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->R()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final A()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->p:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->f(Z)V

    return-void
.end method

.method protected final B()Lcom/google/android/youtube/core/player/PlayerView;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->A:Lcom/google/android/youtube/core/player/PlayerView;

    return-object v0
.end method

.method protected final C()Lcom/google/android/youtube/app/ui/dh;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->u:Lcom/google/android/youtube/app/ui/dh;

    return-object v0
.end method

.method protected final a(I)Landroid/app/Dialog;
    .locals 1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :sswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->u:Lcom/google/android/youtube/app/ui/dh;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/dh;->a(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x3e8 -> :sswitch_0
        0x3ed -> :sswitch_0
        0x3f4 -> :sswitch_0
        0x3f7 -> :sswitch_0
        0x3ff -> :sswitch_0
    .end sparse-switch
.end method

.method public final a()V
    .locals 0

    return-void
.end method

.method protected final a(IIII)V
    .locals 1

    iput p1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->as:I

    iput p2, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->at:I

    iput p3, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->au:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->av:I

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->R()V

    return-void
.end method

.method protected final a(Lcom/google/android/youtube/app/YouTubeApplication;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/YouTubeApplication;)V

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->U()Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->G:Lcom/google/android/youtube/core/async/UserAuthorizer;

    return-void
.end method

.method public a(Lcom/google/android/youtube/app/remote/RemoteControl;Z)V
    .locals 5

    const/4 v4, 0x1

    const/4 v1, 0x0

    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/remote/RemoteControl;Z)V

    if-nez p1, :cond_4

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->n:Lcom/google/android/youtube/app/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bh;->s()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->n:Lcom/google/android/youtube/app/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bh;->u()Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    move-result-object v2

    if-eqz v2, :cond_0

    sget-object v3, Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;->ENDED:Lcom/google/android/youtube/app/remote/RemoteControl$RemotePlayerState;

    if-eq v2, v3, :cond_5

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bh;->t()I

    move-result v0

    :goto_0
    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/core/player/Director;->a(I)V

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->aq:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ap:Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p2, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->D:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->v_()V

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->n:Lcom/google/android/youtube/app/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bh;->m()V

    iput-boolean v4, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->aa:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->an:Lcom/google/android/youtube/app/honeycomb/phone/dg;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/dg;->b(Z)V

    :cond_2
    :goto_2
    return-void

    :cond_3
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/Director;->b(Z)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->n:Lcom/google/android/youtube/app/ui/bh;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/bh;->a(Lcom/google/android/youtube/app/remote/RemoteControl;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->an:Lcom/google/android/youtube/app/honeycomb/phone/dg;

    invoke-virtual {v0, v4}, Lcom/google/android/youtube/app/honeycomb/phone/dg;->b(Z)V

    iput-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->aa:Z

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method public a(Lcom/google/android/youtube/core/model/Branding;)V
    .locals 0

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ai:Lcom/google/android/youtube/gmsplus1/i;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ah:Lcom/google/android/youtube/gmsplus1/f;

    iget-object v2, p1, Lcom/google/android/youtube/core/model/UserAuth;->account:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/gmsplus1/i;->a(Lcom/google/android/youtube/gmsplus1/f;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(Landroid/content/Intent;Lcom/google/android/youtube/core/model/UserAuth;)V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/core/model/VastAd;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->an:Lcom/google/android/youtube/app/honeycomb/phone/dg;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/dg;->a(Lcom/google/android/youtube/core/model/VastAd;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0258

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->V:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->T:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->V:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->b(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->N:Lcom/google/android/youtube/core/v11/ui/b;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/v11/ui/b;->b(Z)V

    return-void
.end method

.method public a(Lcom/google/android/youtube/core/model/Video;)V
    .locals 4

    iget-object v0, p1, Lcom/google/android/youtube/core/model/Video;->title:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->U:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->U:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->V:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->T:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->V:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->b(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "unfavorite_uri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(Lcom/google/android/youtube/core/model/Video;Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->n:Lcom/google/android/youtube/app/ui/bh;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/ui/bh;->a(Lcom/google/android/youtube/core/model/Video;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->J()Lcom/google/android/youtube/app/honeycomb/ui/j;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->J()Lcom/google/android/youtube/app/honeycomb/ui/j;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/ui/j;->b(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->E:Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;->b()V

    invoke-virtual {p1}, Lcom/google/android/youtube/core/model/Video;->isLive()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->D:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->G:Lcom/google/android/youtube/core/async/UserAuthorizer;

    new-instance v1, Lcom/google/android/youtube/app/ui/dl;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ae:Lcom/google/android/youtube/core/client/bc;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->v:Lcom/google/android/youtube/core/e;

    invoke-direct {v1, p1, p0, v2, v3}, Lcom/google/android/youtube/app/ui/dl;-><init>(Lcom/google/android/youtube/core/model/Video;Landroid/app/Activity;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/e;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/async/UserAuthorizer;->a(Lcom/google/android/youtube/core/async/bk;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->q()Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->G:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/async/UserAuthorizer;->d()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->w:Lcom/google/android/youtube/core/utils/p;

    invoke-interface {v1}, Lcom/google/android/youtube/core/utils/p;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ah:Lcom/google/android/youtube/gmsplus1/f;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->G:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/async/UserAuthorizer;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/gmsplus1/f;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ai:Lcom/google/android/youtube/gmsplus1/i;

    invoke-virtual {v1}, Lcom/google/android/youtube/gmsplus1/i;->D()Lcom/google/android/gms/plus/a;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/youtube/core/utils/Util;->a(Lcom/google/android/youtube/core/model/Video;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;->a(Lcom/google/android/gms/plus/a;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;->setVisibility(I)V

    :goto_0
    return-void

    :cond_2
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/youtube/core/player/Director$StopReason;)V
    .locals 2

    sget-object v0, Lcom/google/android/youtube/core/player/Director$StopReason;->ITERATOR_FINISHED:Lcom/google/android/youtube/core/player/Director$StopReason;

    if-ne p1, v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->W:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/Director;->g(Z)V

    :cond_0
    :goto_0
    sget-object v0, Lcom/google/android/youtube/core/player/Director$StopReason;->ITERATOR_FINISHED:Lcom/google/android/youtube/core/player/Director$StopReason;

    if-ne p1, v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->Y:Z

    if-eqz v0, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->finish()V

    :cond_2
    return-void

    :cond_3
    sget-object v0, Lcom/google/android/youtube/core/player/Director$StopReason;->AUTOPLAY_DENIED:Lcom/google/android/youtube/core/player/Director$StopReason;

    if-ne p1, v0, :cond_0

    const v0, 0x7f0b0076

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;II)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/youtube/core/player/DirectorException;)V
    .locals 2

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/f;->a:[I

    iget-object v1, p1, Lcom/google/android/youtube/core/player/DirectorException;->reason:Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/DirectorException$ErrorReason;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->aa:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->Z:Landroid/widget/Toast;

    const v1, 0x7f0b01d6

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->Z:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->A:Lcom/google/android/youtube/core/player/PlayerView;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/e;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/e;-><init>(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/PlayerView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Lcom/google/android/youtube/core/player/DirectorException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->n:Lcom/google/android/youtube/app/ui/bh;

    invoke-virtual {p1}, Lcom/google/android/youtube/core/player/DirectorException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/bh;->b(Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->O()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "error authenticating for playlist request"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(Landroid/content/Intent;Lcom/google/android/youtube/core/model/UserAuth;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)Z
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/m;)Z

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->E()Lcom/google/android/youtube/app/compat/r;

    move-result-object v0

    const v1, 0x7f110006

    invoke-virtual {v0, v1, p1}, Lcom/google/android/youtube/app/compat/r;->a(ILcom/google/android/youtube/app/compat/m;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->an:Lcom/google/android/youtube/app/honeycomb/phone/dg;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/dg;->a(Lcom/google/android/youtube/app/compat/m;)V

    const/4 v0, 0x1

    return v0
.end method

.method public final a(Lcom/google/android/youtube/app/compat/t;)Z
    .locals 4

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ab:Z

    if-nez v1, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/t;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-interface {p1}, Lcom/google/android/youtube/app/compat/t;->f()I

    move-result v1

    const v2, 0x7f070194

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->u:Lcom/google/android/youtube/app/ui/dh;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/dh;->a()V

    goto :goto_0

    :cond_1
    const v2, 0x7f070195

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->u:Lcom/google/android/youtube/app/ui/dh;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/dh;->b()V

    goto :goto_0

    :cond_2
    const v2, 0x7f070191

    if-eq v1, v2, :cond_3

    const v2, 0x7f070193

    if-ne v1, v2, :cond_4

    :cond_3
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->u:Lcom/google/android/youtube/app/ui/dh;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/dh;->c()V

    goto :goto_0

    :cond_4
    const v2, 0x7f070190

    if-eq v1, v2, :cond_5

    const v2, 0x7f070192

    if-ne v1, v2, :cond_6

    :cond_5
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->u:Lcom/google/android/youtube/app/ui/dh;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/dh;->f()V

    goto :goto_0

    :cond_6
    const v2, 0x7f070196

    if-ne v1, v2, :cond_7

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->u:Lcom/google/android/youtube/app/ui/dh;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/dh;->d()V

    goto :goto_0

    :cond_7
    const v2, 0x7f070197

    if-ne v1, v2, :cond_8

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->u:Lcom/google/android/youtube/app/ui/dh;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/dh;->e()V

    goto :goto_0

    :cond_8
    const v2, 0x7f07018e

    if-ne v1, v2, :cond_9

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->P:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "LearnMore"

    const-string v3, "Menu"

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/Director;->b()V

    goto :goto_0

    :cond_9
    const v2, 0x7f07018f

    if-ne v1, v2, :cond_a

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->P:Lcom/google/android/youtube/core/Analytics;

    const-string v2, "GotoAd"

    const-string v3, "Menu"

    invoke-interface {v1, v2, v3}, Lcom/google/android/youtube/core/Analytics;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/player/Director;->e()V

    goto :goto_0

    :cond_a
    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/t;)Z

    move-result v0

    goto :goto_0
.end method

.method public final a_(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->W:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->h(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->O:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->enable()V

    return-void
.end method

.method public final b()V
    .locals 0

    return-void
.end method

.method public final b(Lcom/google/android/youtube/core/model/Video;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->an:Lcom/google/android/youtube/app/honeycomb/phone/dg;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/dg;->a(Lcom/google/android/youtube/core/model/Video;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "unfavorite_uri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-direct {p0, p1, v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(Lcom/google/android/youtube/core/model/Video;Landroid/net/Uri;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->U:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->V:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->T:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->V:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 0

    return-void
.end method

.method public final b(Lcom/google/android/youtube/app/compat/m;)Z
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->b(Lcom/google/android/youtube/app/compat/m;)Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->an:Lcom/google/android/youtube/app/honeycomb/phone/dg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/dg;->a()V

    const/4 v0, 0x1

    return v0
.end method

.method public final c(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->O:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->disable()V

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->setRequestedOrientation(I)V

    return-void
.end method

.method public final d(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ac:Z

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->Q()V

    return-void
.end method

.method protected abstract e(Z)V
.end method

.method public final f()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->S:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->D:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->E:Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;->a()V

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->Q()V

    return-void
.end method

.method protected abstract f(Z)V
.end method

.method protected final g(Z)Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setAutoHide(Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setShowFullscreenInPortrait(Z)V

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setShowButtonsWhenNotFullscreen(Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setHideOnTap(Z)V

    return-object v0
.end method

.method public final g()V
    .locals 0

    return-void
.end method

.method public final g_()V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->O()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(Landroid/content/Intent;Lcom/google/android/youtube/core/model/UserAuth;)V

    goto :goto_0
.end method

.method public final h()V
    .locals 0

    return-void
.end method

.method protected final h(Z)V
    .locals 7

    const/4 v6, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    iput-boolean p1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->T:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    iget-boolean v3, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->T:Z

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/core/player/Director;->g(Z)V

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->T:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->f(Z)V

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->T:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->l(Z)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->r()Landroid/view/ViewGroup$MarginLayoutParams;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->N:Lcom/google/android/youtube/core/v11/ui/b;

    iget-boolean v4, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->T:Z

    invoke-interface {v3, v4}, Lcom/google/android/youtube/core/v11/ui/b;->a(Z)V

    iget-boolean v3, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->T:Z

    if-eqz v3, :cond_2

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->V:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->b(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->y:Lcom/google/android/youtube/app/compat/SupportActionBar;

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090009

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v3}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Landroid/graphics/drawable/Drawable;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->Q()V

    iput v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->width:I

    iput v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    invoke-virtual {p0, v2, v2, v2, v2}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(IIII)V

    :goto_2
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->an:Lcom/google/android/youtube/app/honeycomb/phone/dg;

    iget-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->T:Z

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/dg;->a(Z)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->closeOptionsMenu()V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->N:Lcom/google/android/youtube/core/v11/ui/b;

    invoke-interface {v0, v2}, Lcom/google/android/youtube/core/v11/ui/b;->b(Z)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v3, 0x400

    invoke-virtual {v0, v2, v3}, Landroid/view/Window;->setFlags(II)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->D()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->y:Lcom/google/android/youtube/app/compat/SupportActionBar;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02005a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_3

    move v2, v1

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->n()Z

    move-result v0

    if-nez v0, :cond_5

    if-eqz v2, :cond_5

    sget v0, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v2, 0x9

    if-lt v0, v2, :cond_4

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->setRequestedOrientation(I)V

    goto :goto_2

    :cond_4
    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->setRequestedOrientation(I)V

    goto :goto_2

    :cond_5
    invoke-virtual {p0, v2}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->e(Z)V

    goto :goto_2
.end method

.method public h_()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(Lcom/google/android/youtube/core/model/Video;Landroid/net/Uri;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->U()V

    return-void
.end method

.method public final i(Z)V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->T:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->l(Z)V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->N:Lcom/google/android/youtube/core/v11/ui/b;

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/v11/ui/b;->b(Z)V

    :goto_1
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->Q()V

    goto :goto_1
.end method

.method public final i_()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->S:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->E:Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;->b()V

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->T:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->N:Lcom/google/android/youtube/core/v11/ui/b;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/v11/ui/b;->b(Z)V

    :cond_0
    return-void
.end method

.method public final j_()V
    .locals 0

    return-void
.end method

.method public final k_()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->T:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->Q:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->g()V

    :cond_0
    return-void
.end method

.method protected abstract m()I
.end method

.method protected abstract n()Z
.end method

.method protected abstract o()V
.end method

.method public onBackPressed()V
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->W:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/Director;->a(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->U()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->N()V

    goto :goto_0
.end method

.method public final onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->n:Lcom/google/android/youtube/app/ui/bh;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->S()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/youtube/app/ui/bh;->a(Landroid/content/res/Configuration;I)V

    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->c()V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->n()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->h(Z)V

    :cond_1
    iget-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->T:Z

    if-nez v1, :cond_2

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->e(Z)V

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 26

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->L()V

    invoke-super/range {p0 .. p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "force_fullscreen"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->W:Z

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->m()I

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->setContentView(I)V

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->l(Z)V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->getApplication()Landroid/app/Application;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/app/YouTubeApplication;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/app/k;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->am:Lcom/google/android/youtube/app/k;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->i()Lcom/google/android/youtube/core/Analytics;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->P:Lcom/google/android/youtube/core/Analytics;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->S()Landroid/content/SharedPreferences;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->F:Landroid/content/SharedPreferences;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->v:Lcom/google/android/youtube/core/e;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->l()Lcom/google/android/youtube/core/utils/p;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->w:Lcom/google/android/youtube/core/utils/p;

    const v2, 0x7f0700f2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/player/PlayerView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->A:Lcom/google/android/youtube/core/player/PlayerView;

    new-instance v2, Lcom/google/android/youtube/core/player/as;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->A:Lcom/google/android/youtube/core/player/PlayerView;

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/PlayerView;->a()Lcom/google/android/youtube/core/player/af;

    move-result-object v4

    new-instance v5, Lcom/google/android/youtube/app/player/j;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v7}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/app/k;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->w:Lcom/google/android/youtube/core/utils/p;

    invoke-direct {v5, v6, v7, v8}, Lcom/google/android/youtube/app/player/j;-><init>(Lcom/google/android/youtube/app/YouTubeApplication;Lcom/google/android/youtube/app/k;Lcom/google/android/youtube/core/utils/p;)V

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/youtube/core/player/as;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/player/af;Lcom/google/android/youtube/core/player/ax;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->z:Lcom/google/android/youtube/core/player/as;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->z:Lcom/google/android/youtube/core/player/as;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->am:Lcom/google/android/youtube/app/k;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/k;->k()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/player/as;->a(Z)V

    new-instance v2, Lcom/google/android/youtube/core/utils/HdmiReceiver;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-direct {v2, v0, v1}, Lcom/google/android/youtube/core/utils/HdmiReceiver;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/utils/o;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->B:Lcom/google/android/youtube/core/utils/HdmiReceiver;

    new-instance v2, Lcom/google/android/youtube/core/utils/DockReceiver;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-direct {v2, v0, v1}, Lcom/google/android/youtube/core/utils/DockReceiver;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/utils/k;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->C:Lcom/google/android/youtube/core/utils/DockReceiver;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->l()Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->y:Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ae:Lcom/google/android/youtube/core/client/bc;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->v()Lcom/google/android/youtube/core/client/bo;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->af:Lcom/google/android/youtube/core/client/bo;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->e()Lcom/google/android/youtube/core/client/b;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->u()Lcom/google/android/youtube/core/client/d;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->f()Lcom/google/android/youtube/core/client/bj;

    move-result-object v25

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->g()Lcom/google/android/youtube/core/client/bl;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->w()Lcom/google/android/youtube/core/client/bg;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ag:Lcom/google/android/youtube/core/client/bg;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->x()Lcom/google/android/youtube/gmsplus1/f;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ah:Lcom/google/android/youtube/gmsplus1/f;

    const-class v2, Lcom/google/android/youtube/gmsplus1/i;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->d()Lvedroid/support/v4/app/l;

    move-result-object v2

    invoke-virtual {v2, v3}, Lvedroid/support/v4/app/l;->a(Ljava/lang/String;)Lvedroid/support/v4/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/gmsplus1/i;

    if-nez v2, :cond_0

    new-instance v2, Lcom/google/android/youtube/gmsplus1/i;

    invoke-direct {v2}, Lcom/google/android/youtube/gmsplus1/i;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->d()Lvedroid/support/v4/app/l;

    move-result-object v4

    invoke-virtual {v4}, Lvedroid/support/v4/app/l;->a()Lvedroid/support/v4/app/v;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Lvedroid/support/v4/app/v;->a(Lvedroid/support/v4/app/Fragment;Ljava/lang/String;)Lvedroid/support/v4/app/v;

    move-result-object v3

    invoke-virtual {v3}, Lvedroid/support/v4/app/v;->a()I

    :cond_0
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ai:Lcom/google/android/youtube/gmsplus1/i;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->x:Lcom/google/android/youtube/app/d;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->z()Lcom/google/android/youtube/app/remote/bf;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ak:Lcom/google/android/youtube/app/remote/bf;

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->E:Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->A:Lcom/google/android/youtube/core/player/PlayerView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->E:Lcom/google/android/youtube/app/honeycomb/player/LiveBadgeOverlay;

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/player/PlayerView;->addView(Landroid/view/View;)V

    new-instance v11, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->F:Landroid/content/SharedPreferences;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->G:Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ae:Lcom/google/android/youtube/core/client/bc;

    move-object/from16 v0, p0

    invoke-direct {v11, v0, v2, v3, v4}, Lcom/google/android/youtube/core/player/DefaultAdultContentHelper;-><init>(Landroid/app/Activity;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/client/bc;)V

    new-instance v2, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->Q:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->Q:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0007

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->setShowFullscreenInPortrait(Z)V

    new-instance v2, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->P:Lcom/google/android/youtube/core/Analytics;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->Q:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    invoke-virtual {v4}, Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;->j()I

    move-result v4

    const/4 v5, 0x1

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/google/android/youtube/core/player/overlay/DefaultAdOverlay;-><init>(Landroid/content/Context;Lcom/google/android/youtube/core/Analytics;IZ)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->R:Lcom/google/android/youtube/core/player/overlay/a;

    new-instance v16, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultBrandingOverlay;-><init>(Landroid/content/Context;)V

    new-instance v18, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultLiveOverlay;-><init>(Landroid/content/Context;)V

    new-instance v19, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/google/android/youtube/core/player/overlay/DefaultSubtitlesOverlay;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->A:Lcom/google/android/youtube/core/player/PlayerView;

    const/4 v3, 0x5

    new-array v3, v3, [Lcom/google/android/youtube/core/player/overlay/s;

    const/4 v4, 0x0

    aput-object v16, v3, v4

    const/4 v4, 0x1

    aput-object v19, v3, v4

    const/4 v4, 0x2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->Q:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    aput-object v5, v3, v4

    const/4 v4, 0x3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->R:Lcom/google/android/youtube/core/player/overlay/a;

    aput-object v5, v3, v4

    const/4 v4, 0x4

    aput-object v18, v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/youtube/core/player/PlayerView;->a([Lcom/google/android/youtube/core/player/overlay/s;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->A:Lcom/google/android/youtube/core/player/PlayerView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->z:Lcom/google/android/youtube/core/player/as;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->F:Landroid/content/SharedPreferences;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ae:Lcom/google/android/youtube/core/client/bc;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->af:Lcom/google/android/youtube/core/client/bo;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->P:Lcom/google/android/youtube/core/Analytics;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v4}, Lcom/google/android/youtube/app/YouTubeApplication;->m()Ljava/lang/String;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->Q:Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->R:Lcom/google/android/youtube/core/player/overlay/a;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->v:Lcom/google/android/youtube/core/e;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->w:Lcom/google/android/youtube/core/utils/p;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v4}, Lcom/google/android/youtube/app/YouTubeApplication;->h()Lcom/google/android/youtube/core/player/ak;

    move-result-object v22

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v4}, Lcom/google/android/youtube/app/YouTubeApplication;->W()Lcom/google/android/youtube/core/player/c;

    move-result-object v23

    move-object/from16 v4, p0

    move-object/from16 v14, p0

    invoke-static/range {v2 .. v25}, Lcom/google/android/youtube/core/player/Director;->a(Lcom/google/android/youtube/core/player/ai;Lcom/google/android/youtube/core/player/as;Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/d;Lcom/google/android/youtube/core/client/bo;Lcom/google/android/youtube/core/client/bl;Lcom/google/android/youtube/core/player/a;Lcom/google/android/youtube/core/Analytics;Ljava/lang/String;Lcom/google/android/youtube/core/player/v;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;Lcom/google/android/youtube/core/player/overlay/c;Lcom/google/android/youtube/core/player/overlay/a;Lcom/google/android/youtube/core/player/overlay/i;Lcom/google/android/youtube/core/player/overlay/aa;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/core/player/ak;Lcom/google/android/youtube/core/player/c;Lcom/google/android/youtube/core/client/b;Lcom/google/android/youtube/core/client/bj;)Lcom/google/android/youtube/core/player/Director;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->z:Lcom/google/android/youtube/core/player/as;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->P:Lcom/google/android/youtube/core/Analytics;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->p()Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    move-result-object v16

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->g(Z)Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->A:Lcom/google/android/youtube/core/player/PlayerView;

    move-object/from16 v18, v0

    move-object/from16 v12, p0

    move-object v13, v7

    move-object v14, v10

    move-object/from16 v19, p0

    move-object/from16 v20, p0

    invoke-static/range {v11 .. v20}, Lcom/google/android/youtube/app/ui/bh;->a(Lcom/google/android/youtube/core/player/as;Landroid/app/Activity;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bl;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;Lcom/google/android/youtube/core/player/overlay/ControllerOverlay;Landroid/view/View;Lcom/google/android/youtube/app/ui/bp;Lcom/google/android/youtube/app/ui/bq;)Lcom/google/android/youtube/app/ui/bh;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->n:Lcom/google/android/youtube/app/ui/bh;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->A:Lcom/google/android/youtube/core/player/PlayerView;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-static {v0, v2, v1}, Lcom/google/android/youtube/app/compat/k;->a(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Lcom/google/android/youtube/core/player/overlay/PlayerOverlaysLayout;Lcom/google/android/youtube/core/v11/ui/c;)Lcom/google/android/youtube/core/v11/ui/b;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->N:Lcom/google/android/youtube/core/v11/ui/b;

    const v2, 0x7f0700f4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->p:Landroid/view/ViewGroup;

    const v2, 0x7f0700f5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->q:Landroid/widget/ProgressBar;

    const v2, 0x7f0700f6

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->r:Landroid/widget/ImageView;

    const v2, 0x7f0700f7

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->s:Landroid/widget/TextView;

    const v2, 0x7f0700f8

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->t:Landroid/widget/Button;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->t:Landroid/widget/Button;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->A()Lcom/google/android/youtube/app/remote/br;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ao:Lcom/google/android/youtube/app/remote/br;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->C()Lcom/google/android/youtube/app/remote/e;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->D()Lcom/google/android/youtube/app/remote/AtHomeConnection;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->A()Lcom/google/android/youtube/app/remote/br;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ao:Lcom/google/android/youtube/app/remote/br;

    new-instance v2, Lcom/google/android/youtube/app/ui/dh;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ae:Lcom/google/android/youtube/core/client/bc;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->G:Lcom/google/android/youtube/core/async/UserAuthorizer;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/YouTubeApplication;->s()Lcom/google/android/youtube/core/async/by;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/app/k;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->P:Lcom/google/android/youtube/core/Analytics;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->v:Lcom/google/android/youtube/core/e;

    move-object/from16 v3, p0

    move-object/from16 v9, p0

    invoke-direct/range {v2 .. v10}, Lcom/google/android/youtube/app/ui/dh;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/async/UserAuthorizer;Lcom/google/android/youtube/core/async/by;Lcom/google/android/youtube/app/k;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/app/ui/dq;Lcom/google/android/youtube/core/e;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->u:Lcom/google/android/youtube/app/ui/dh;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H()Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;

    move-result-object v3

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/phone/dg;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/google/android/youtube/app/honeycomb/phone/dg;-><init>(Landroid/content/Context;Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->an:Lcom/google/android/youtube/app/honeycomb/phone/dg;

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/h;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v4}, Lcom/google/android/youtube/app/honeycomb/h;-><init>(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;B)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->L:Lcom/google/android/youtube/app/honeycomb/h;

    new-instance v2, Lcom/google/android/youtube/app/honeycomb/j;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v4}, Lcom/google/android/youtube/app/honeycomb/j;-><init>(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;B)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->M:Lcom/google/android/youtube/app/honeycomb/j;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    sget v4, Lcom/google/android/youtube/core/utils/Util;->a:I

    const/16 v5, 0xe

    if-lt v4, v5, :cond_1

    move-object/from16 v0, p0

    invoke-static {v0, v2}, Lcom/google/android/youtube/app/honeycomb/n;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/player/Director;)V

    :cond_1
    new-instance v2, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;

    move-object/from16 v0, p0

    move-object/from16 v1, p0

    invoke-direct {v2, v0, v1}, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/utils/i;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->O:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v4, "audio"

    invoke-virtual {v2, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->o:Landroid/media/AudioManager;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->y:Lcom/google/android/youtube/app/compat/SupportActionBar;

    move-object/from16 v0, p0

    invoke-virtual {v2, v0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a(Lcom/google/android/youtube/app/compat/h;)V

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/honeycomb/ui/i;)V

    sget-object v2, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;->DISABLED:Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;

    invoke-virtual {v3, v2}, Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper;->a(Lcom/google/android/youtube/app/honeycomb/ui/ActionBarMenuHelper$SearchMode;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->K()Lcom/google/android/youtube/app/remote/ad;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->aj:Lcom/google/android/youtube/app/remote/ad;

    if-nez p1, :cond_2

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->J:Lcom/google/android/youtube/core/player/Director$DirectorState;

    if-nez p1, :cond_3

    const/4 v2, -0x1

    :goto_1
    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->K:I

    const-string v2, "keyguard"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/KeyguardManager;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ap:Landroid/app/KeyguardManager;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->o()V

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    const-string v2, ""

    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->Z:Landroid/widget/Toast;

    invoke-direct/range {p0 .. p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->U()V

    return-void

    :cond_2
    const-string v2, "director_state"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/player/Director$DirectorState;

    goto :goto_0

    :cond_3
    const-string v2, "playlist_start_position"

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->isFinishing()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/player/Director;->f(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->O:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->disable()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->N:Lcom/google/android/youtube/core/v11/ui/b;

    invoke-interface {v0}, Lcom/google/android/youtube/core/v11/ui/b;->a()V

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onDestroy()V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/player/Director;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/youtube/core/player/Director;->b(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onNewIntent(Landroid/content/Intent;)V

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/core/utils/ak;->a(Landroid/net/Uri;)Lcom/google/android/youtube/core/utils/ak;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->n:Lcom/google/android/youtube/app/ui/bh;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/bh;->q()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->n:Lcom/google/android/youtube/app/ui/bh;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/ui/bh;->q()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/youtube/core/utils/ak;->a:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "invalid intercepted URI"

    invoke-static {v1, v0}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->finish()V

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lcom/google/android/youtube/core/utils/ak;->a:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v1, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->REMOTE_QUEUE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-static {p0, v0, v1}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->j()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->o()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->n:Lcom/google/android/youtube/app/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bh;->p()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->aq:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->O:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->disable()V

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->aj:Lcom/google/android/youtube/app/remote/ad;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/ad;->b()Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->n:Lcom/google/android/youtube/app/ui/bh;

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/ui/bh;->a(Lcom/google/android/youtube/app/remote/RemoteControl;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->an:Lcom/google/android/youtube/app/honeycomb/phone/dg;

    invoke-virtual {v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/dg;->b(Z)V

    iput-boolean v3, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->aa:Z

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->n:Lcom/google/android/youtube/app/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bh;->o()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->O:Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/DeviceOrientationHelper;->enable()V

    iput-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->aq:Z

    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->n:Lcom/google/android/youtube/app/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bh;->m()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->an:Lcom/google/android/youtube/app/honeycomb/phone/dg;

    invoke-virtual {v0, v3}, Lcom/google/android/youtube/app/honeycomb/phone/dg;->b(Z)V

    iput-boolean v2, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->aa:Z

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->P()V

    const-string v0, "director_state"

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->J:Lcom/google/android/youtube/core/player/Director$DirectorState;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v0, "playlist_start_position"

    iget v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->K:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method

.method public onStart()V
    .locals 4

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStart()V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->W:Z

    if-eqz v1, :cond_0

    iput-boolean v3, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->T:Z

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/core/player/Director;->h(Z)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v1, v3}, Lcom/google/android/youtube/core/player/Director;->g(Z)V

    const/4 v1, 0x6

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->setRequestedOrientation(I)V

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->T:Z

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->h(Z)V

    const-string v1, "finish_on_ended"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->Y:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->B:Lcom/google/android/youtube/core/utils/HdmiReceiver;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/HdmiReceiver;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->C:Lcom/google/android/youtube/core/utils/DockReceiver;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/DockReceiver;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->al:Lcom/google/android/youtube/app/YouTubeApplication;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/YouTubeApplication;->a(I)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->o:Landroid/media/AudioManager;

    const/4 v1, 0x0

    const/high16 v2, -0x80000000

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->L:Lcom/google/android/youtube/app/honeycomb/h;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/h;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->M:Lcom/google/android/youtube/app/honeycomb/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/j;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->aj:Lcom/google/android/youtube/app/remote/ad;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/remote/ad;->a(Lcom/google/android/youtube/app/remote/ah;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->G:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/UserAuthorizer;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ai:Lcom/google/android/youtube/gmsplus1/i;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ah:Lcom/google/android/youtube/gmsplus1/f;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->G:Lcom/google/android/youtube/core/async/UserAuthorizer;

    invoke-virtual {v2}, Lcom/google/android/youtube/core/async/UserAuthorizer;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/gmsplus1/i;->a(Lcom/google/android/youtube/gmsplus1/f;Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->N()V

    return-void
.end method

.method protected onStop()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->o:Landroid/media/AudioManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->L:Lcom/google/android/youtube/app/honeycomb/h;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/h;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->M:Lcom/google/android/youtube/app/honeycomb/j;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/j;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->B:Lcom/google/android/youtube/core/utils/HdmiReceiver;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/HdmiReceiver;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->C:Lcom/google/android/youtube/core/utils/DockReceiver;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/utils/DockReceiver;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->N:Lcom/google/android/youtube/core/v11/ui/b;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/youtube/core/v11/ui/b;->b(Z)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->aj:Lcom/google/android/youtube/app/remote/ad;

    invoke-virtual {v0, p0}, Lcom/google/android/youtube/app/remote/ad;->b(Lcom/google/android/youtube/app/remote/ah;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->an:Lcom/google/android/youtube/app/honeycomb/phone/dg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/dg;->b()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->aw:Lcom/google/android/youtube/core/async/o;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->aw:Lcom/google/android/youtube/core/async/o;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/async/o;->a()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->J:Lcom/google/android/youtube/core/player/Director$DirectorState;

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->P()V

    :cond_1
    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStop()V

    return-void
.end method

.method protected p()Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->g(Z)Lcom/google/android/youtube/core/player/overlay/DefaultControllerOverlay;

    move-result-object v0

    return-object v0
.end method

.method protected abstract q()Lcom/google/android/youtube/gmsplus1/StyleablePlusOneButtonWithPopup;
.end method

.method protected r()Landroid/view/ViewGroup$MarginLayoutParams;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->A:Lcom/google/android/youtube/core/player/PlayerView;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/PlayerView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    return-object v0
.end method

.method public final s()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ad:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->an:Lcom/google/android/youtube/app/honeycomb/phone/dg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/dg;->s()V

    return-void
.end method

.method public final t()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->ad:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->an:Lcom/google/android/youtube/app/honeycomb/phone/dg;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/dg;->t()V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->Q()V

    return-void
.end method

.method public final u()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->T()V

    return-void
.end method

.method public final v()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->T()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->y:Lcom/google/android/youtube/app/compat/SupportActionBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->y:Lcom/google/android/youtube/app/compat/SupportActionBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->y:Lcom/google/android/youtube/app/compat/SupportActionBar;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/compat/SupportActionBar;->b()V

    :cond_0
    return-void
.end method

.method protected final w()Ljava/lang/String;
    .locals 1

    const-string v0, "yt_watch"

    return-object v0
.end method

.method public final x()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->aj:Lcom/google/android/youtube/app/remote/ad;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/ad;->e()V

    return-void
.end method

.method public final y()Lcom/google/android/youtube/core/model/Video;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->p()Lcom/google/android/youtube/core/model/Video;

    move-result-object v0

    return-object v0
.end method

.method public final z()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->n:Lcom/google/android/youtube/app/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bh;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->n:Lcom/google/android/youtube/app/ui/bh;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/bh;->t()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->H:Lcom/google/android/youtube/core/player/Director;

    invoke-virtual {v0}, Lcom/google/android/youtube/core/player/Director;->r()I

    move-result v0

    goto :goto_0
.end method
