.class public final Lcom/google/android/youtube/app/honeycomb/phone/l;
.super Lcom/google/android/youtube/app/honeycomb/phone/x;
.source "SourceFile"


# static fields
.field public static final b:Ljava/util/Map;

.field public static final c:Ljava/util/Map;


# instance fields
.field private final d:Landroid/content/res/Resources;

.field private final e:Landroid/view/LayoutInflater;

.field private final f:Lcom/google/android/youtube/app/YouTubeApplication;

.field private final g:Lcom/google/android/youtube/core/e;

.field private final h:Ljava/lang/String;

.field private final i:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

.field private j:Lcom/google/android/youtube/app/ui/bx;

.field private k:Landroid/view/View;

.field private l:Lcom/google/android/youtube/core/ui/PagedListView;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/LinkedHashMap;

    const/16 v1, 0xf

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->b:Ljava/util/Map;

    const-string v1, "Autos"

    const v2, 0x7f0b00ff

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200e7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->b:Ljava/util/Map;

    const-string v1, "Comedy"

    const v2, 0x7f0b0100

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200ea

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->b:Ljava/util/Map;

    const-string v1, "Education"

    const v2, 0x7f0b0101

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200ed

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->b:Ljava/util/Map;

    const-string v1, "Entertainment"

    const v2, 0x7f0b0102

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200f0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->b:Ljava/util/Map;

    const-string v1, "Film"

    const v2, 0x7f0b0103

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200f3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->b:Ljava/util/Map;

    const-string v1, "Games"

    const v2, 0x7f0b0104

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200f6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->b:Ljava/util/Map;

    const-string v1, "Music"

    const v2, 0x7f0b0105

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200fc

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->b:Ljava/util/Map;

    const-string v1, "News"

    const v2, 0x7f0b0106

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f0200ff

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->b:Ljava/util/Map;

    const-string v1, "Nonprofit"

    const v2, 0x7f0b0107

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f020102

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->b:Ljava/util/Map;

    const-string v1, "People"

    const v2, 0x7f0b0108

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f020105

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->b:Ljava/util/Map;

    const-string v1, "Animals"

    const v2, 0x7f0b0109

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f020108

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->b:Ljava/util/Map;

    const-string v1, "Tech"

    const v2, 0x7f0b010a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f020111

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->b:Ljava/util/Map;

    const-string v1, "Sports"

    const v2, 0x7f0b010b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f020114

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->b:Ljava/util/Map;

    const-string v1, "Howto"

    const v2, 0x7f0b010c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f020117

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->b:Ljava/util/Map;

    const-string v1, "Travel"

    const v2, 0x7f0b010d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const v3, 0x7f02011a

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/LinkedHashMap;

    const/16 v1, 0x14

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    sput-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->c:Ljava/util/Map;

    const-string v1, "Autos"

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_AUTOS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->c:Ljava/util/Map;

    const-string v1, "Comedy"

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_COMEDY:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->c:Ljava/util/Map;

    const-string v1, "Education"

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_EDUCATION:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->c:Ljava/util/Map;

    const-string v1, "Entertainment"

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_ENTERTAINMENT:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->c:Ljava/util/Map;

    const-string v1, "Film"

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_FILM:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->c:Ljava/util/Map;

    const-string v1, "Games"

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_GAMES:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->c:Ljava/util/Map;

    const-string v1, "Music"

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_MUSIC:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->c:Ljava/util/Map;

    const-string v1, "News"

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_NEWS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->c:Ljava/util/Map;

    const-string v1, "Nonprofit"

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_NONPROFIT:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->c:Ljava/util/Map;

    const-string v1, "People"

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_PEOPLE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->c:Ljava/util/Map;

    const-string v1, "Animals"

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_ANIMALS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->c:Ljava/util/Map;

    const-string v1, "Tech"

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_SCIENCE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->c:Ljava/util/Map;

    const-string v1, "Sports"

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_SPORTS:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->c:Ljava/util/Map;

    const-string v1, "Howto"

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_HOWTO:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->c:Ljava/util/Map;

    const-string v1, "Travel"

    sget-object v2, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->GUIDE_CATEGORY_TRAVEL:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;Ljava/lang/String;Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;)V
    .locals 3

    invoke-direct {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/x;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;)V

    const-string v0, "categoryTerm cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->h:Ljava/lang/String;

    const-string v0, "feature cannot be null"

    invoke-static {p3, v0}, Lcom/google/android/youtube/core/utils/s;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->i:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->e:Landroid/view/LayoutInflater;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/YouTubeApplication;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->f:Lcom/google/android/youtube/app/YouTubeApplication;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->f:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->d:Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->f:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->g:Lcom/google/android/youtube/core/e;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->e:Landroid/view/LayoutInflater;

    const v2, 0x7f04001d

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/l;->o()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->k:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->k:Landroid/view/View;

    const v1, 0x7f070067

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/ui/PagedListView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->l:Lcom/google/android/youtube/core/ui/PagedListView;

    return-void
.end method

.method private b()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->j:Lcom/google/android/youtube/app/ui/bx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->j:Lcom/google/android/youtube/app/ui/bx;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->d:Landroid/content/res/Resources;

    const v2, 0x7f0a000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/bx;->a(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 12

    const/4 v6, 0x0

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/x;->a()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->f:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->f:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->f:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v3}, Lcom/google/android/youtube/app/YouTubeApplication;->w()Lcom/google/android/youtube/core/client/bg;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->f:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v4}, Lcom/google/android/youtube/app/YouTubeApplication;->l()Lcom/google/android/youtube/core/utils/p;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/youtube/app/adapter/bn;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;Lcom/google/android/youtube/core/utils/p;Lcom/google/android/youtube/app/prefetch/d;)Lcom/google/android/youtube/app/adapter/bm;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v1, v0}, Lcom/google/android/youtube/app/ui/bx;->a(Landroid/content/Context;Lcom/google/android/youtube/core/a/a;)Lcom/google/android/youtube/app/ui/bx;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->j:Lcom/google/android/youtube/app/ui/bx;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/l;->b()V

    new-instance v0, Lcom/google/android/youtube/app/ui/ea;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->l:Lcom/google/android/youtube/core/ui/PagedListView;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->j:Lcom/google/android/youtube/app/ui/bx;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->f:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v4}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/youtube/core/client/bc;->k()Lcom/google/android/youtube/core/async/au;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->g:Lcom/google/android/youtube/core/e;

    iget-object v7, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v7}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v7

    iget-object v9, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->i:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    iget-object v8, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-virtual {v8}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->I()Lcom/google/android/youtube/core/Analytics;

    move-result-object v10

    sget-object v11, Lcom/google/android/youtube/core/Analytics$VideoCategory;->HomeFeed:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    move v8, v6

    invoke-direct/range {v0 .. v11}, Lcom/google/android/youtube/app/ui/ea;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;ZLcom/google/android/youtube/app/d;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->f:Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v2

    sget-object v3, Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;->MOST_POPULAR:Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->h:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v5}, Lcom/google/android/youtube/core/utils/Util;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    sget-object v7, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;->THIS_WEEK:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-virtual {v2, v3, v4, v5, v7}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/ea;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    return-void
.end method

.method public final a(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/x;->a(Landroid/content/res/Configuration;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/l;->b()V

    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 3

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    sget-object v0, Lcom/google/android/youtube/app/honeycomb/phone/l;->b:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/l;->h:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
