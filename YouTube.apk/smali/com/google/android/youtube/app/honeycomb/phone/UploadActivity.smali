.class public Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/honeycomb/ui/aa;
.implements Lcom/google/android/youtube/core/async/bk;


# instance fields
.field private n:Lcom/google/android/youtube/core/async/by;

.field private o:Lcom/google/android/youtube/app/k;

.field private p:Lcom/google/android/youtube/app/honeycomb/ui/t;

.field private q:Lcom/google/android/youtube/core/e;

.field private r:Z

.field private s:Z

.field private t:Lcom/google/android/youtube/core/model/UserAuth;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method private h()V
    .locals 3

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->r:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->t:Lcom/google/android/youtube/core/model/UserAuth;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->s:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->p:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->t:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/honeycomb/ui/t;->a(Landroid/content/Intent;Lcom/google/android/youtube/core/model/UserAuth;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->s:Z

    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(I)Landroid/app/Dialog;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->p:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-virtual {v0, p1}, Lcom/google/android/youtube/app/honeycomb/ui/t;->a(I)Landroid/app/Dialog;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(I)Landroid/app/Dialog;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public final a(Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->t:Lcom/google/android/youtube/core/model/UserAuth;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->h()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->p:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/ui/t;->e()V

    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 1

    const-string v0, "Error authenticating"

    invoke-static {v0, p2}, Lcom/google/android/youtube/core/L;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->q:Lcom/google/android/youtube/core/e;

    invoke-virtual {v0, p2}, Lcom/google/android/youtube/core/e;->b(Ljava/lang/Throwable;)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->finish()V

    return-void
.end method

.method public final a(Lcom/google/android/youtube/app/compat/m;)Z
    .locals 2

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/compat/m;)Z

    move-result v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->p:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-virtual {v1, p1}, Lcom/google/android/youtube/app/honeycomb/ui/t;->a(Lcom/google/android/youtube/app/compat/m;)Z

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method public final f()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->finish()V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/d;->d()V

    return-void
.end method

.method public final g()V
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->finish()V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/app/d;->d()V

    return-void
.end method

.method public final g_()V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400a1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->setContentView(Landroid/view/View;)V

    const v0, 0x7f0b015e

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->b(I)V

    sget-boolean v0, Lcom/google/android/youtube/app/honeycomb/ui/t;->a:Z

    if-eqz v0, :cond_0

    const v0, 0x7f070069

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/ui/TabRow;

    const v1, 0x7f07006a

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/ui/Workspace;

    invoke-static {p0, v1, v0}, Lcom/google/android/youtube/core/ui/Workspace;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/Workspace;Lcom/google/android/youtube/core/ui/TabRow;)Lcom/google/android/youtube/core/ui/Workspace;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/core/ui/Workspace;->setCurrentScreen(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->getApplication()Landroid/app/Application;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/google/android/youtube/app/YouTubeApplication;

    invoke-virtual {v7}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v3

    invoke-virtual {v7}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->q:Lcom/google/android/youtube/core/e;

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/ui/t;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->q:Lcom/google/android/youtube/core/e;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->E()Lcom/google/android/youtube/app/compat/r;

    move-result-object v6

    move-object v1, p0

    move-object v4, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/youtube/app/honeycomb/ui/t;-><init>(Landroid/app/Activity;Landroid/view/View;Lcom/google/android/youtube/core/client/bc;Lcom/google/android/youtube/app/honeycomb/ui/aa;Lcom/google/android/youtube/core/e;Lcom/google/android/youtube/app/compat/r;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->p:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-virtual {v7}, Lcom/google/android/youtube/app/YouTubeApplication;->s()Lcom/google/android/youtube/core/async/by;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->n:Lcom/google/android/youtube/core/async/by;

    invoke-virtual {v7}, Lcom/google/android/youtube/app/YouTubeApplication;->n()Lcom/google/android/youtube/app/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->o:Lcom/google/android/youtube/app/k;

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onResume()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->t:Lcom/google/android/youtube/core/model/UserAuth;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->p:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/ui/t;->e()V

    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 3

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStart()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->r:Z

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->t:Lcom/google/android/youtube/core/model/UserAuth;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->n:Lcom/google/android/youtube/core/async/by;

    const v1, 0x7f0b01c0

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->o:Lcom/google/android/youtube/app/k;

    invoke-virtual {v2}, Lcom/google/android/youtube/app/k;->g()Z

    move-result v2

    invoke-virtual {v0, p0, p0, v1, v2}, Lcom/google/android/youtube/core/async/by;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/bk;IZ)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->h()V

    goto :goto_0
.end method

.method protected onStop()V
    .locals 2

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStop()V

    iput-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->r:Z

    iget-boolean v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->s:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->p:Lcom/google/android/youtube/app/honeycomb/ui/t;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/ui/t;->a()V

    iput-boolean v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/UploadActivity;->s:Z

    :cond_0
    return-void
.end method

.method protected final w()Ljava/lang/String;
    .locals 1

    const-string v0, "yt_upload"

    return-object v0
.end method
