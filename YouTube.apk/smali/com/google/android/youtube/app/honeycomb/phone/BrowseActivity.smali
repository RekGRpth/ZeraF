.class public Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;
.super Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/ui/aq;


# instance fields
.field private n:Lcom/google/android/youtube/core/async/GDataRequestFactory;

.field private o:Lcom/google/android/youtube/core/async/au;

.field private p:Lcom/google/android/youtube/core/client/be;

.field private q:Lcom/google/android/youtube/core/client/bg;

.field private r:Lcom/google/android/youtube/core/e;

.field private s:Lcom/google/android/youtube/app/ui/ao;

.field private t:Lcom/google/android/youtube/app/ui/ao;

.field private u:Ljava/lang/String;

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/String;

.field private x:Lcom/google/android/youtube/app/ui/ea;

.field private y:Lcom/google/android/youtube/core/async/GDataRequestFactory$StatisticFilter;

.field private z:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;)Lcom/google/android/youtube/core/async/GDataRequestFactory$StatisticFilter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->y:Lcom/google/android/youtube/core/async/GDataRequestFactory$StatisticFilter;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;Lcom/google/android/youtube/core/async/GDataRequestFactory$StatisticFilter;)Lcom/google/android/youtube/core/async/GDataRequestFactory$StatisticFilter;
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->y:Lcom/google/android/youtube/core/async/GDataRequestFactory$StatisticFilter;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;Lcom/google/android/youtube/core/async/GDataRequestFactory$StatisticFilter;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$StatisticFilter;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)V

    return-void
.end method

.method private a(Lcom/google/android/youtube/core/async/GDataRequestFactory$StatisticFilter;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)V
    .locals 7

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->x:Lcom/google/android/youtube/app/ui/ea;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/youtube/core/async/GDataRequest;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->n:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    iget-object v4, p1, Lcom/google/android/youtube/core/async/GDataRequestFactory$StatisticFilter;->feed:Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->v:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->u:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v6, p2}, Lcom/google/android/youtube/core/async/GDataRequestFactory;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$StandardFeed;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)Lcom/google/android/youtube/core/async/GDataRequest;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/youtube/app/ui/ea;->a([Lcom/google/android/youtube/core/async/GDataRequest;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;)Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->z:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    return-object v0
.end method


# virtual methods
.method protected final a(I)Landroid/app/Dialog;
    .locals 1

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->s:Lcom/google/android/youtube/app/ui/ao;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ao;->a()Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3fa
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/youtube/app/YouTubeApplication;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->a(Lcom/google/android/youtube/app/YouTubeApplication;)V

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bc;->a()Lcom/google/android/youtube/core/async/GDataRequestFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->n:Lcom/google/android/youtube/core/async/GDataRequestFactory;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->a()Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/youtube/core/client/bc;->k()Lcom/google/android/youtube/core/async/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->o:Lcom/google/android/youtube/core/async/au;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->e_()Lcom/google/android/youtube/core/client/be;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->p:Lcom/google/android/youtube/core/client/be;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->w()Lcom/google/android/youtube/core/client/bg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->q:Lcom/google/android/youtube/core/client/bg;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->j()Lcom/google/android/youtube/core/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->r:Lcom/google/android/youtube/core/e;

    invoke-virtual {p1}, Lcom/google/android/youtube/app/YouTubeApplication;->F()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->u:Ljava/lang/String;

    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Enum;)V
    .locals 2

    check-cast p1, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->z:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->y:Lcom/google/android/youtube/core/async/GDataRequestFactory$StatisticFilter;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->z:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$StatisticFilter;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 12

    const/4 v6, 0x0

    invoke-super {p0, p1}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f040018

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "category_label"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->w:Ljava/lang/String;

    const-string v1, "category_term"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->v:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->w:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->b(Ljava/lang/String;)V

    const v0, 0x7f07005b

    invoke-virtual {p0, v0}, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    const v1, 0x7f07005c

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    sget-object v2, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;->THIS_WEEK:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    const v3, 0x7f0400be

    invoke-static {p0, p0, v2, v1, v3}, Lcom/google/android/youtube/app/ui/ao;->a(Landroid/app/Activity;Lcom/google/android/youtube/app/ui/aq;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;Landroid/widget/Spinner;I)Lcom/google/android/youtube/app/ui/ao;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->s:Lcom/google/android/youtube/app/ui/ao;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/k;

    invoke-direct {v1, p0}, Lcom/google/android/youtube/app/honeycomb/phone/k;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;)V

    sget-object v2, Lcom/google/android/youtube/core/async/GDataRequestFactory$StatisticFilter;->MOST_VIEWED:Lcom/google/android/youtube/core/async/GDataRequestFactory$StatisticFilter;

    invoke-static {p0, v1, v2, v0}, Lcom/google/android/youtube/app/ui/ao;->a(Landroid/app/Activity;Lcom/google/android/youtube/app/ui/aq;Lcom/google/android/youtube/core/async/GDataRequestFactory$StatisticFilter;Landroid/widget/Spinner;)Lcom/google/android/youtube/app/ui/ao;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->t:Lcom/google/android/youtube/app/ui/ao;

    new-instance v0, Lcom/google/android/youtube/app/ui/ea;

    const v1, 0x7f07005d

    invoke-virtual {p0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/youtube/core/ui/PagedView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->p:Lcom/google/android/youtube/core/client/be;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->q:Lcom/google/android/youtube/core/client/bg;

    invoke-static {p0, v1, v3}, Lcom/google/android/youtube/app/adapter/bn;->a(Landroid/content/Context;Lcom/google/android/youtube/core/client/be;Lcom/google/android/youtube/core/client/bg;)Lcom/google/android/youtube/app/adapter/bm;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->o:Lcom/google/android/youtube/core/async/au;

    iget-object v5, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->r:Lcom/google/android/youtube/core/e;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v7

    sget-object v9, Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;->BROWSE:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->I()Lcom/google/android/youtube/core/Analytics;

    move-result-object v10

    sget-object v11, Lcom/google/android/youtube/core/Analytics$VideoCategory;->Browse:Lcom/google/android/youtube/core/Analytics$VideoCategory;

    move-object v1, p0

    move v8, v6

    invoke-direct/range {v0 .. v11}, Lcom/google/android/youtube/app/ui/ea;-><init>(Landroid/app/Activity;Lcom/google/android/youtube/core/ui/PagedView;Lcom/google/android/youtube/core/a/a;Lcom/google/android/youtube/core/async/au;Lcom/google/android/youtube/core/e;ZLcom/google/android/youtube/app/d;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;Lcom/google/android/youtube/core/Analytics;Lcom/google/android/youtube/core/Analytics$VideoCategory;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->x:Lcom/google/android/youtube/app/ui/ea;

    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;->onStart()V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->s:Lcom/google/android/youtube/app/ui/ao;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ao;->b()Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->z:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->t:Lcom/google/android/youtube/app/ui/ao;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/ui/ao;->b()Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/async/GDataRequestFactory$StatisticFilter;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->y:Lcom/google/android/youtube/core/async/GDataRequestFactory$StatisticFilter;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->y:Lcom/google/android/youtube/core/async/GDataRequestFactory$StatisticFilter;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->z:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-direct {p0, v0, v1}, Lcom/google/android/youtube/app/honeycomb/phone/BrowseActivity;->a(Lcom/google/android/youtube/core/async/GDataRequestFactory$StatisticFilter;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)V

    return-void
.end method

.method protected final w()Ljava/lang/String;
    .locals 1

    const-string v0, "yt_browse"

    return-object v0
.end method
