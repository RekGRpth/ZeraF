.class final Lcom/google/android/youtube/app/honeycomb/phone/ai;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/by;
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

.field private final b:Landroid/view/View;

.field private final c:Landroid/widget/TextView;

.field private final d:Lcom/google/android/youtube/app/adapter/k;

.field private e:Lcom/google/android/youtube/core/model/UserProfile;

.field private f:I


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;Landroid/content/Context;)V
    .locals 6

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ai;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->z(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040045

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ai;->b:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ai;->b:Landroid/view/View;

    const v1, 0x7f07002c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ai;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ai;->b:Landroid/view/View;

    const v1, 0x7f070056

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f020049

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/aj;

    iget-object v3, p0, Lcom/google/android/youtube/app/honeycomb/phone/ai;->b:Landroid/view/View;

    const v4, 0x7f070035

    move-object v1, p0

    move-object v2, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/youtube/app/honeycomb/phone/aj;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/ai;Landroid/content/Context;Landroid/view/View;ILcom/google/android/youtube/app/honeycomb/phone/ab;)V

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ai;->d:Lcom/google/android/youtube/app/adapter/k;

    invoke-virtual {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ai;->b()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/youtube/app/honeycomb/phone/ai;)Lcom/google/android/youtube/core/model/UserProfile;
    .locals 1

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ai;->e:Lcom/google/android/youtube/core/model/UserProfile;

    return-object v0
.end method

.method private c()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ai;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ai;->b:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ai;->c:Landroid/widget/TextView;

    const-string v3, "ACCOUNT"

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a(Lcom/google/android/youtube/app/honeycomb/phone/ab;Landroid/view/View;Landroid/widget/TextView;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ai;->e:Lcom/google/android/youtube/core/model/UserProfile;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ai;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ai;->e:Lcom/google/android/youtube/core/model/UserProfile;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/UserProfile;->displayUsername:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ai;->d:Lcom/google/android/youtube/app/adapter/k;

    iget v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ai;->f:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/youtube/app/adapter/k;->a(ILjava/lang/Object;)Landroid/view/View;

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)Landroid/view/View;
    .locals 2

    iput p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ai;->f:I

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ai;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->A(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ai;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ai;->c()V

    :goto_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ai;->b:Landroid/view/View;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ai;->c:Landroid/widget/TextView;

    const v1, 0x7f0b00e1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/google/android/youtube/core/model/UserProfile;

    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ai;->e:Lcom/google/android/youtube/core/model/UserProfile;

    invoke-direct {p0}, Lcom/google/android/youtube/app/honeycomb/phone/ai;->c()V

    return-void
.end method

.method public final a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final b()V
    .locals 3

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ai;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->A(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ai;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->v(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Lcom/google/android/youtube/core/client/bc;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ai;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->A(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Lcom/google/android/youtube/core/model/UserAuth;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/ai;->a:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    iget-object v2, v2, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v2, p0}, Lcom/google/android/youtube/core/async/h;->a(Landroid/app/Activity;Lcom/google/android/youtube/core/async/n;)Lcom/google/android/youtube/core/async/h;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/core/client/bc;->a(Lcom/google/android/youtube/core/model/UserAuth;Lcom/google/android/youtube/core/async/n;)V

    :cond_0
    return-void
.end method
