.class final Lcom/google/android/youtube/app/honeycomb/phone/bx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/phone/bw;

.field private final b:Lcom/google/android/youtube/core/model/Page;


# direct methods
.method private constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/bw;Lcom/google/android/youtube/core/model/Page;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bx;->a:Lcom/google/android/youtube/app/honeycomb/phone/bw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bx;->b:Lcom/google/android/youtube/core/model/Page;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/bw;Lcom/google/android/youtube/core/model/Page;B)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/google/android/youtube/app/honeycomb/phone/bx;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/bw;Lcom/google/android/youtube/core/model/Page;)V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/bx;->b:Lcom/google/android/youtube/core/model/Page;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bx;->b:Lcom/google/android/youtube/core/model/Page;

    invoke-virtual {v1}, Lcom/google/android/youtube/core/model/Page;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/youtube/core/model/Video;

    iget-object v1, v1, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bx;->a:Lcom/google/android/youtube/app/honeycomb/phone/bw;

    iget-object v1, v1, Lcom/google/android/youtube/app/honeycomb/phone/bw;->a:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->a(Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v1

    invoke-interface {v1, v2}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Ljava/util/List;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bx;->a:Lcom/google/android/youtube/app/honeycomb/phone/bw;

    iget-object v1, v1, Lcom/google/android/youtube/app/honeycomb/phone/bw;->a:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->a(Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;)Lcom/google/android/youtube/app/remote/RemoteControl;

    move-result-object v3

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v3, v1}, Lcom/google/android/youtube/app/remote/RemoteControl;->a(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/phone/bx;->a:Lcom/google/android/youtube/app/honeycomb/phone/bw;

    iget-object v1, v1, Lcom/google/android/youtube/app/honeycomb/phone/bw;->a:Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;

    invoke-virtual {v1}, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity;->F()Lcom/google/android/youtube/app/d;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/phone/bx;->a:Lcom/google/android/youtube/app/honeycomb/phone/bw;

    invoke-static {v2}, Lcom/google/android/youtube/app/honeycomb/phone/bw;->a(Lcom/google/android/youtube/app/honeycomb/phone/bw;)Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/youtube/app/honeycomb/phone/PostPairingActivity$FeedItem;->feature:Lcom/google/android/youtube/core/client/VideoStats2Client$Feature;

    invoke-interface {v1, v0, v4, v2}, Lcom/google/android/youtube/app/d;->a(Ljava/lang/String;ZLcom/google/android/youtube/core/client/VideoStats2Client$Feature;)V

    return-void
.end method
