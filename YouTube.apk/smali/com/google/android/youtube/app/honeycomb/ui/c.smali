.class final Lcom/google/android/youtube/app/honeycomb/ui/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SearchView$OnSuggestionListener;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/ui/a;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/honeycomb/ui/a;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/ui/c;->a:Lcom/google/android/youtube/app/honeycomb/ui/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSuggestionClick(I)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/ui/c;->a:Lcom/google/android/youtube/app/honeycomb/ui/a;

    iget-object v1, v1, Lcom/google/android/youtube/app/honeycomb/ui/a;->b:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getSuggestionsAdapter()Landroid/widget/CursorAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_0

    :try_start_0
    const-string v2, "suggest_intent_query"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/youtube/app/honeycomb/ui/c;->a:Lcom/google/android/youtube/app/honeycomb/ui/a;

    invoke-static {v2, v1}, Lcom/google/android/youtube/app/honeycomb/ui/a;->a(Lcom/google/android/youtube/app/honeycomb/ui/a;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final onSuggestionSelect(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
