.class final Lcom/google/android/youtube/app/honeycomb/phone/cd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/cd;->a:Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 5

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/remote/bp;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/ci;

    invoke-direct {v1}, Lcom/google/android/youtube/app/honeycomb/phone/ci;-><init>()V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "screenId"

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bp;->c()Lcom/google/android/ytremote/model/CloudScreen;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/ytremote/model/ScreenId;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/ytremote/model/ScreenId;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "screenName"

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bp;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ci;->g(Landroid/os/Bundle;)V

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/ce;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/ce;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/cd;)V

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/honeycomb/phone/ci;->a(Lcom/google/android/youtube/app/honeycomb/phone/cl;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cd;->a:Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;->d()Lvedroid/support/v4/app/l;

    move-result-object v0

    const-string v2, "confirm_remove_tv"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/ci;->a(Lvedroid/support/v4/app/l;Ljava/lang/String;)V

    return-void
.end method
