.class final Lcom/google/android/youtube/app/honeycomb/phone/cf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;


# direct methods
.method constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/cf;->a:Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cf;->a:Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;->b(Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/app/honeycomb/phone/cu;

    new-instance v1, Lcom/google/android/youtube/app/honeycomb/phone/cn;

    invoke-direct {v1}, Lcom/google/android/youtube/app/honeycomb/phone/cn;-><init>()V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "screenId"

    iget-object v4, v0, Lcom/google/android/youtube/app/honeycomb/phone/cu;->a:Lcom/google/android/youtube/app/remote/bp;

    invoke-virtual {v4}, Lcom/google/android/youtube/app/remote/bp;->c()Lcom/google/android/ytremote/model/CloudScreen;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/ytremote/model/CloudScreen;->getScreenId()Lcom/google/android/ytremote/model/ScreenId;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/ytremote/model/ScreenId;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "screenName"

    iget-object v0, v0, Lcom/google/android/youtube/app/honeycomb/phone/cu;->a:Lcom/google/android/youtube/app/remote/bp;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/remote/bp;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->g(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/cf;->a:Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/phone/ScreenManagementActivity;->d()Lvedroid/support/v4/app/l;

    move-result-object v0

    const-string v2, "rename_tv"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->a(Lvedroid/support/v4/app/l;Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/youtube/app/honeycomb/phone/cg;

    invoke-direct {v0, p0}, Lcom/google/android/youtube/app/honeycomb/phone/cg;-><init>(Lcom/google/android/youtube/app/honeycomb/phone/cf;)V

    invoke-virtual {v1, v0}, Lcom/google/android/youtube/app/honeycomb/phone/cn;->a(Lcom/google/android/youtube/app/honeycomb/phone/cr;)V

    return-void
.end method
