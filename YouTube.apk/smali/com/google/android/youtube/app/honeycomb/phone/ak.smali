.class abstract Lcom/google/android/youtube/app/honeycomb/phone/ak;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/app/adapter/by;


# instance fields
.field protected final a:Landroid/view/View;

.field protected final b:Landroid/widget/ImageView;

.field protected final c:Landroid/widget/TextView;

.field final synthetic d:Lcom/google/android/youtube/app/honeycomb/phone/ab;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/phone/ab;)V
    .locals 3

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/phone/ak;->d:Lcom/google/android/youtube/app/honeycomb/phone/ab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/youtube/app/honeycomb/phone/ab;->z(Lcom/google/android/youtube/app/honeycomb/phone/ab;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040045

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ak;->a:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ak;->a:Landroid/view/View;

    const v1, 0x7f070035

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ak;->b:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ak;->a:Landroid/view/View;

    const v1, 0x7f07002c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ak;->c:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/phone/ak;->c:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/youtube/app/honeycomb/phone/ab;->a:Lcom/google/android/youtube/app/honeycomb/phone/YouTubeActivity;

    invoke-static {v1}, Lcom/google/android/youtube/core/utils/Util;->g(Landroid/content/Context;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
