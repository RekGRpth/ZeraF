.class final Lcom/google/android/youtube/app/honeycomb/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/youtube/core/async/n;


# instance fields
.field final synthetic a:Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;

.field private final b:Lcom/google/android/youtube/core/model/UserAuth;


# direct methods
.method public constructor <init>(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;Lcom/google/android/youtube/core/model/UserAuth;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/youtube/app/honeycomb/i;->a:Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/youtube/app/honeycomb/i;->b:Lcom/google/android/youtube/core/model/UserAuth;

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/i;->a:Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/i;->a:Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->d(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;)Lcom/google/android/youtube/core/e;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/youtube/core/e;->a(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;Ljava/lang/String;)V

    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6

    const/4 v2, 0x0

    check-cast p2, Lcom/google/android/youtube/core/model/Page;

    invoke-static {}, Lcom/google/android/youtube/core/utils/s;->a()V

    iget v0, p2, Lcom/google/android/youtube/core/model/Page;->totalResults:I

    if-nez v0, :cond_0

    const-string v0, "empty search result, redirecting to search for refining"

    invoke-static {v0}, Lcom/google/android/youtube/core/L;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/i;->a:Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;

    invoke-static {v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->i(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;)Lcom/google/android/youtube/app/d;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/i;->a:Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;

    invoke-static {v1}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->h(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;->ALL_TIME:Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;

    invoke-interface {v0, v1, v2}, Lcom/google/android/youtube/app/d;->a(Ljava/lang/String;Lcom/google/android/youtube/core/async/GDataRequestFactory$TimeFilter;)V

    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/i;->a:Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;

    invoke-virtual {v0}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    iget-object v0, p2, Lcom/google/android/youtube/core/model/Page;->entries:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/core/model/Video;

    iget-object v0, v0, Lcom/google/android/youtube/core/model/Video;->id:Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/youtube/app/honeycomb/i;->a:Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;

    iget-object v1, p0, Lcom/google/android/youtube/app/honeycomb/i;->b:Lcom/google/android/youtube/core/model/UserAuth;

    const/4 v5, 0x0

    move-object v4, v2

    invoke-static/range {v0 .. v5}, Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;->a(Lcom/google/android/youtube/app/honeycomb/BaseWatchActivity;Lcom/google/android/youtube/core/model/UserAuth;Landroid/net/Uri;Ljava/util/List;Landroid/net/Uri;I)V

    goto :goto_0
.end method
