.class public Lcom/google/android/ytremote/b/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/ytremote/logic/a;


# static fields
.field protected static final a:Ljava/lang/String;


# instance fields
.field private final b:Lorg/apache/http/client/HttpClient;

.field private final c:Lcom/google/android/ytremote/backend/b/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/ytremote/b/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/ytremote/b/c;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/android/ytremote/a/d/a;->c()Lorg/apache/http/impl/client/DefaultHttpClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ytremote/b/c;->b:Lorg/apache/http/client/HttpClient;

    new-instance v0, Lcom/google/android/ytremote/backend/b/a;

    invoke-direct {v0}, Lcom/google/android/ytremote/backend/b/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/ytremote/b/c;->c:Lcom/google/android/ytremote/backend/b/a;

    return-void
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)Lcom/google/android/ytremote/model/a;
    .locals 7

    const/4 v2, 0x0

    const/4 v6, -0x2

    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    :try_start_0
    iget-object v1, p0, Lcom/google/android/ytremote/b/c;->b:Lorg/apache/http/client/HttpClient;

    invoke-interface {v1, v0}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :try_start_1
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v0

    const/16 v3, 0x194

    if-ne v0, v3, :cond_0

    new-instance v0, Lcom/google/android/ytremote/model/a;

    const/4 v2, -0x1

    invoke-direct {v0, v2}, Lcom/google/android/ytremote/model/a;-><init>(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Lorg/xml/sax/SAXException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-static {v1}, Lcom/google/android/ytremote/a/c/a;->a(Lorg/apache/http/HttpResponse;)V

    :goto_0
    return-object v0

    :cond_0
    const/16 v3, 0xc8

    if-eq v0, v3, :cond_1

    :try_start_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Request for app status from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " got response code"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/youtube/L;->a(Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/ytremote/model/a;

    const/4 v2, -0x2

    invoke-direct {v0, v2}, Lcom/google/android/ytremote/model/a;-><init>(I)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Lorg/xml/sax/SAXException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    invoke-static {v1}, Lcom/google/android/ytremote/a/c/a;->a(Lorg/apache/http/HttpResponse;)V

    goto :goto_0

    :cond_1
    :try_start_3
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    sget-object v3, Landroid/util/Xml$Encoding;->UTF_8:Landroid/util/Xml$Encoding;

    iget-object v4, p0, Lcom/google/android/ytremote/b/c;->c:Lcom/google/android/ytremote/backend/b/a;

    invoke-static {v0, v3, v4}, Landroid/util/Xml;->parse(Ljava/io/InputStream;Landroid/util/Xml$Encoding;Lorg/xml/sax/ContentHandler;)V

    iget-object v0, p0, Lcom/google/android/ytremote/b/c;->c:Lcom/google/android/ytremote/backend/b/a;

    invoke-virtual {v0}, Lcom/google/android/ytremote/backend/b/a;->d()I

    move-result v3

    if-gez v3, :cond_2

    new-instance v0, Lcom/google/android/ytremote/model/a;

    const/4 v2, -0x2

    invoke-direct {v0, v2}, Lcom/google/android/ytremote/model/a;-><init>(I)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Lorg/xml/sax/SAXException; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    invoke-static {v1}, Lcom/google/android/ytremote/a/c/a;->a(Lorg/apache/http/HttpResponse;)V

    goto :goto_0

    :cond_2
    :try_start_4
    iget-object v0, p0, Lcom/google/android/ytremote/b/c;->c:Lcom/google/android/ytremote/backend/b/a;

    invoke-virtual {v0}, Lcom/google/android/ytremote/backend/b/a;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    :goto_1
    new-instance v0, Lcom/google/android/ytremote/model/a;

    iget-object v4, p0, Lcom/google/android/ytremote/b/c;->c:Lcom/google/android/ytremote/backend/b/a;

    invoke-virtual {v4}, Lcom/google/android/ytremote/backend/b/a;->a()Landroid/net/Uri;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/ytremote/b/c;->c:Lcom/google/android/ytremote/backend/b/a;

    invoke-virtual {v5}, Lcom/google/android/ytremote/backend/b/a;->b()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v3, v4, v5, v2}, Lcom/google/android/ytremote/model/a;-><init>(ILandroid/net/Uri;Ljava/lang/String;Lcom/google/android/ytremote/model/ScreenId;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Lorg/xml/sax/SAXException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    invoke-static {v1}, Lcom/google/android/ytremote/a/c/a;->a(Lorg/apache/http/HttpResponse;)V

    goto :goto_0

    :cond_3
    :try_start_5
    new-instance v2, Lcom/google/android/ytremote/model/ScreenId;

    iget-object v0, p0, Lcom/google/android/ytremote/b/c;->c:Lcom/google/android/ytremote/backend/b/a;

    invoke-virtual {v0}, Lcom/google/android/ytremote/backend/b/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/android/ytremote/model/ScreenId;-><init>(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Lorg/xml/sax/SAXException; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1

    :catch_0
    move-exception v0

    :goto_2
    :try_start_6
    sget-object v2, Lcom/google/android/ytremote/b/c;->a:Ljava/lang/String;

    const-string v3, "Could not send the request to TV."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    invoke-static {v1}, Lcom/google/android/ytremote/a/c/a;->a(Lorg/apache/http/HttpResponse;)V

    :goto_3
    new-instance v0, Lcom/google/android/ytremote/model/a;

    invoke-direct {v0, v6}, Lcom/google/android/ytremote/model/a;-><init>(I)V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    move-object v1, v2

    :goto_4
    :try_start_7
    sget-object v2, Lcom/google/android/ytremote/b/c;->a:Ljava/lang/String;

    const-string v3, "Illegal state exception."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    invoke-static {v1}, Lcom/google/android/ytremote/a/c/a;->a(Lorg/apache/http/HttpResponse;)V

    goto :goto_3

    :catch_2
    move-exception v0

    move-object v1, v2

    :goto_5
    :try_start_8
    sget-object v2, Lcom/google/android/ytremote/b/c;->a:Ljava/lang/String;

    const-string v3, "Sax exception"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    invoke-static {v1}, Lcom/google/android/ytremote/a/c/a;->a(Lorg/apache/http/HttpResponse;)V

    goto :goto_3

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_6
    invoke-static {v1}, Lcom/google/android/ytremote/a/c/a;->a(Lorg/apache/http/HttpResponse;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_6

    :catch_3
    move-exception v0

    goto :goto_5

    :catch_4
    move-exception v0

    goto :goto_4

    :catch_5
    move-exception v0

    move-object v1, v2

    goto :goto_2
.end method
