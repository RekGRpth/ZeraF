.class public Lcom/google/android/ytremote/b/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/ytremote/logic/d;


# static fields
.field private static final a:[J

.field private static final b:Ljava/lang/String;


# instance fields
.field private c:I

.field private final d:Lorg/apache/http/client/HttpClient;

.field private e:J

.field private final f:[J

.field private final g:Lcom/google/android/ytremote/backend/logic/a;

.field private final h:Lcom/google/android/ytremote/b/h;

.field private final i:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final j:Ljava/util/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0xa

    new-array v0, v0, [J

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/ytremote/b/d;->a:[J

    const-class v0, Lcom/google/android/ytremote/b/d;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/ytremote/b/d;->b:Ljava/lang/String;

    return-void

    :array_0
    .array-data 8
        0x1388
        0x7d0
        0x7d0
        0x7d0
        0x7d0
        0x7d0
        0x1388
        0x2710
        0x3a98
        0x4e20
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/ytremote/backend/logic/a;)V
    .locals 3

    new-instance v0, Lcom/google/android/ytremote/b/g;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/ytremote/b/g;-><init>(B)V

    invoke-static {}, Lcom/google/android/ytremote/a/d/a;->b()Lorg/apache/http/impl/client/DefaultHttpClient;

    move-result-object v1

    sget-object v2, Lcom/google/android/ytremote/b/d;->a:[J

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/ytremote/b/d;-><init>(Lcom/google/android/ytremote/backend/logic/a;Lcom/google/android/ytremote/b/h;Lorg/apache/http/client/HttpClient;[J)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/ytremote/backend/logic/a;Lcom/google/android/ytremote/b/h;Lorg/apache/http/client/HttpClient;[J)V
    .locals 2
    .annotation build Lcom/google/android/ytremote/util/VisibleForTesting;
    .end annotation

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/ytremote/b/d;->g:Lcom/google/android/ytremote/backend/logic/a;

    iput-object p2, p0, Lcom/google/android/ytremote/b/d;->h:Lcom/google/android/ytremote/b/h;

    iput-object p3, p0, Lcom/google/android/ytremote/b/d;->d:Lorg/apache/http/client/HttpClient;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/ytremote/b/d;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput v1, p0, Lcom/google/android/ytremote/b/d;->c:I

    new-instance v0, Ljava/util/Timer;

    const-string v1, "Poll for online device"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/ytremote/b/d;->j:Ljava/util/Timer;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/ytremote/b/d;->e:J

    iput-object p4, p0, Lcom/google/android/ytremote/b/d;->f:[J

    return-void
.end method

.method static synthetic a(Lcom/google/android/ytremote/b/d;J)J
    .locals 0

    iput-wide p1, p0, Lcom/google/android/ytremote/b/d;->e:J

    return-wide p1
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/google/android/ytremote/b/d;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/ytremote/b/d;)Lorg/apache/http/client/HttpClient;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/b/d;->d:Lorg/apache/http/client/HttpClient;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/ytremote/b/d;Lcom/google/android/ytremote/logic/e;Lcom/google/android/ytremote/model/CloudScreen;)V
    .locals 1

    if-nez p2, :cond_0

    const/4 v0, 0x3

    invoke-interface {p1, v0}, Lcom/google/android/ytremote/logic/e;->a(I)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1, p2}, Lcom/google/android/ytremote/logic/e;->a(Lcom/google/android/ytremote/model/CloudScreen;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/ytremote/b/d;)V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/ytremote/b/d;->c:I

    return-void
.end method

.method static synthetic c(Lcom/google/android/ytremote/b/d;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/b/d;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/ytremote/b/d;)J
    .locals 3

    iget v0, p0, Lcom/google/android/ytremote/b/d;->c:I

    iget-object v1, p0, Lcom/google/android/ytremote/b/d;->f:[J

    array-length v1, v1

    if-lt v0, v1, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/ytremote/b/d;->f:[J

    iget v1, p0, Lcom/google/android/ytremote/b/d;->c:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/ytremote/b/d;->c:I

    aget-wide v0, v0, v1

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/android/ytremote/b/d;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/ytremote/b/d;->e:J

    return-wide v0
.end method

.method static synthetic f(Lcom/google/android/ytremote/b/d;)Ljava/util/Timer;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/b/d;->j:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/ytremote/b/d;)Lcom/google/android/ytremote/backend/logic/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/b/d;->g:Lcom/google/android/ytremote/backend/logic/a;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/net/Uri;)V
    .locals 2

    new-instance v0, Lcom/google/android/ytremote/b/f;

    const-string v1, "Stopping YouTubeTV"

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/ytremote/b/f;-><init>(Lcom/google/android/ytremote/b/d;Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v0}, Lcom/google/android/ytremote/b/f;->start()V

    return-void
.end method

.method public final a(Landroid/net/Uri;Lcom/google/android/ytremote/logic/e;)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/ytremote/b/d;->h:Lcom/google/android/ytremote/b/h;

    invoke-interface {v0}, Lcom/google/android/ytremote/b/h;->a()Lcom/google/android/ytremote/model/PairingCode;

    move-result-object v4

    new-instance v0, Lcom/google/android/ytremote/b/e;

    const-string v2, "Launching YouTubeTV"

    move-object v1, p0

    move-object v3, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/ytremote/b/e;-><init>(Lcom/google/android/ytremote/b/d;Ljava/lang/String;Landroid/net/Uri;Lcom/google/android/ytremote/model/PairingCode;Lcom/google/android/ytremote/logic/e;)V

    invoke-virtual {v0}, Lcom/google/android/ytremote/b/e;->start()V

    return-void
.end method
