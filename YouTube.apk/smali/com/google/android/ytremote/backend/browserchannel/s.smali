.class final Lcom/google/android/ytremote/backend/browserchannel/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/ytremote/backend/browserchannel/e;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/ytremote/backend/browserchannel/a;

.field private final c:Lcom/google/android/ytremote/backend/browserchannel/u;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/ytremote/backend/browserchannel/s;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/ytremote/backend/browserchannel/s;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/ytremote/backend/browserchannel/a;Lcom/google/android/ytremote/backend/browserchannel/u;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/ytremote/backend/browserchannel/s;->b:Lcom/google/android/ytremote/backend/browserchannel/a;

    iput-object p2, p0, Lcom/google/android/ytremote/backend/browserchannel/s;->c:Lcom/google/android/ytremote/backend/browserchannel/u;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 6

    const/4 v0, 0x0

    const/4 v5, 0x1

    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getInt(I)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/ytremote/backend/browserchannel/s;->b:Lcom/google/android/ytremote/backend/browserchannel/a;

    invoke-virtual {v4, v3}, Lcom/google/android/ytremote/backend/browserchannel/a;->a(I)V

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->getJSONArray(I)Lorg/json/JSONArray;

    move-result-object v2

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-lez v4, :cond_0

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/google/android/ytremote/backend/browserchannel/s;->b:Lcom/google/android/ytremote/backend/browserchannel/a;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/ytremote/backend/browserchannel/a;->c:Ljava/lang/String;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    if-ne v3, v5, :cond_3

    iget-object v3, p0, Lcom/google/android/ytremote/backend/browserchannel/s;->b:Lcom/google/android/ytremote/backend/browserchannel/a;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/android/ytremote/backend/browserchannel/a;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    sget-object v1, Lcom/google/android/ytremote/backend/browserchannel/s;->a:Ljava/lang/String;

    const-string v2, "Chunk stream error"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    return-void

    :cond_3
    if-le v3, v5, :cond_0

    :try_start_1
    iget-object v3, p0, Lcom/google/android/ytremote/backend/browserchannel/s;->c:Lcom/google/android/ytremote/backend/browserchannel/u;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/ytremote/backend/browserchannel/s;->c:Lcom/google/android/ytremote/backend/browserchannel/u;

    invoke-interface {v3, v2}, Lcom/google/android/ytremote/backend/browserchannel/u;->a(Lorg/json/JSONArray;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
