.class final Lcom/google/android/ytremote/backend/browserchannel/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field final synthetic a:Lcom/google/android/ytremote/backend/browserchannel/k;


# direct methods
.method constructor <init>(Lcom/google/android/ytremote/backend/browserchannel/k;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/ytremote/backend/browserchannel/o;->a:Lcom/google/android/ytremote/backend/browserchannel/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Ljava/lang/Void;
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/o;->a:Lcom/google/android/ytremote/backend/browserchannel/k;

    invoke-static {v0}, Lcom/google/android/ytremote/backend/browserchannel/k;->l(Lcom/google/android/ytremote/backend/browserchannel/k;)Lcom/google/android/ytremote/backend/browserchannel/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/ytremote/backend/browserchannel/r;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/android/ytremote/backend/browserchannel/k;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Message: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/ytremote/backend/browserchannel/o;->a:Lcom/google/android/ytremote/backend/browserchannel/k;

    invoke-static {v2}, Lcom/google/android/ytremote/backend/browserchannel/k;->l(Lcom/google/android/ytremote/backend/browserchannel/k;)Lcom/google/android/ytremote/backend/browserchannel/r;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is older than "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget v2, Lcom/google/android/ytremote/backend/browserchannel/r;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms. Dropping."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/o;->a:Lcom/google/android/ytremote/backend/browserchannel/k;

    invoke-static {v0}, Lcom/google/android/ytremote/backend/browserchannel/k;->m(Lcom/google/android/ytremote/backend/browserchannel/k;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/o;->a:Lcom/google/android/ytremote/backend/browserchannel/k;

    iget-object v1, p0, Lcom/google/android/ytremote/backend/browserchannel/o;->a:Lcom/google/android/ytremote/backend/browserchannel/k;

    invoke-static {v1}, Lcom/google/android/ytremote/backend/browserchannel/k;->l(Lcom/google/android/ytremote/backend/browserchannel/k;)Lcom/google/android/ytremote/backend/browserchannel/r;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/ytremote/backend/browserchannel/r;->b:Ljava/util/List;

    sget-object v2, Lcom/google/android/ytremote/backend/logic/CloudService$OnSendMessageResult$SendMessageResult;->CANCELED:Lcom/google/android/ytremote/backend/logic/CloudService$OnSendMessageResult$SendMessageResult;

    invoke-static {v0, v1, v2}, Lcom/google/android/ytremote/backend/browserchannel/k;->a(Lcom/google/android/ytremote/backend/browserchannel/k;Ljava/util/List;Lcom/google/android/ytremote/backend/logic/CloudService$OnSendMessageResult$SendMessageResult;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/o;->a:Lcom/google/android/ytremote/backend/browserchannel/k;

    invoke-static {v0}, Lcom/google/android/ytremote/backend/browserchannel/k;->e(Lcom/google/android/ytremote/backend/browserchannel/k;)V

    const/4 v0, 0x0

    return-object v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/o;->a:Lcom/google/android/ytremote/backend/browserchannel/k;

    iget-object v1, p0, Lcom/google/android/ytremote/backend/browserchannel/o;->a:Lcom/google/android/ytremote/backend/browserchannel/k;

    invoke-static {v1}, Lcom/google/android/ytremote/backend/browserchannel/k;->l(Lcom/google/android/ytremote/backend/browserchannel/k;)Lcom/google/android/ytremote/backend/browserchannel/r;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/ytremote/backend/browserchannel/r;->c:Lcom/google/android/ytremote/backend/model/Method;

    iget-object v2, p0, Lcom/google/android/ytremote/backend/browserchannel/o;->a:Lcom/google/android/ytremote/backend/browserchannel/k;

    invoke-static {v2}, Lcom/google/android/ytremote/backend/browserchannel/k;->l(Lcom/google/android/ytremote/backend/browserchannel/k;)Lcom/google/android/ytremote/backend/browserchannel/r;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/ytremote/backend/browserchannel/r;->d:Lcom/google/android/ytremote/backend/model/Params;

    iget-object v3, p0, Lcom/google/android/ytremote/backend/browserchannel/o;->a:Lcom/google/android/ytremote/backend/browserchannel/k;

    invoke-static {v3}, Lcom/google/android/ytremote/backend/browserchannel/k;->l(Lcom/google/android/ytremote/backend/browserchannel/k;)Lcom/google/android/ytremote/backend/browserchannel/r;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/ytremote/backend/browserchannel/r;->b:Ljava/util/List;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/ytremote/backend/browserchannel/k;->a(Lcom/google/android/ytremote/backend/browserchannel/k;Lcom/google/android/ytremote/backend/model/Method;Lcom/google/android/ytremote/backend/model/Params;Ljava/util/List;)V

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/o;->a:Lcom/google/android/ytremote/backend/browserchannel/k;

    invoke-static {v0}, Lcom/google/android/ytremote/backend/browserchannel/k;->n(Lcom/google/android/ytremote/backend/browserchannel/k;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/o;->a:Lcom/google/android/ytremote/backend/browserchannel/k;

    invoke-static {v0}, Lcom/google/android/ytremote/backend/browserchannel/k;->n(Lcom/google/android/ytremote/backend/browserchannel/k;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ScheduledFuture;->cancel(Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/ytremote/backend/browserchannel/o;->a:Lcom/google/android/ytremote/backend/browserchannel/k;

    invoke-static {v1}, Lcom/google/android/ytremote/backend/browserchannel/k;->e(Lcom/google/android/ytremote/backend/browserchannel/k;)V

    throw v0
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    invoke-direct {p0}, Lcom/google/android/ytremote/backend/browserchannel/o;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
