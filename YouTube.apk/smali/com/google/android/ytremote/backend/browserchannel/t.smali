.class public final Lcom/google/android/ytremote/backend/browserchannel/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/ytremote/backend/browserchannel/c;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Landroid/content/Context;

.field private final e:Ljava/util/Map;

.field private final f:Lcom/google/android/ytremote/backend/browserchannel/u;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/ytremote/backend/browserchannel/u;Ljava/util/Map;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/ytremote/backend/browserchannel/t;->d:Landroid/content/Context;

    iput-object p2, p0, Lcom/google/android/ytremote/backend/browserchannel/t;->c:Ljava/lang/String;

    const/16 v0, 0x50

    iput v0, p0, Lcom/google/android/ytremote/backend/browserchannel/t;->b:I

    iput-object p4, p0, Lcom/google/android/ytremote/backend/browserchannel/t;->a:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/ytremote/backend/browserchannel/t;->f:Lcom/google/android/ytremote/backend/browserchannel/u;

    iput-object p6, p0, Lcom/google/android/ytremote/backend/browserchannel/t;->e:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/ytremote/backend/model/a;)Lcom/google/android/ytremote/backend/browserchannel/a;
    .locals 10

    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/ytremote/backend/model/a;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "method"

    invoke-virtual {p1}, Lcom/google/android/ytremote/backend/model/a;->b()Lcom/google/android/ytremote/backend/model/Method;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/ytremote/backend/model/Method;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v7, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/ytremote/backend/model/a;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "params"

    invoke-virtual {p1}, Lcom/google/android/ytremote/backend/model/a;->c()Lcom/google/android/ytremote/backend/model/Params;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/ytremote/util/b;->a(Lcom/google/android/ytremote/backend/model/Params;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v7, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/ytremote/backend/model/a;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "ui"

    const-string v1, ""

    invoke-interface {v7, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/ytremote/backend/model/a;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "X-YouTube-LoungeId-Token"

    invoke-virtual {p1}, Lcom/google/android/ytremote/backend/model/a;->d()Lcom/google/android/ytremote/model/LoungeToken;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/ytremote/model/LoungeToken;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v9, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/ytremote/backend/model/a;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "Authorization"

    invoke-virtual {p1}, Lcom/google/android/ytremote/backend/model/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v9, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    new-instance v0, Lcom/google/android/ytremote/backend/browserchannel/a;

    iget-object v1, p0, Lcom/google/android/ytremote/backend/browserchannel/t;->d:Landroid/content/Context;

    invoke-static {}, Lcom/google/net/async/x;->a()Lcom/google/net/async/p;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/ytremote/backend/browserchannel/t;->c:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/ytremote/backend/browserchannel/t;->b:I

    iget-object v5, p0, Lcom/google/android/ytremote/backend/browserchannel/t;->a:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/ytremote/backend/browserchannel/t;->f:Lcom/google/android/ytremote/backend/browserchannel/u;

    iget-object v8, p0, Lcom/google/android/ytremote/backend/browserchannel/t;->e:Ljava/util/Map;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/ytremote/backend/browserchannel/a;-><init>(Landroid/content/Context;Lcom/google/net/async/p;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/ytremote/backend/browserchannel/u;Ljava/util/Map;Ljava/util/Map;Ljava/util/Map;)V

    return-object v0
.end method
