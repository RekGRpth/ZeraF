.class Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/logging/Logger;

.field private static final b:Ljava/lang/String;

.field private static c:Z


# instance fields
.field private d:Ljava/net/InetSocketAddress;

.field private e:Ljava/io/ByteArrayOutputStream;

.field private f:I

.field private g:Ljava/io/ByteArrayOutputStream;

.field private h:Lcom/google/net/async/l;

.field private i:I

.field private final j:Landroid/content/Context;

.field private k:Ljava/util/Map;

.field private l:Lcom/google/net/async/p;

.field private m:Lcom/google/android/ytremote/backend/browserchannel/i;

.field private n:Ljava/io/ByteArrayOutputStream;

.field private o:Ljava/util/Map;

.field private p:Z

.field private final q:Ljava/util/concurrent/LinkedBlockingQueue;

.field private r:Ljava/io/ByteArrayOutputStream;

.field private s:I

.field private t:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->a:Ljava/util/logging/Logger;

    const-class v0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->b:Ljava/lang/String;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->c:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/net/async/p;Ljava/lang/String;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->l:Lcom/google/net/async/p;

    new-instance v0, Ljava/net/InetSocketAddress;

    invoke-direct {v0, p3, p4}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->d:Ljava/net/InetSocketAddress;

    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->q:Ljava/util/concurrent/LinkedBlockingQueue;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->h:Lcom/google/net/async/l;

    sget-object v0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;->HEADER:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->t:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->p:Z

    iput-object p1, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->j:Landroid/content/Context;

    return-void
.end method

.method private a([BIIZ)I
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, -0x1

    invoke-static {p1, p2, p3}, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->b([BII)I

    move-result v1

    if-eq v1, v5, :cond_8

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->n:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, p1, p2, v1}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->n:Ljava/io/ByteArrayOutputStream;

    const-string v2, "UTF-8"

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    if-eqz p4, :cond_3

    sget-object v0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;->BODY:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->t:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->o:Ljava/util/Map;

    const-string v2, "transfer-encoding"

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "chunked"

    iget-object v2, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->o:Ljava/util/Map;

    const-string v3, "transfer-encoding"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;->CHUNK_SIZE:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->t:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->n:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->reset()V

    add-int/lit8 p3, v1, 0x2

    :goto_1
    return p3

    :cond_1
    new-instance v0, Lcom/google/android/ytremote/logic/exception/HttpConnectionException;

    const-string v1, "Unknown transfer-encoding"

    invoke-direct {v0, v1}, Lcom/google/android/ytremote/logic/exception/HttpConnectionException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->o:Ljava/util/Map;

    const-string v2, "content-length"

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->o:Ljava/util/Map;

    const-string v2, "content-length"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->i:I

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;->DONE:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->t:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;

    iput-boolean v7, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->p:Z

    goto :goto_0

    :cond_4
    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-ne v2, v5, :cond_5

    if-eqz p4, :cond_5

    iget v3, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->s:I

    if-nez v3, :cond_5

    const-string v2, "HTTP/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v2

    if-eq v2, v5, :cond_0

    const-string v3, " "

    add-int/lit8 v4, v2, 0x1

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v3

    if-eq v3, v5, :cond_0

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->s:I

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->m:Lcom/google/android/ytremote/backend/browserchannel/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->m:Lcom/google/android/ytremote/backend/browserchannel/i;

    iget v2, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->s:I

    invoke-interface {v0, v2}, Lcom/google/android/ytremote/backend/browserchannel/i;->a(I)V

    goto :goto_0

    :cond_5
    iget v3, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->s:I

    if-nez v3, :cond_6

    new-instance v0, Lcom/google/android/ytremote/logic/exception/HttpConnectionException;

    const-string v1, "Received header fields without HTTP response code"

    invoke-direct {v0, v1}, Lcom/google/android/ytremote/logic/exception/HttpConnectionException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    add-int/lit8 v3, v2, 0x1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v6, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v2, "set-cookie"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v0, ";"

    invoke-virtual {v3, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, v6

    const-string v2, "="

    const/4 v3, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->k:Ljava/util/Map;

    aget-object v3, v0, v6

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    aget-object v0, v0, v7

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_7
    iget-object v2, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->o:Ljava/util/Map;

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->n:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto/16 :goto_1
.end method

.method static synthetic a(Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;)Lcom/google/net/async/l;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->h:Lcom/google/net/async/l;

    return-object v0
.end method

.method private a([BII)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->r:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->m:Lcom/google/android/ytremote/backend/browserchannel/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->m:Lcom/google/android/ytremote/backend/browserchannel/i;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/ytremote/backend/browserchannel/i;->a([BII)V

    :cond_0
    return-void
.end method

.method private static b([BII)I
    .locals 3

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v1, p2, -0x1

    if-ge v0, v1, :cond_1

    add-int v1, p1, v0

    aget-byte v1, p0, v1

    const/16 v2, 0xd

    if-ne v1, v2, :cond_0

    add-int v1, p1, v0

    add-int/lit8 v1, v1, 0x1

    aget-byte v1, p0, v1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method static synthetic b(Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;)Ljava/util/concurrent/LinkedBlockingQueue;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->q:Ljava/util/concurrent/LinkedBlockingQueue;

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a(Ljava/lang/String;)Lcom/google/android/ytremote/backend/browserchannel/j;
    .locals 11

    const/16 v5, 0xa

    const/4 v3, 0x1

    const/4 v4, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->j:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/ytremote/a/d/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->b:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No network connection - request: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not sent."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/google/android/ytremote/backend/browserchannel/NetworkNotAvailableException;

    const-string v1, "No network connection"

    invoke-direct {v0, v1}, Lcom/google/android/ytremote/backend/browserchannel/NetworkNotAvailableException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->s:I

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->r:Ljava/io/ByteArrayOutputStream;

    sget-object v0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;->HEADER:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->t:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->f:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->p:Z

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->n:Ljava/io/ByteArrayOutputStream;

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->g:Ljava/io/ByteArrayOutputStream;

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->e:Ljava/io/ByteArrayOutputStream;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->o:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->k:Ljava/util/Map;

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->h:Lcom/google/net/async/l;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_1

    :try_start_2
    invoke-virtual {p0}, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->a()V

    new-instance v0, Lcom/google/net/async/l;

    iget-object v1, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->l:Lcom/google/net/async/p;

    new-instance v2, Lcom/google/android/ytremote/backend/browserchannel/f;

    invoke-direct {v2, p0}, Lcom/google/android/ytremote/backend/browserchannel/f;-><init>(Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;)V

    invoke-direct {v0, v1, v2}, Lcom/google/net/async/l;-><init>(Lcom/google/net/async/u;Lcom/google/net/async/f;)V

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->h:Lcom/google/net/async/l;

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->h:Lcom/google/net/async/l;

    iget-object v1, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->d:Ljava/net/InetSocketAddress;

    new-instance v2, Lcom/google/android/ytremote/backend/browserchannel/g;

    invoke-direct {v2, p0}, Lcom/google/android/ytremote/backend/browserchannel/g;-><init>(Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/net/async/l;->a(Ljava/net/InetSocketAddress;Lcom/google/net/async/n;)V

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->h:Lcom/google/net/async/l;

    invoke-virtual {v0}, Lcom/google/net/async/l;->d()V
    :try_end_2
    .catch Ljava/lang/AssertionError; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_1
    :try_start_3
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->h:Lcom/google/net/async/l;

    invoke-virtual {v0}, Lcom/google/net/async/l;->c()Ljava/io/OutputStream;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->h:Lcom/google/net/async/l;

    invoke-virtual {v0}, Lcom/google/net/async/l;->e()V

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    :cond_2
    iget-boolean v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->p:Z

    if-nez v0, :cond_16

    sget-object v0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;->BODY:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;

    iget-object v2, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->t:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;

    invoke-virtual {v0, v2}, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->i:I

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->q:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->a()V

    move v0, v3

    :goto_0
    iget-object v2, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->h:Lcom/google/net/async/l;

    if-nez v2, :cond_12

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->q:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_13

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    :goto_1
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->q:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->q:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem;

    invoke-virtual {v0}, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem;->c()Z

    move-result v2

    if-eqz v2, :cond_f

    const-string v0, "\nclosed event"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/ytremote/logic/exception/HttpConnectionException;

    const-string v2, "Could not open socket"

    invoke-direct {v1, v2, v0}, Lcom/google/android/ytremote/logic/exception/HttpConnectionException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_3
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->q:Ljava/util/concurrent/LinkedBlockingQueue;

    const-wide/16 v6, 0x3c

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v6, v7, v2}, Ljava/util/concurrent/LinkedBlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem;->c()Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_4
    invoke-virtual {p0}, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->a()V

    move v0, v3

    goto :goto_0

    :cond_5
    invoke-virtual {v0}, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem;->d()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {p0}, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->a()V

    new-instance v1, Lcom/google/android/ytremote/logic/exception/HttpConnectionException;

    const-string v2, "Error occurred in connection"

    invoke-virtual {v0}, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem;->b()Ljava/lang/Exception;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/google/android/ytremote/logic/exception/HttpConnectionException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_6
    invoke-virtual {v0}, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/ByteArrayOutputStream;->write([B)V

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v7

    move v6, v4

    :goto_2
    if-eqz v2, :cond_2

    sget-boolean v0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->c:Z

    if-eqz v0, :cond_7

    sget-object v8, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->a:Ljava/util/logging/Logger;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v9, "PRE offset = "

    invoke-direct {v0, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, "; lengthRemaining = "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, "; responseState = "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v9, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->t:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, "; chunkLength = "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v9, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->f:I

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, "\nNext few bytes: "

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    new-instance v10, Ljava/lang/String;

    if-ge v2, v5, :cond_a

    move v0, v2

    :goto_3
    invoke-direct {v10, v7, v6, v0}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, "\n-----------------------"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_7
    sget-object v0, Lcom/google/android/ytremote/backend/browserchannel/h;->a:[I

    iget-object v8, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->t:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;

    invoke-virtual {v8}, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;->ordinal()I

    move-result v8

    aget v0, v0, v8

    packed-switch v0, :pswitch_data_0

    move v0, v2

    :goto_4
    sget-boolean v8, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->c:Z

    if-eqz v8, :cond_8

    sget-object v8, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->a:Ljava/util/logging/Logger;

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "POST ; responseState = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->t:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "; consumedNow = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\nconsumed: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    new-instance v10, Ljava/lang/String;

    invoke-direct {v10, v7, v6, v0}, Ljava/lang/String;-><init>([BII)V

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\n-----------------------"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    :cond_8
    if-nez v0, :cond_e

    iget-object v8, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->t:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;

    sget-object v9, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;->BODY:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;

    if-ne v8, v9, :cond_9

    iget v8, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->i:I

    if-eqz v8, :cond_e

    :cond_9
    new-instance v0, Lcom/google/android/ytremote/logic/exception/HttpConnectionException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Unable to process data in state "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->t:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\nlengthRemaining: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\ncontentLength: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->i:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\noffset: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nbytes: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nRequest:\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nResponse:\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/lang/String;-><init>([B)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/ytremote/logic/exception/HttpConnectionException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    move v0, v5

    goto/16 :goto_3

    :pswitch_0
    const/4 v0, 0x1

    invoke-direct {p0, v7, v6, v2, v0}, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->a([BIIZ)I

    move-result v0

    goto/16 :goto_4

    :pswitch_1
    iget v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->i:I

    iget-object v8, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->r:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v8

    sub-int/2addr v0, v8

    if-lt v2, v0, :cond_15

    iget v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->i:I

    iget-object v8, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->r:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v8

    sub-int/2addr v0, v8

    const/4 v8, 0x1

    iput-boolean v8, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->p:Z

    :goto_5
    invoke-direct {p0, v7, v6, v0}, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->a([BII)V

    goto/16 :goto_4

    :pswitch_2
    move v0, v2

    goto/16 :goto_4

    :pswitch_3
    const/4 v0, 0x0

    invoke-direct {p0, v7, v6, v2, v0}, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->a([BIIZ)I

    move-result v0

    goto/16 :goto_4

    :pswitch_4
    invoke-static {v7, v6, v2}, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->b([BII)I

    move-result v0

    const/4 v8, -0x1

    if-eq v0, v8, :cond_c

    sget-object v8, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;->CHUNK_DATA:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;

    iput-object v8, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->t:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;

    iget-object v8, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->g:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v8, v7, v6, v0}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    iget-object v8, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->g:Ljava/io/ByteArrayOutputStream;

    const-string v9, "UTF-8"

    invoke-virtual {v8, v9}, Ljava/io/ByteArrayOutputStream;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/16 v9, 0x10

    invoke-static {v8, v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v8

    iput v8, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->f:I

    iget v8, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->f:I

    if-nez v8, :cond_b

    sget-object v8, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;->TRAILER:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;

    iput-object v8, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->t:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;

    :cond_b
    iget-object v8, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->g:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->reset()V

    add-int/lit8 v0, v0, 0x2

    goto/16 :goto_4

    :cond_c
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->g:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, v7, v6, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    move v0, v2

    goto/16 :goto_4

    :pswitch_5
    iget v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->f:I

    if-lt v2, v0, :cond_14

    iget v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->f:I

    sget-object v8, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;->CHUNK_END:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;

    iput-object v8, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->t:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;

    :goto_6
    invoke-direct {p0, v7, v6, v0}, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->a([BII)V

    iget v8, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->f:I

    sub-int/2addr v8, v0

    iput v8, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->f:I

    goto/16 :goto_4

    :pswitch_6
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->e:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v0

    add-int v8, v2, v0

    const/4 v9, 0x2

    if-lt v8, v9, :cond_d

    rsub-int/lit8 v0, v0, 0x2

    iget-object v8, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->e:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v8, v7, v6, v0}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    sget-object v8, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;->CHUNK_SIZE:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;

    iput-object v8, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->t:Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$ResponseState;

    iget-object v8, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->e:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->reset()V

    goto/16 :goto_4

    :cond_d
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->e:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, v7, v6, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    move v0, v2

    goto/16 :goto_4

    :cond_e
    add-int/2addr v6, v0

    sub-int/2addr v2, v0

    goto/16 :goto_2

    :cond_f
    invoke-virtual {v0}, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem;->d()Z

    move-result v2

    if-eqz v2, :cond_10

    const-string v2, "\nerror: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem;->b()Ljava/lang/Exception;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    :cond_10
    const-string v2, "\ndata: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection$IncomingItem;->a()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_1

    :cond_11
    new-instance v0, Lcom/google/android/ytremote/logic/exception/HttpConnectionException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Request completed with data still in the incoming queue:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/ytremote/logic/exception/HttpConnectionException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_12
    iget-boolean v2, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->p:Z

    if-nez v2, :cond_13

    if-nez v0, :cond_13

    new-instance v0, Lcom/google/android/ytremote/logic/exception/HttpConnectionException;

    const-string v1, "Abrupt connection end"

    invoke-direct {v0, v1}, Lcom/google/android/ytremote/logic/exception/HttpConnectionException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_13
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->r:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    new-instance v0, Lcom/google/android/ytremote/backend/browserchannel/j;

    iget-boolean v2, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->p:Z

    iget v3, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->s:I

    iget-object v5, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->o:Ljava/util/Map;

    iget-object v6, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->k:Ljava/util/Map;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/ytremote/backend/browserchannel/j;-><init>(Ljava/io/ByteArrayOutputStream;ZI[BLjava/util/Map;Ljava/util/Map;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_14
    move v0, v2

    goto/16 :goto_6

    :cond_15
    move v0, v2

    goto/16 :goto_5

    :cond_16
    move v0, v4

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final a()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->h:Lcom/google/net/async/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->h:Lcom/google/net/async/l;

    invoke-virtual {v0}, Lcom/google/net/async/l;->f()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->h:Lcom/google/net/async/l;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/ytremote/backend/browserchannel/i;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/ytremote/backend/browserchannel/HttpClientConnection;->m:Lcom/google/android/ytremote/backend/browserchannel/i;

    return-void
.end method
