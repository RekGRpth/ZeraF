.class final Lcom/google/android/ytremote/backend/browserchannel/m;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/util/concurrent/CountDownLatch;

.field final synthetic b:Lcom/google/android/ytremote/backend/browserchannel/k;


# direct methods
.method constructor <init>(Lcom/google/android/ytremote/backend/browserchannel/k;Ljava/lang/String;Ljava/util/concurrent/CountDownLatch;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/ytremote/backend/browserchannel/m;->b:Lcom/google/android/ytremote/backend/browserchannel/k;

    iput-object p3, p0, Lcom/google/android/ytremote/backend/browserchannel/m;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/m;->b:Lcom/google/android/ytremote/backend/browserchannel/k;

    invoke-static {v0}, Lcom/google/android/ytremote/backend/browserchannel/k;->c(Lcom/google/android/ytremote/backend/browserchannel/k;)Lcom/google/android/ytremote/backend/browserchannel/c;

    move-result-object v0

    new-instance v1, Lcom/google/android/ytremote/backend/model/b;

    invoke-direct {v1}, Lcom/google/android/ytremote/backend/model/b;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/ytremote/backend/model/b;->a()Lcom/google/android/ytremote/backend/model/a;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/ytremote/backend/browserchannel/c;->a(Lcom/google/android/ytremote/backend/model/a;)Lcom/google/android/ytremote/backend/browserchannel/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/ytremote/backend/browserchannel/m;->b:Lcom/google/android/ytremote/backend/browserchannel/k;

    invoke-virtual {v0}, Lcom/google/android/ytremote/backend/browserchannel/a;->b()Z

    move-result v0

    invoke-static {v1, v0}, Lcom/google/android/ytremote/backend/browserchannel/k;->d(Lcom/google/android/ytremote/backend/browserchannel/k;Z)Z

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/m;->b:Lcom/google/android/ytremote/backend/browserchannel/k;

    iget-object v1, p0, Lcom/google/android/ytremote/backend/browserchannel/m;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-static {v0, v1}, Lcom/google/android/ytremote/backend/browserchannel/k;->a(Lcom/google/android/ytremote/backend/browserchannel/k;Ljava/util/concurrent/CountDownLatch;)V
    :try_end_0
    .catch Lcom/google/net/async/IpV6BugException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {}, Lcom/google/android/ytremote/backend/browserchannel/k;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "IP Address of the phone is: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/ytremote/backend/browserchannel/m;->b:Lcom/google/android/ytremote/backend/browserchannel/k;

    invoke-static {v2}, Lcom/google/android/ytremote/backend/browserchannel/k;->g(Lcom/google/android/ytremote/backend/browserchannel/k;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "java.net.preferIPv4Stack"

    const-string v1, "true"

    invoke-static {v0, v1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    const-string v0, "java.net.preferIPv6Addresses"

    const-string v1, "false"

    invoke-static {v0, v1}, Ljava/lang/System;->setProperty(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {}, Lcom/google/android/ytremote/backend/browserchannel/k;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "This device suffers from issue 9431 on code.google.com - setting java.net.preferIPv6Addresses to false, java.net.preferIPv4Stack to true"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/m;->b:Lcom/google/android/ytremote/backend/browserchannel/k;

    invoke-static {v0}, Lcom/google/android/ytremote/backend/browserchannel/k;->c(Lcom/google/android/ytremote/backend/browserchannel/k;)Lcom/google/android/ytremote/backend/browserchannel/c;

    move-result-object v0

    new-instance v1, Lcom/google/android/ytremote/backend/model/b;

    invoke-direct {v1}, Lcom/google/android/ytremote/backend/model/b;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/ytremote/backend/model/b;->a()Lcom/google/android/ytremote/backend/model/a;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/ytremote/backend/browserchannel/c;->a(Lcom/google/android/ytremote/backend/model/a;)Lcom/google/android/ytremote/backend/browserchannel/a;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/m;->b:Lcom/google/android/ytremote/backend/browserchannel/k;

    iget-object v1, p0, Lcom/google/android/ytremote/backend/browserchannel/m;->b:Lcom/google/android/ytremote/backend/browserchannel/k;

    invoke-static {v1}, Lcom/google/android/ytremote/backend/browserchannel/k;->h(Lcom/google/android/ytremote/backend/browserchannel/k;)Lcom/google/android/ytremote/backend/browserchannel/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/ytremote/backend/browserchannel/a;->b()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/ytremote/backend/browserchannel/k;->d(Lcom/google/android/ytremote/backend/browserchannel/k;Z)Z

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/m;->b:Lcom/google/android/ytremote/backend/browserchannel/k;

    iget-object v1, p0, Lcom/google/android/ytremote/backend/browserchannel/m;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-static {v0, v1}, Lcom/google/android/ytremote/backend/browserchannel/k;->a(Lcom/google/android/ytremote/backend/browserchannel/k;Ljava/util/concurrent/CountDownLatch;)V
    :try_end_2
    .catch Lcom/google/net/async/IpV6BugException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :goto_1
    :try_start_3
    invoke-static {}, Lcom/google/android/ytremote/backend/browserchannel/k;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "You\'re lucky - that seems to have worked!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-static {}, Lcom/google/android/ytremote/backend/browserchannel/k;->c()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error testing for buffered proxy. Will assume the worst (buffered proxy)"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v0, p0, Lcom/google/android/ytremote/backend/browserchannel/m;->b:Lcom/google/android/ytremote/backend/browserchannel/k;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/ytremote/backend/browserchannel/k;->d(Lcom/google/android/ytremote/backend/browserchannel/k;Z)Z

    goto :goto_0

    :catch_2
    move-exception v0

    :try_start_4
    invoke-static {}, Lcom/google/android/ytremote/backend/browserchannel/k;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Tough luck, still can\'t connect. The remote control will not work"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/google/android/ytremote/intent/Intents$IntentAction;->CLOUD_SERVICE_IPV6_ERROR:Lcom/google/android/ytremote/intent/Intents$IntentAction;

    invoke-virtual {v0}, Lcom/google/android/ytremote/intent/Intents$IntentAction;->asIntent()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/ytremote/backend/browserchannel/m;->b:Lcom/google/android/ytremote/backend/browserchannel/k;

    invoke-static {v1}, Lcom/google/android/ytremote/backend/browserchannel/k;->i(Lcom/google/android/ytremote/backend/browserchannel/k;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1
.end method
