.class public final Lcom/google/android/ytremote/backend/model/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/google/android/ytremote/backend/model/Method;

.field private final c:Lcom/google/android/ytremote/backend/model/Params;

.field private final d:Lcom/google/android/ytremote/model/LoungeToken;

.field private final e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/ytremote/backend/model/b;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/ytremote/backend/model/b;->a(Lcom/google/android/ytremote/backend/model/b;)Lcom/google/android/ytremote/model/LoungeToken;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ytremote/backend/model/a;->d:Lcom/google/android/ytremote/model/LoungeToken;

    invoke-static {p1}, Lcom/google/android/ytremote/backend/model/b;->b(Lcom/google/android/ytremote/backend/model/b;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ytremote/backend/model/a;->a:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/ytremote/backend/model/b;->c(Lcom/google/android/ytremote/backend/model/b;)Lcom/google/android/ytremote/backend/model/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ytremote/backend/model/a;->b:Lcom/google/android/ytremote/backend/model/Method;

    invoke-static {p1}, Lcom/google/android/ytremote/backend/model/b;->d(Lcom/google/android/ytremote/backend/model/b;)Lcom/google/android/ytremote/backend/model/Params;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/ytremote/backend/model/a;->c:Lcom/google/android/ytremote/backend/model/Params;

    invoke-static {p1}, Lcom/google/android/ytremote/backend/model/b;->e(Lcom/google/android/ytremote/backend/model/b;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/ytremote/backend/model/a;->e:Z

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/backend/model/a;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Lcom/google/android/ytremote/backend/model/Method;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/backend/model/a;->b:Lcom/google/android/ytremote/backend/model/Method;

    return-object v0
.end method

.method public final c()Lcom/google/android/ytremote/backend/model/Params;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/backend/model/a;->c:Lcom/google/android/ytremote/backend/model/Params;

    return-object v0
.end method

.method public final d()Lcom/google/android/ytremote/model/LoungeToken;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/backend/model/a;->d:Lcom/google/android/ytremote/model/LoungeToken;

    return-object v0
.end method

.method public final e()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/backend/model/a;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/backend/model/a;->b:Lcom/google/android/ytremote/backend/model/Method;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/backend/model/a;->c:Lcom/google/android/ytremote/backend/model/Params;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/backend/model/a;->d:Lcom/google/android/ytremote/model/LoungeToken;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/ytremote/backend/model/a;->e:Z

    return v0
.end method
