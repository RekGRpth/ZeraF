.class public final Lcom/google/android/ytremote/backend/model/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/google/android/ytremote/backend/model/Method;

.field private c:Lcom/google/android/ytremote/backend/model/Params;

.field private d:Lcom/google/android/ytremote/model/LoungeToken;

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/ytremote/backend/model/b;)Lcom/google/android/ytremote/model/LoungeToken;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/backend/model/b;->d:Lcom/google/android/ytremote/model/LoungeToken;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/ytremote/backend/model/b;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/backend/model/b;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/ytremote/backend/model/b;)Lcom/google/android/ytremote/backend/model/Method;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/backend/model/b;->b:Lcom/google/android/ytremote/backend/model/Method;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/ytremote/backend/model/b;)Lcom/google/android/ytremote/backend/model/Params;
    .locals 1

    iget-object v0, p0, Lcom/google/android/ytremote/backend/model/b;->c:Lcom/google/android/ytremote/backend/model/Params;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/ytremote/backend/model/b;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/ytremote/backend/model/b;->e:Z

    return v0
.end method


# virtual methods
.method public final a()Lcom/google/android/ytremote/backend/model/a;
    .locals 1

    new-instance v0, Lcom/google/android/ytremote/backend/model/a;

    invoke-direct {v0, p0}, Lcom/google/android/ytremote/backend/model/a;-><init>(Lcom/google/android/ytremote/backend/model/b;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/ytremote/backend/model/Method;)Lcom/google/android/ytremote/backend/model/b;
    .locals 0

    iput-object p1, p0, Lcom/google/android/ytremote/backend/model/b;->b:Lcom/google/android/ytremote/backend/model/Method;

    return-object p0
.end method

.method public final a(Lcom/google/android/ytremote/backend/model/Params;)Lcom/google/android/ytremote/backend/model/b;
    .locals 0

    iput-object p1, p0, Lcom/google/android/ytremote/backend/model/b;->c:Lcom/google/android/ytremote/backend/model/Params;

    return-object p0
.end method

.method public final a(Lcom/google/android/ytremote/model/LoungeToken;)Lcom/google/android/ytremote/backend/model/b;
    .locals 0

    iput-object p1, p0, Lcom/google/android/ytremote/backend/model/b;->d:Lcom/google/android/ytremote/model/LoungeToken;

    return-object p0
.end method

.method public final a(Z)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/ytremote/backend/model/b;->e:Z

    return-void
.end method
