.class final Lcom/google/android/gms/plus/internal/u;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/d;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;


# direct methods
.method constructor <init>(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/u;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/a;Lcom/google/android/gms/plus/data/a/b;)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/common/a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/u;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-static {v0, p2}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->a(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;Lcom/google/android/gms/plus/data/a/b;)Lcom/google/android/gms/plus/data/a/b;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/u;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    iget-object v0, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->i:Lcom/google/android/gms/plus/a;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/u;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-static {v1}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->c(Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;)Lcom/google/android/gms/plus/c;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/u;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    iget-object v2, v2, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/a;->a(Lcom/google/android/gms/plus/c;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/u;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    iget-boolean v0, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->m:Z

    if-eqz v0, :cond_1

    const-string v0, "PlusOneButtonWithPopup"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PlusOneButtonWithPopup"

    const-string v1, "onSignUpStateLoaded: performing pending click"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/u;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    iput-boolean v3, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->a:Z

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/u;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->performClick()Z

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/u;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;

    iput-boolean v3, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonWithPopupContentView;->m:Z

    :cond_1
    return-void
.end method
