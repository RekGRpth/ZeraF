.class final Lcom/google/android/gms/plus/internal/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/plus/e;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

.field private final b:Lcom/google/android/gms/plus/e;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;Lcom/google/android/gms/plus/e;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/plus/internal/p;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/google/android/gms/plus/internal/p;->b:Lcom/google/android/gms/plus/e;

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Intent;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/p;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->getContext()Landroid/content/Context;

    move-result-object v0

    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    check-cast v0, Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/p;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    iget v1, v1, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->b:I

    invoke-virtual {v0, p1, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/p;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    iget-object v0, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->e:Landroid/widget/CompoundButton;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/p;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    iget-object v0, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->f:Lcom/google/android/gms/plus/internal/ResizingTextView;

    if-ne p1, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/p;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    iget-object v0, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->j:Lcom/google/android/gms/plus/data/a/a;

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/internal/p;->b:Lcom/google/android/gms/plus/e;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/p;->b:Lcom/google/android/gms/plus/e;

    invoke-interface {v1, v0}, Lcom/google/android/gms/plus/e;->a(Landroid/content/Intent;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/p;->a:Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;

    iget-object v0, v0, Lcom/google/android/gms/plus/internal/PlusOneButtonContentView;->j:Lcom/google/android/gms/plus/data/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/data/a/a;->e()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/p;->a(Landroid/content/Intent;)V

    goto :goto_1
.end method
