.class public final Lcom/google/protobuf/h;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static volatile a:Z

.field private static final c:Lcom/google/protobuf/h;


# instance fields
.field private final b:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    sput-boolean v1, Lcom/google/protobuf/h;->a:Z

    new-instance v0, Lcom/google/protobuf/h;

    invoke-direct {v0, v1}, Lcom/google/protobuf/h;-><init>(B)V

    sput-object v0, Lcom/google/protobuf/h;->c:Lcom/google/protobuf/h;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/protobuf/h;->b:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(B)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/protobuf/h;->b:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/protobuf/ad;I)Lcom/google/protobuf/s;
    .locals 2

    iget-object v0, p0, Lcom/google/protobuf/h;->b:Ljava/util/Map;

    new-instance v1, Lcom/google/protobuf/i;

    invoke-direct {v1, p1, p2}, Lcom/google/protobuf/i;-><init>(Ljava/lang/Object;I)V

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/s;

    return-object v0
.end method
