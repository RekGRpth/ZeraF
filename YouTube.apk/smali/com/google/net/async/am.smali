.class final Lcom/google/net/async/am;
.super Ljava/io/OutputStream;
.source "SourceFile"


# instance fields
.field private a:Ljava/io/OutputStream;

.field private b:Lorg/apache/http/util/ByteArrayBuffer;


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/net/async/am;->a:Ljava/io/OutputStream;

    new-instance v0, Lorg/apache/http/util/ByteArrayBuffer;

    const/16 v1, 0x400

    invoke-direct {v0, v1}, Lorg/apache/http/util/ByteArrayBuffer;-><init>(I)V

    iput-object v0, p0, Lcom/google/net/async/am;->b:Lorg/apache/http/util/ByteArrayBuffer;

    return-void
.end method


# virtual methods
.method public final a(Ljava/io/OutputStream;)V
    .locals 3

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "newOut cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/net/async/am;->a:Ljava/io/OutputStream;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "setUnderlyingOutputStream() cannot be called more than once"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/net/async/am;->b:Lorg/apache/http/util/ByteArrayBuffer;

    invoke-virtual {v0}, Lorg/apache/http/util/ByteArrayBuffer;->buffer()[B

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/net/async/am;->b:Lorg/apache/http/util/ByteArrayBuffer;

    invoke-virtual {v2}, Lorg/apache/http/util/ByteArrayBuffer;->length()I

    move-result v2

    invoke-virtual {p1, v0, v1, v2}, Ljava/io/OutputStream;->write([BII)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/net/async/am;->b:Lorg/apache/http/util/ByteArrayBuffer;

    iput-object p1, p0, Lcom/google/net/async/am;->a:Ljava/io/OutputStream;

    return-void
.end method

.method public final write(I)V
    .locals 2

    iget-object v0, p0, Lcom/google/net/async/am;->a:Ljava/io/OutputStream;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/net/async/am;->b:Lorg/apache/http/util/ByteArrayBuffer;

    int-to-byte v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/http/util/ByteArrayBuffer;->append(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/net/async/am;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    goto :goto_0
.end method

.method public final write([BII)V
    .locals 1

    iget-object v0, p0, Lcom/google/net/async/am;->a:Ljava/io/OutputStream;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/net/async/am;->b:Lorg/apache/http/util/ByteArrayBuffer;

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/http/util/ByteArrayBuffer;->append([BII)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/net/async/am;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0
.end method
