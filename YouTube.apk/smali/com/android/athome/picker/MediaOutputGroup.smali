.class public final Lcom/android/athome/picker/MediaOutputGroup;
.super Lcom/android/athome/picker/MediaOutput;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private mItems:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/android/athome/picker/o;

    invoke-direct {v0}, Lcom/android/athome/picker/o;-><init>()V

    sput-object v0, Lcom/android/athome/picker/MediaOutputGroup;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    new-instance v0, Lcom/android/athome/picker/p;

    new-instance v1, Lcom/android/athome/picker/MediaOutput;

    invoke-direct {v1, p1}, Lcom/android/athome/picker/MediaOutput;-><init>(Landroid/os/Parcel;)V

    invoke-direct {v0, v1}, Lcom/android/athome/picker/p;-><init>(Lcom/android/athome/picker/MediaOutput;)V

    invoke-direct {p0, v0}, Lcom/android/athome/picker/MediaOutputGroup;-><init>(Lcom/android/athome/picker/p;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    new-instance v3, Lcom/android/athome/picker/MediaOutput;

    invoke-direct {v3, p1}, Lcom/android/athome/picker/MediaOutput;-><init>(Landroid/os/Parcel;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public constructor <init>(Lcom/android/athome/picker/p;)V
    .locals 8

    invoke-static {p1}, Lcom/android/athome/picker/p;->a(Lcom/android/athome/picker/p;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/android/athome/picker/p;->b(Lcom/android/athome/picker/p;)I

    move-result v2

    invoke-static {p1}, Lcom/android/athome/picker/p;->c(Lcom/android/athome/picker/p;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Lcom/android/athome/picker/p;->d(Lcom/android/athome/picker/p;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1}, Lcom/android/athome/picker/p;->e(Lcom/android/athome/picker/p;)F

    move-result v5

    invoke-static {p1}, Lcom/android/athome/picker/p;->f(Lcom/android/athome/picker/p;)Z

    move-result v6

    invoke-static {p1}, Lcom/android/athome/picker/p;->g(Lcom/android/athome/picker/p;)Ljava/util/List;

    move-result-object v7

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/android/athome/picker/MediaOutputGroup;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;FZLjava/util/List;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;FZLjava/util/List;)V
    .locals 1

    invoke-direct/range {p0 .. p6}, Lcom/android/athome/picker/MediaOutput;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;FZ)V

    if-nez p7, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    :goto_0
    return-void

    :cond_0
    iput-object p7, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    goto :goto_0
.end method

.method private setGroupMuted(Z)V
    .locals 2

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v0, p1}, Lcom/android/athome/picker/MediaOutput;->setIsMuted(Z)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private setGroupVolume(F)V
    .locals 2

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v0, p1}, Lcom/android/athome/picker/MediaOutput;->setVolume(F)V

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public final add(Lcom/android/athome/picker/MediaOutput;)Z
    .locals 3

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "null not allowed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    instance-of v0, p1, Lcom/android/athome/picker/MediaOutputGroup;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can\'t add a group to an existing group."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p1}, Lcom/android/athome/picker/MediaOutput;->isGroupable()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/android/athome/picker/MediaOutput;->getType()I

    move-result v0

    invoke-virtual {p0}, Lcom/android/athome/picker/MediaOutputGroup;->getType()I

    move-result v1

    if-eq v0, v1, :cond_3

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t add "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/android/athome/picker/MediaOutput;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "to a group of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/athome/picker/MediaOutputGroup;->getType()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final contains(Lcom/android/athome/picker/MediaOutput;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getGroupMaxVolume()F
    .locals 4

    const/high16 v0, 0x7fc00000

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v0}, Lcom/android/athome/picker/MediaOutput;->getIsMuted()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/android/athome/picker/MediaOutput;->getVolume()F

    move-result v0

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/android/athome/picker/MediaOutput;->getVolume()F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    move v1, v0

    goto :goto_0

    :cond_2
    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v1, 0x0

    :cond_3
    return v1
.end method

.method public final getIsMuted()Z
    .locals 1

    invoke-virtual {p0}, Lcom/android/athome/picker/MediaOutputGroup;->isGroupMuted()Z

    move-result v0

    return v0
.end method

.method public final getMediaOutputs()Ljava/util/List;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public final getVolume()F
    .locals 1

    invoke-virtual {p0}, Lcom/android/athome/picker/MediaOutputGroup;->getGroupMaxVolume()F

    move-result v0

    return v0
.end method

.method public final isGroupMuted()Z
    .locals 3

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v0}, Lcom/android/athome/picker/MediaOutput;->getIsMuted()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final remove(Lcom/android/athome/picker/MediaOutput;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final setIsMuted(Z)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/athome/picker/MediaOutput;->setIsMuted(Z)V

    invoke-direct {p0, p1}, Lcom/android/athome/picker/MediaOutputGroup;->setGroupMuted(Z)V

    return-void
.end method

.method public final setVolume(F)V
    .locals 0

    invoke-super {p0, p1}, Lcom/android/athome/picker/MediaOutput;->setVolume(F)V

    invoke-direct {p0, p1}, Lcom/android/athome/picker/MediaOutputGroup;->setGroupVolume(F)V

    return-void
.end method

.method public final size()I
    .locals 1

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v0}, Lcom/android/athome/picker/MediaOutput;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ","

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/android/athome/picker/MediaOutput;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "groupSize:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "mItems:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/android/athome/picker/MediaOutput;->writeToParcel(Landroid/os/Parcel;I)V

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/android/athome/picker/MediaOutputGroup;->mItems:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/android/athome/picker/MediaOutput;

    invoke-virtual {v0, p1, p2}, Lcom/android/athome/picker/MediaOutput;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    :cond_0
    return-void
.end method
