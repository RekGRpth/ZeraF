.class public final Lcom/android/athome/picker/media/k;
.super Lcom/android/athome/picker/media/a;
.source "SourceFile"


# static fields
.field static final a:Lcom/android/athome/picker/media/l;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    new-instance v0, Lcom/android/athome/picker/media/n;

    invoke-direct {v0}, Lcom/android/athome/picker/media/n;-><init>()V

    sput-object v0, Lcom/android/athome/picker/media/k;->a:Lcom/android/athome/picker/media/l;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/android/athome/picker/media/m;

    invoke-direct {v0}, Lcom/android/athome/picker/media/m;-><init>()V

    sput-object v0, Lcom/android/athome/picker/media/k;->a:Lcom/android/athome/picker/media/l;

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;)I
    .locals 1

    sget-object v0, Lcom/android/athome/picker/media/k;->a:Lcom/android/athome/picker/media/l;

    invoke-interface {v0, p0}, Lcom/android/athome/picker/media/l;->a(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/android/athome/picker/media/k;->a:Lcom/android/athome/picker/media/l;

    invoke-interface {v0, p0}, Lcom/android/athome/picker/media/l;->a(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/android/athome/picker/media/k;->a:Lcom/android/athome/picker/media/l;

    invoke-interface {v0, p0, p1}, Lcom/android/athome/picker/media/l;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/CharSequence;Z)Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/android/athome/picker/media/k;->a:Lcom/android/athome/picker/media/l;

    invoke-interface {v0, p0, p1, p2}, Lcom/android/athome/picker/media/l;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/Object;ILcom/android/athome/picker/media/b;)V
    .locals 1

    sget-object v0, Lcom/android/athome/picker/media/k;->a:Lcom/android/athome/picker/media/l;

    invoke-interface {v0, p0, p1, p2}, Lcom/android/athome/picker/media/l;->a(Ljava/lang/Object;ILcom/android/athome/picker/media/b;)V

    return-void
.end method

.method public static a(Ljava/lang/Object;ILjava/lang/Object;)V
    .locals 1

    sget-object v0, Lcom/android/athome/picker/media/k;->a:Lcom/android/athome/picker/media/l;

    invoke-interface {v0, p0, p1, p2}, Lcom/android/athome/picker/media/l;->a(Ljava/lang/Object;ILjava/lang/Object;)V

    return-void
.end method

.method public static a(Ljava/lang/Object;Lvedroid/support/v4/app/l;)V
    .locals 1

    sget-object v0, Lcom/android/athome/picker/media/k;->a:Lcom/android/athome/picker/media/l;

    invoke-interface {v0, p0, p1}, Lcom/android/athome/picker/media/l;->a(Ljava/lang/Object;Lvedroid/support/v4/app/l;)V

    return-void
.end method

.method public static a(Ljava/lang/Object;Lcom/android/athome/picker/media/b;)V
    .locals 1

    sget-object v0, Lcom/android/athome/picker/media/k;->a:Lcom/android/athome/picker/media/l;

    invoke-interface {v0, p0, p1}, Lcom/android/athome/picker/media/l;->a(Ljava/lang/Object;Lcom/android/athome/picker/media/b;)V

    return-void
.end method

.method public static a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    sget-object v0, Lcom/android/athome/picker/media/k;->a:Lcom/android/athome/picker/media/l;

    invoke-interface {v0, p0, p1}, Lcom/android/athome/picker/media/l;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public static b(Ljava/lang/Object;)I
    .locals 1

    sget-object v0, Lcom/android/athome/picker/media/k;->a:Lcom/android/athome/picker/media/l;

    invoke-interface {v0, p0}, Lcom/android/athome/picker/media/l;->b(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public static b(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/android/athome/picker/media/k;->a:Lcom/android/athome/picker/media/l;

    invoke-interface {v0, p0, p1}, Lcom/android/athome/picker/media/l;->b(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1

    sget-object v0, Lcom/android/athome/picker/media/k;->a:Lcom/android/athome/picker/media/l;

    invoke-interface {v0, p0, p1}, Lcom/android/athome/picker/media/l;->b(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public static c(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/android/athome/picker/media/k;->a:Lcom/android/athome/picker/media/l;

    invoke-interface {v0, p0, p1}, Lcom/android/athome/picker/media/l;->c(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    sget-object v0, Lcom/android/athome/picker/media/k;->a:Lcom/android/athome/picker/media/l;

    invoke-interface {v0, p0, p1}, Lcom/android/athome/picker/media/l;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/lang/Object;)Z
    .locals 1

    sget-object v0, Lcom/android/athome/picker/media/k;->a:Lcom/android/athome/picker/media/l;

    invoke-interface {v0, p0}, Lcom/android/athome/picker/media/l;->q(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static d(Ljava/lang/Object;)Z
    .locals 1

    sget-object v0, Lcom/android/athome/picker/media/k;->a:Lcom/android/athome/picker/media/l;

    invoke-interface {v0, p0}, Lcom/android/athome/picker/media/l;->r(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
