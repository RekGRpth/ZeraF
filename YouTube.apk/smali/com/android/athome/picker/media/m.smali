.class final Lcom/android/athome/picker/media/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/athome/picker/media/l;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/athome/picker/media/v;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/v;->e()I

    move-result v0

    return v0
.end method

.method public final a(Landroid/content/Context;)Ljava/lang/Object;
    .locals 1

    invoke-static {p1}, Lcom/android/athome/picker/media/v;->a(Landroid/content/Context;)Lcom/android/athome/picker/media/v;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/android/athome/picker/media/v;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/v;->c(I)Lcom/android/athome/picker/media/z;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/CharSequence;Z)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/android/athome/picker/media/v;

    invoke-virtual {p1, p2, p3}, Lcom/android/athome/picker/media/v;->a(Ljava/lang/CharSequence;Z)Lcom/android/athome/picker/media/x;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/Object;Ljava/util/List;)Ljava/util/List;
    .locals 4

    check-cast p1, Lcom/android/athome/picker/media/x;

    if-nez p2, :cond_1

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    iget-object v0, p1, Lcom/android/athome/picker/media/x;->d:Lcom/android/athome/picker/media/v;

    invoke-virtual {v0}, Lcom/android/athome/picker/media/v;->e()I

    move-result v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_2

    iget-object v2, p1, Lcom/android/athome/picker/media/x;->d:Lcom/android/athome/picker/media/v;

    invoke-virtual {v2, v0}, Lcom/android/athome/picker/media/v;->c(I)Lcom/android/athome/picker/media/z;

    move-result-object v2

    iget-object v3, v2, Lcom/android/athome/picker/media/z;->f:Lcom/android/athome/picker/media/x;

    if-ne v3, p1, :cond_0

    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-interface {p2}, Ljava/util/List;->clear()V

    goto :goto_0

    :cond_2
    return-object p2
.end method

.method public final a(Ljava/lang/Object;ILcom/android/athome/picker/media/b;)V
    .locals 0

    check-cast p1, Lcom/android/athome/picker/media/v;

    invoke-virtual {p1, p2, p3}, Lcom/android/athome/picker/media/v;->a(ILcom/android/athome/picker/media/b;)V

    return-void
.end method

.method public final a(Ljava/lang/Object;ILjava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/android/athome/picker/media/v;

    check-cast p3, Lcom/android/athome/picker/media/z;

    invoke-virtual {p1, p2, p3}, Lcom/android/athome/picker/media/v;->a(ILcom/android/athome/picker/media/z;)V

    return-void
.end method

.method public final a(Ljava/lang/Object;Lvedroid/support/v4/app/l;)V
    .locals 0

    invoke-static {}, Lcom/android/athome/picker/media/v;->c()V

    return-void
.end method

.method public final a(Ljava/lang/Object;Lcom/android/athome/picker/media/b;)V
    .locals 0

    check-cast p1, Lcom/android/athome/picker/media/v;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/v;->a(Lcom/android/athome/picker/media/b;)V

    return-void
.end method

.method public final a(Ljava/lang/Object;Lcom/android/athome/picker/media/d;)V
    .locals 1

    check-cast p1, Lcom/android/athome/picker/media/aa;

    new-instance v0, Lcom/android/athome/picker/media/ab;

    invoke-direct {v0, p2, p1}, Lcom/android/athome/picker/media/ab;-><init>(Lcom/android/athome/picker/media/d;Lcom/android/athome/picker/media/z;)V

    iput-object v0, p1, Lcom/android/athome/picker/media/aa;->m:Lcom/android/athome/picker/media/ab;

    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .locals 0

    check-cast p1, Lcom/android/athome/picker/media/aa;

    iput-object p2, p1, Lcom/android/athome/picker/media/aa;->c:Ljava/lang/CharSequence;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/aa;->d()V

    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/android/athome/picker/media/v;

    check-cast p2, Lcom/android/athome/picker/media/aa;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/v;->a(Lcom/android/athome/picker/media/aa;)V

    return-void
.end method

.method public final b(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/athome/picker/media/v;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/v;->d()I

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/android/athome/picker/media/v;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/v;->b()Lcom/android/athome/picker/media/z;

    move-result-object v0

    return-object v0
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/CharSequence;)V
    .locals 0

    check-cast p1, Lcom/android/athome/picker/media/aa;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/aa;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final b(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/android/athome/picker/media/v;

    check-cast p2, Lcom/android/athome/picker/media/aa;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/v;->b(Lcom/android/athome/picker/media/aa;)V

    return-void
.end method

.method public final c(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1

    check-cast p1, Lcom/android/athome/picker/media/z;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/z;->a()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/android/athome/picker/media/v;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/v;->b(I)Lcom/android/athome/picker/media/x;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/android/athome/picker/media/v;

    check-cast p2, Lcom/android/athome/picker/media/x;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/v;->a(Lcom/android/athome/picker/media/x;)Lcom/android/athome/picker/media/aa;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1

    check-cast p1, Lcom/android/athome/picker/media/z;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/z;->b()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/Object;I)V
    .locals 0

    check-cast p1, Lcom/android/athome/picker/media/y;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/y;->a(I)V

    return-void
.end method

.method public final d(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/android/athome/picker/media/y;

    check-cast p2, Lcom/android/athome/picker/media/z;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/y;->a(Lcom/android/athome/picker/media/z;)V

    return-void
.end method

.method public final e(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/athome/picker/media/z;

    iget v0, p1, Lcom/android/athome/picker/media/z;->d:I

    return v0
.end method

.method public final e(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/android/athome/picker/media/y;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/y;->b(I)Lcom/android/athome/picker/media/z;

    move-result-object v0

    return-object v0
.end method

.method public final e(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/android/athome/picker/media/y;

    check-cast p2, Lcom/android/athome/picker/media/z;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/y;->b(Lcom/android/athome/picker/media/z;)V

    return-void
.end method

.method public final f(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/android/athome/picker/media/z;

    iget-object v0, p1, Lcom/android/athome/picker/media/z;->e:Lcom/android/athome/picker/media/y;

    return-object v0
.end method

.method public final f(Ljava/lang/Object;I)V
    .locals 0

    check-cast p1, Lcom/android/athome/picker/media/z;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/z;->c(I)V

    return-void
.end method

.method public final f(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/android/athome/picker/media/z;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/z;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final g(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/android/athome/picker/media/z;

    iget-object v0, p1, Lcom/android/athome/picker/media/z;->f:Lcom/android/athome/picker/media/x;

    return-object v0
.end method

.method public final g(Ljava/lang/Object;I)V
    .locals 0

    check-cast p1, Lcom/android/athome/picker/media/z;

    invoke-virtual {p1, p2}, Lcom/android/athome/picker/media/z;->d(I)V

    return-void
.end method

.method public final h(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/athome/picker/media/y;

    iget-object v0, p1, Lcom/android/athome/picker/media/y;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final h(Ljava/lang/Object;I)V
    .locals 2

    check-cast p1, Lcom/android/athome/picker/media/aa;

    const/4 v0, 0x0

    invoke-virtual {p1}, Lcom/android/athome/picker/media/aa;->g()I

    move-result v1

    invoke-static {p2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v1, p1, Lcom/android/athome/picker/media/aa;->j:I

    if-eq v1, v0, :cond_0

    iput v0, p1, Lcom/android/athome/picker/media/aa;->j:I

    iget-object v0, p1, Lcom/android/athome/picker/media/aa;->a:Lcom/android/athome/picker/media/v;

    invoke-virtual {v0, p1}, Lcom/android/athome/picker/media/v;->c(Lcom/android/athome/picker/media/z;)V

    iget-object v0, p1, Lcom/android/athome/picker/media/aa;->e:Lcom/android/athome/picker/media/y;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/android/athome/picker/media/aa;->e:Lcom/android/athome/picker/media/y;

    invoke-virtual {v0}, Lcom/android/athome/picker/media/y;->c()V

    :cond_0
    return-void
.end method

.method public final i(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1

    check-cast p1, Lcom/android/athome/picker/media/x;

    iget-object v0, p1, Lcom/android/athome/picker/media/x;->a:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final i(Ljava/lang/Object;I)V
    .locals 1

    check-cast p1, Lcom/android/athome/picker/media/aa;

    iget v0, p1, Lcom/android/athome/picker/media/aa;->i:I

    if-eq v0, p2, :cond_0

    iput p2, p1, Lcom/android/athome/picker/media/aa;->i:I

    :cond_0
    return-void
.end method

.method public final j(Ljava/lang/Object;I)V
    .locals 1

    check-cast p1, Lcom/android/athome/picker/media/aa;

    iget v0, p1, Lcom/android/athome/picker/media/aa;->h:I

    if-eq v0, p2, :cond_0

    iput p2, p1, Lcom/android/athome/picker/media/aa;->h:I

    :cond_0
    return-void
.end method

.method public final j(Ljava/lang/Object;)Z
    .locals 1

    check-cast p1, Lcom/android/athome/picker/media/x;

    iget-boolean v0, p1, Lcom/android/athome/picker/media/x;->c:Z

    return v0
.end method

.method public final k(Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;
    .locals 1

    check-cast p1, Lcom/android/athome/picker/media/z;

    iget-object v0, p1, Lcom/android/athome/picker/media/z;->g:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final k(Ljava/lang/Object;I)V
    .locals 1

    check-cast p1, Lcom/android/athome/picker/media/aa;

    iget v0, p1, Lcom/android/athome/picker/media/aa;->l:I

    if-eq v0, p2, :cond_0

    iput p2, p1, Lcom/android/athome/picker/media/aa;->l:I

    :cond_0
    return-void
.end method

.method public final l(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, Lcom/android/athome/picker/media/z;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/z;->e()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final m(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/athome/picker/media/z;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/z;->f()I

    move-result v0

    return v0
.end method

.method public final n(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/athome/picker/media/z;

    invoke-virtual {p1}, Lcom/android/athome/picker/media/z;->g()I

    move-result v0

    return v0
.end method

.method public final o(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/athome/picker/media/z;

    iget v0, p1, Lcom/android/athome/picker/media/z;->l:I

    return v0
.end method

.method public final p(Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lcom/android/athome/picker/media/z;

    iget v0, p1, Lcom/android/athome/picker/media/z;->h:I

    return v0
.end method

.method public final q(Ljava/lang/Object;)Z
    .locals 1

    instance-of v0, p1, Lcom/android/athome/picker/media/z;

    return v0
.end method

.method public final r(Ljava/lang/Object;)Z
    .locals 1

    instance-of v0, p1, Lcom/android/athome/picker/media/x;

    return v0
.end method
