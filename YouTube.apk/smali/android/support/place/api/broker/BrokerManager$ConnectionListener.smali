.class public abstract Landroid/support/place/api/broker/BrokerManager$ConnectionListener;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBrokerConnected(Landroid/support/place/connector/Broker;)V
    .locals 0

    return-void
.end method

.method public onBrokerDisconnected()V
    .locals 0

    return-void
.end method

.method public onConnectedToRegistry(Ljava/util/List;)V
    .locals 0

    return-void
.end method

.method public onConnectorAdded(Landroid/support/place/connector/ConnectorInfo;)V
    .locals 0

    return-void
.end method

.method public onConnectorRemoved(Landroid/support/place/connector/ConnectorInfo;)V
    .locals 0

    return-void
.end method

.method public onFailToConnect()V
    .locals 0

    return-void
.end method

.method public onPlaceAdded(Landroid/support/place/connector/PlaceInfo;)V
    .locals 0

    return-void
.end method

.method public onPlaceConnected(Landroid/support/place/connector/PlaceInfo;)V
    .locals 0

    return-void
.end method

.method public onPlaceDisconnected()V
    .locals 0

    return-void
.end method

.method public onPlaceNameChanged(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public onPlaceRemoved(Landroid/support/place/connector/PlaceInfo;)V
    .locals 0

    return-void
.end method

.method public onPlaceUpdated(Landroid/support/place/connector/PlaceInfo;)V
    .locals 0

    return-void
.end method
