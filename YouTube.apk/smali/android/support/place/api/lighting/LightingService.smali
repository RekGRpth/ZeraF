.class public Landroid/support/place/api/lighting/LightingService;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private _broker:Landroid/support/place/connector/Broker;

.field private _endpoint:Landroid/support/place/rpc/EndpointInfo;

.field private _presenter:Landroid/support/place/api/lighting/LightingService$Presenter;


# direct methods
.method public constructor <init>(Landroid/support/place/connector/Broker;Landroid/support/place/rpc/EndpointInfo;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/place/api/lighting/LightingService;->_broker:Landroid/support/place/connector/Broker;

    iput-object p2, p0, Landroid/support/place/api/lighting/LightingService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    return-void
.end method


# virtual methods
.method public getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;
    .locals 1

    iget-object v0, p0, Landroid/support/place/api/lighting/LightingService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    return-object v0
.end method

.method public listLights(Landroid/support/place/api/lighting/LightingService$OnListLights;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v0, p0, Landroid/support/place/api/lighting/LightingService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/api/lighting/LightingService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "listLights"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    new-instance v4, Landroid/support/place/api/lighting/LightingService$_ResultDispatcher;

    const/4 v5, 0x0

    invoke-direct {v4, p0, v5, p1}, Landroid/support/place/api/lighting/LightingService$_ResultDispatcher;-><init>(Landroid/support/place/api/lighting/LightingService;ILjava/lang/Object;)V

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public setLightLevel(Ljava/lang/String;ILandroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "id"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "level"

    invoke-virtual {v3, v0, p2}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    iget-object v0, p0, Landroid/support/place/api/lighting/LightingService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/api/lighting/LightingService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "setLightLevel"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public setLightLocation(Ljava/lang/String;Ljava/lang/String;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "id"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "location"

    invoke-virtual {v3, v0, p2}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/place/api/lighting/LightingService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/api/lighting/LightingService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "setLightLocation"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public setLightName(Ljava/lang/String;Ljava/lang/String;Landroid/support/place/rpc/RpcErrorHandler;)V
    .locals 6

    new-instance v3, Landroid/support/place/rpc/RpcData;

    invoke-direct {v3}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v0, "id"

    invoke-virtual {v3, v0, p1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "name"

    invoke-virtual {v3, v0, p2}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Landroid/support/place/api/lighting/LightingService;->_broker:Landroid/support/place/connector/Broker;

    iget-object v1, p0, Landroid/support/place/api/lighting/LightingService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    const-string v2, "setLightName"

    invoke-virtual {v3}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/support/place/connector/Broker;->sendRpc(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;[BLandroid/support/place/rpc/RpcResultHandler;Landroid/support/place/rpc/RpcErrorHandler;)V

    return-void
.end method

.method public startListening(Landroid/support/place/api/lighting/LightingService$Listener;)V
    .locals 2

    invoke-virtual {p0}, Landroid/support/place/api/lighting/LightingService;->stopListening()V

    new-instance v0, Landroid/support/place/api/lighting/LightingService$Presenter;

    iget-object v1, p0, Landroid/support/place/api/lighting/LightingService;->_broker:Landroid/support/place/connector/Broker;

    invoke-direct {v0, p0, v1, p1}, Landroid/support/place/api/lighting/LightingService$Presenter;-><init>(Landroid/support/place/api/lighting/LightingService;Landroid/support/place/connector/Broker;Landroid/support/place/api/lighting/LightingService$Listener;)V

    iput-object v0, p0, Landroid/support/place/api/lighting/LightingService;->_presenter:Landroid/support/place/api/lighting/LightingService$Presenter;

    iget-object v0, p0, Landroid/support/place/api/lighting/LightingService;->_presenter:Landroid/support/place/api/lighting/LightingService$Presenter;

    iget-object v1, p0, Landroid/support/place/api/lighting/LightingService;->_endpoint:Landroid/support/place/rpc/EndpointInfo;

    invoke-virtual {v0, v1}, Landroid/support/place/api/lighting/LightingService$Presenter;->startListening(Landroid/support/place/rpc/EndpointInfo;)V

    return-void
.end method

.method public stopListening()V
    .locals 1

    iget-object v0, p0, Landroid/support/place/api/lighting/LightingService;->_presenter:Landroid/support/place/api/lighting/LightingService$Presenter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/api/lighting/LightingService;->_presenter:Landroid/support/place/api/lighting/LightingService$Presenter;

    invoke-virtual {v0}, Landroid/support/place/api/lighting/LightingService$Presenter;->stopListening()V

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/place/api/lighting/LightingService;->_presenter:Landroid/support/place/api/lighting/LightingService$Presenter;

    :cond_0
    return-void
.end method
