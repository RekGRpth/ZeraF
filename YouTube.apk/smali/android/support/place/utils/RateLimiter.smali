.class public Landroid/support/place/utils/RateLimiter;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mLastRunTimestampMs:Ljava/lang/Long;

.field private mPeriodMs:I

.field private mTask:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Ljava/lang/Runnable;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroid/support/place/utils/RateLimiter;->mHandler:Landroid/os/Handler;

    iput-object p2, p0, Landroid/support/place/utils/RateLimiter;->mTask:Ljava/lang/Runnable;

    iput p3, p0, Landroid/support/place/utils/RateLimiter;->mPeriodMs:I

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/place/utils/RateLimiter;->mLastRunTimestampMs:Ljava/lang/Long;

    return-void
.end method


# virtual methods
.method public declared-synchronized execute()V
    .locals 6

    monitor-enter p0

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Landroid/support/place/utils/RateLimiter;->mLastRunTimestampMs:Ljava/lang/Long;

    if-eqz v2, :cond_0

    iget-object v2, p0, Landroid/support/place/utils/RateLimiter;->mLastRunTimestampMs:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget v4, p0, Landroid/support/place/utils/RateLimiter;->mPeriodMs:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/utils/RateLimiter;->mLastRunTimestampMs:Ljava/lang/Long;

    iget-object v0, p0, Landroid/support/place/utils/RateLimiter;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/place/utils/RateLimiter;->mTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :cond_2
    :try_start_1
    iget-object v2, p0, Landroid/support/place/utils/RateLimiter;->mLastRunTimestampMs:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    iget-object v0, p0, Landroid/support/place/utils/RateLimiter;->mLastRunTimestampMs:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget v2, p0, Landroid/support/place/utils/RateLimiter;->mPeriodMs:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/utils/RateLimiter;->mLastRunTimestampMs:Ljava/lang/Long;

    iget-object v0, p0, Landroid/support/place/utils/RateLimiter;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/place/utils/RateLimiter;->mTask:Ljava/lang/Runnable;

    iget-object v2, p0, Landroid/support/place/utils/RateLimiter;->mLastRunTimestampMs:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
