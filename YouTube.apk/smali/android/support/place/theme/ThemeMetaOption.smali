.class public Landroid/support/place/theme/ThemeMetaOption;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Landroid/support/place/rpc/Flattenable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final KEY_MODE:Ljava/lang/String; = "mode"

.field public static final MODE_BOUNCING:I = 0x2

.field public static final MODE_NONE:I = 0x0

.field public static final MODE_TRANSIENT:I = 0x1

.field public static final RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;


# instance fields
.field private mMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/support/place/theme/ThemeMetaOption$1;

    invoke-direct {v0}, Landroid/support/place/theme/ThemeMetaOption$1;-><init>()V

    sput-object v0, Landroid/support/place/theme/ThemeMetaOption;->CREATOR:Landroid/os/Parcelable$Creator;

    new-instance v0, Landroid/support/place/theme/ThemeMetaOption$2;

    invoke-direct {v0}, Landroid/support/place/theme/ThemeMetaOption$2;-><init>()V

    sput-object v0, Landroid/support/place/theme/ThemeMetaOption;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v0, v0, [B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    invoke-virtual {v1, v0}, Landroid/support/place/rpc/RpcData;->deserialize([B)V

    invoke-virtual {p0, v1}, Landroid/support/place/theme/ThemeMetaOption;->readFromRpcData(Landroid/support/place/rpc/RpcData;)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/support/place/theme/ThemeMetaOption$1;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/place/theme/ThemeMetaOption;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Landroid/support/place/rpc/RpcData;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1}, Landroid/support/place/theme/ThemeMetaOption;->readFromRpcData(Landroid/support/place/rpc/RpcData;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Landroid/support/place/theme/ThemeMetaOption;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Landroid/support/place/theme/ThemeMetaOption;

    iget v1, p0, Landroid/support/place/theme/ThemeMetaOption;->mMode:I

    iget v2, p1, Landroid/support/place/theme/ThemeMetaOption;->mMode:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getMode()I
    .locals 1

    iget v0, p0, Landroid/support/place/theme/ThemeMetaOption;->mMode:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Landroid/support/place/theme/ThemeMetaOption;->mMode:I

    add-int/lit16 v0, v0, 0x193

    return v0
.end method

.method public readFromRpcData(Landroid/support/place/rpc/RpcData;)V
    .locals 1

    const-string v0, "mode"

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->getInteger(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Landroid/support/place/theme/ThemeMetaOption;->mMode:I

    return-void
.end method

.method public setMode(I)V
    .locals 0

    iput p1, p0, Landroid/support/place/theme/ThemeMetaOption;->mMode:I

    return-void
.end method

.method public toDebugString()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p0}, Landroid/support/place/theme/ThemeMetaOption;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ThemeMetaOption("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Landroid/support/place/theme/ThemeMetaOption;->mMode:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    invoke-virtual {p0, v0}, Landroid/support/place/theme/ThemeMetaOption;->writeToRpcData(Landroid/support/place/rpc/RpcData;)V

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v0

    array-length v1, v0

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    return-void
.end method

.method public writeToRpcData(Landroid/support/place/rpc/RpcData;)V
    .locals 2

    const-string v0, "mode"

    iget v1, p0, Landroid/support/place/theme/ThemeMetaOption;->mMode:I

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putInteger(Ljava/lang/String;I)V

    return-void
.end method
