.class final Landroid/support/place/theme/ThemeService$_ResultDispatcher;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/place/rpc/RpcResultHandler;


# instance fields
.field private callback:Ljava/lang/Object;

.field private methodId:I

.field final synthetic this$0:Landroid/support/place/theme/ThemeService;


# direct methods
.method public constructor <init>(Landroid/support/place/theme/ThemeService;ILjava/lang/Object;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/theme/ThemeService$_ResultDispatcher;->this$0:Landroid/support/place/theme/ThemeService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Landroid/support/place/theme/ThemeService$_ResultDispatcher;->methodId:I

    iput-object p3, p0, Landroid/support/place/theme/ThemeService$_ResultDispatcher;->callback:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final getTheme([B)V
    .locals 3

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    sget-object v2, Landroid/support/place/theme/Theme;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    invoke-virtual {v0, v1, v2}, Landroid/support/place/rpc/RpcData;->getFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;

    move-result-object v0

    check-cast v0, Landroid/support/place/theme/Theme;

    iget-object v1, p0, Landroid/support/place/theme/ThemeService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/place/theme/ThemeService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v1, Landroid/support/place/theme/ThemeService$OnGetTheme;

    invoke-interface {v1, v0}, Landroid/support/place/theme/ThemeService$OnGetTheme;->onGetTheme(Landroid/support/place/theme/Theme;)V

    :cond_0
    return-void
.end method

.method public final getThemeEngines([B)V
    .locals 3

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p1}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v1, "_result"

    sget-object v2, Landroid/support/place/theme/ThemeEngine;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    invoke-virtual {v0, v1, v2}, Landroid/support/place/rpc/RpcData;->getFlattenableList(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Ljava/util/List;

    move-result-object v1

    iget-object v0, p0, Landroid/support/place/theme/ThemeService$_ResultDispatcher;->callback:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/place/theme/ThemeService$_ResultDispatcher;->callback:Ljava/lang/Object;

    check-cast v0, Landroid/support/place/theme/ThemeService$OnGetThemeEngines;

    invoke-interface {v0, v1}, Landroid/support/place/theme/ThemeService$OnGetThemeEngines;->onGetThemeEngines(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public final onResult([B)V
    .locals 1

    iget v0, p0, Landroid/support/place/theme/ThemeService$_ResultDispatcher;->methodId:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual {p0, p1}, Landroid/support/place/theme/ThemeService$_ResultDispatcher;->getTheme([B)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, p1}, Landroid/support/place/theme/ThemeService$_ResultDispatcher;->getThemeEngines([B)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
