.class public Landroid/support/place/theme/ThemeEngine;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Landroid/support/place/rpc/Flattenable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final KEY_CONFIG_COMPONENT:Ljava/lang/String; = "configComp"

.field private static final KEY_DEVICE_COMPONENT:Ljava/lang/String; = "deviceComp"

.field private static final KEY_DISPLAY_NAME:Ljava/lang/String; = "displayName"

.field public static final RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;


# instance fields
.field private mConfigComponent:Ljava/lang/String;

.field private mDeviceComponent:Ljava/lang/String;

.field private mDisplayName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/support/place/theme/ThemeEngine$1;

    invoke-direct {v0}, Landroid/support/place/theme/ThemeEngine$1;-><init>()V

    sput-object v0, Landroid/support/place/theme/ThemeEngine;->CREATOR:Landroid/os/Parcelable$Creator;

    new-instance v0, Landroid/support/place/theme/ThemeEngine$2;

    invoke-direct {v0}, Landroid/support/place/theme/ThemeEngine$2;-><init>()V

    sput-object v0, Landroid/support/place/theme/ThemeEngine;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v0, v0, [B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    invoke-virtual {v1, v0}, Landroid/support/place/rpc/RpcData;->deserialize([B)V

    invoke-virtual {p0, v1}, Landroid/support/place/theme/ThemeEngine;->readFromRpcData(Landroid/support/place/rpc/RpcData;)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/support/place/theme/ThemeEngine$1;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/place/theme/ThemeEngine;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Landroid/support/place/rpc/RpcData;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1}, Landroid/support/place/theme/ThemeEngine;->readFromRpcData(Landroid/support/place/rpc/RpcData;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Landroid/support/place/theme/ThemeEngine;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Landroid/support/place/theme/ThemeEngine;

    iget-object v1, p0, Landroid/support/place/theme/ThemeEngine;->mDisplayName:Ljava/lang/String;

    if-nez v1, :cond_2

    iget-object v1, p1, Landroid/support/place/theme/ThemeEngine;->mDisplayName:Ljava/lang/String;

    if-nez v1, :cond_0

    :cond_2
    iget-object v1, p0, Landroid/support/place/theme/ThemeEngine;->mDisplayName:Ljava/lang/String;

    iget-object v2, p1, Landroid/support/place/theme/ThemeEngine;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/place/theme/ThemeEngine;->mDeviceComponent:Ljava/lang/String;

    if-nez v1, :cond_3

    iget-object v1, p1, Landroid/support/place/theme/ThemeEngine;->mDeviceComponent:Ljava/lang/String;

    if-nez v1, :cond_0

    :cond_3
    iget-object v1, p0, Landroid/support/place/theme/ThemeEngine;->mDeviceComponent:Ljava/lang/String;

    iget-object v2, p1, Landroid/support/place/theme/ThemeEngine;->mDeviceComponent:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/place/theme/ThemeEngine;->mConfigComponent:Ljava/lang/String;

    if-nez v1, :cond_4

    iget-object v1, p1, Landroid/support/place/theme/ThemeEngine;->mConfigComponent:Ljava/lang/String;

    if-nez v1, :cond_0

    :cond_4
    iget-object v1, p0, Landroid/support/place/theme/ThemeEngine;->mConfigComponent:Ljava/lang/String;

    iget-object v2, p1, Landroid/support/place/theme/ThemeEngine;->mConfigComponent:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getConfigurationComponent()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/place/theme/ThemeEngine;->mConfigComponent:Ljava/lang/String;

    return-object v0
.end method

.method public getDeviceComponent()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/place/theme/ThemeEngine;->mDeviceComponent:Ljava/lang/String;

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/place/theme/ThemeEngine;->mDisplayName:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Landroid/support/place/theme/ThemeEngine;->mDisplayName:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Landroid/support/place/theme/ThemeEngine;->mDeviceComponent:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Landroid/support/place/theme/ThemeEngine;->mConfigComponent:Ljava/lang/String;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/place/theme/ThemeEngine;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Landroid/support/place/theme/ThemeEngine;->mDeviceComponent:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v1, p0, Landroid/support/place/theme/ThemeEngine;->mConfigComponent:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public readFromRpcData(Landroid/support/place/rpc/RpcData;)V
    .locals 2

    const/4 v1, 0x0

    :try_start_0
    const-string v0, "displayName"

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/theme/ThemeEngine;->mDisplayName:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    :try_start_1
    const-string v0, "deviceComp"

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/theme/ThemeEngine;->mDeviceComponent:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    :try_start_2
    const-string v0, "configComp"

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/theme/ThemeEngine;->mConfigComponent:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2

    :goto_2
    return-void

    :catch_0
    move-exception v0

    iput-object v1, p0, Landroid/support/place/theme/ThemeEngine;->mDisplayName:Ljava/lang/String;

    goto :goto_0

    :catch_1
    move-exception v0

    iput-object v1, p0, Landroid/support/place/theme/ThemeEngine;->mDeviceComponent:Ljava/lang/String;

    goto :goto_1

    :catch_2
    move-exception v0

    iput-object v1, p0, Landroid/support/place/theme/ThemeEngine;->mConfigComponent:Ljava/lang/String;

    goto :goto_2
.end method

.method public setConfigurationComponent(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/theme/ThemeEngine;->mConfigComponent:Ljava/lang/String;

    return-void
.end method

.method public setDeviceComponent(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/theme/ThemeEngine;->mDeviceComponent:Ljava/lang/String;

    return-void
.end method

.method public setDisplayName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/theme/ThemeEngine;->mDisplayName:Ljava/lang/String;

    return-void
.end method

.method public toDebugString()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p0}, Landroid/support/place/theme/ThemeEngine;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ThemeEngine("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/support/place/theme/ThemeEngine;->mDeviceComponent:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/place/theme/ThemeEngine;->mDeviceComponent:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/place/theme/ThemeEngine;->mConfigComponent:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    invoke-virtual {p0, v0}, Landroid/support/place/theme/ThemeEngine;->writeToRpcData(Landroid/support/place/rpc/RpcData;)V

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v0

    array-length v1, v0

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    return-void
.end method

.method public writeToRpcData(Landroid/support/place/rpc/RpcData;)V
    .locals 2

    const-string v0, "displayName"

    iget-object v1, p0, Landroid/support/place/theme/ThemeEngine;->mDisplayName:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "deviceComp"

    iget-object v1, p0, Landroid/support/place/theme/ThemeEngine;->mDeviceComponent:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "configComp"

    iget-object v1, p0, Landroid/support/place/theme/ThemeEngine;->mConfigComponent:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
