.class public Landroid/support/place/rpc/RpcRetrier;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final TAG:Ljava/lang/String; = "RpcRetrier"


# instance fields
.field private mErrorHandler:Landroid/support/place/rpc/RpcErrorHandler;

.field private mHandler:Landroid/os/Handler;

.field private mMaxRetries:I

.field private mName:Ljava/lang/String;

.field private mRetryDelayMs:I

.field private mRunnable:Ljava/lang/Runnable;

.field private mTries:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/support/place/rpc/RpcRetrier$Task;Landroid/os/Handler;II)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/support/place/rpc/RpcRetrier$2;

    invoke-direct {v0, p0}, Landroid/support/place/rpc/RpcRetrier$2;-><init>(Landroid/support/place/rpc/RpcRetrier;)V

    iput-object v0, p0, Landroid/support/place/rpc/RpcRetrier;->mErrorHandler:Landroid/support/place/rpc/RpcErrorHandler;

    iput-object p1, p0, Landroid/support/place/rpc/RpcRetrier;->mName:Ljava/lang/String;

    iput-object p3, p0, Landroid/support/place/rpc/RpcRetrier;->mHandler:Landroid/os/Handler;

    iput p4, p0, Landroid/support/place/rpc/RpcRetrier;->mMaxRetries:I

    iput p5, p0, Landroid/support/place/rpc/RpcRetrier;->mRetryDelayMs:I

    const/4 v0, 0x0

    iput v0, p0, Landroid/support/place/rpc/RpcRetrier;->mTries:I

    new-instance v0, Landroid/support/place/rpc/RpcRetrier$1;

    invoke-direct {v0, p0, p2}, Landroid/support/place/rpc/RpcRetrier$1;-><init>(Landroid/support/place/rpc/RpcRetrier;Landroid/support/place/rpc/RpcRetrier$Task;)V

    iput-object v0, p0, Landroid/support/place/rpc/RpcRetrier;->mRunnable:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Landroid/support/place/rpc/RpcRetrier;)Landroid/support/place/rpc/RpcErrorHandler;
    .locals 1

    iget-object v0, p0, Landroid/support/place/rpc/RpcRetrier;->mErrorHandler:Landroid/support/place/rpc/RpcErrorHandler;

    return-object v0
.end method

.method static synthetic access$100(Landroid/support/place/rpc/RpcRetrier;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/place/rpc/RpcRetrier;->mName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$204(Landroid/support/place/rpc/RpcRetrier;)I
    .locals 1

    iget v0, p0, Landroid/support/place/rpc/RpcRetrier;->mTries:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroid/support/place/rpc/RpcRetrier;->mTries:I

    return v0
.end method

.method static synthetic access$300(Landroid/support/place/rpc/RpcRetrier;)I
    .locals 1

    iget v0, p0, Landroid/support/place/rpc/RpcRetrier;->mMaxRetries:I

    return v0
.end method

.method static synthetic access$400(Landroid/support/place/rpc/RpcRetrier;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Landroid/support/place/rpc/RpcRetrier;->mRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$500(Landroid/support/place/rpc/RpcRetrier;)I
    .locals 1

    iget v0, p0, Landroid/support/place/rpc/RpcRetrier;->mRetryDelayMs:I

    return v0
.end method

.method static synthetic access$600(Landroid/support/place/rpc/RpcRetrier;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Landroid/support/place/rpc/RpcRetrier;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public cancel()V
    .locals 2

    iget-object v0, p0, Landroid/support/place/rpc/RpcRetrier;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/place/rpc/RpcRetrier;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    return-void
.end method

.method public start()V
    .locals 2

    iget-object v0, p0, Landroid/support/place/rpc/RpcRetrier;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/place/rpc/RpcRetrier;->mRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
