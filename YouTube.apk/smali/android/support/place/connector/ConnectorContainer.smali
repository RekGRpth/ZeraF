.class public Landroid/support/place/connector/ConnectorContainer;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mBaseCalled:Z

.field mBrokerConnection:Landroid/support/place/connector/BrokerConnection;

.field mConnectionListener:Landroid/support/place/connector/BrokerConnection$Listener;

.field private final mHandler:Landroid/os/Handler;

.field private mPlace:Landroid/support/place/connector/PlaceInfo;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    const-string v0, "aah.ConnectorContainer"

    iput-object v0, p0, Landroid/support/place/connector/ConnectorContainer;->TAG:Ljava/lang/String;

    new-instance v0, Landroid/support/place/connector/BrokerConnection;

    invoke-direct {v0, p0, p0}, Landroid/support/place/connector/BrokerConnection;-><init>(Landroid/content/Context;Landroid/support/place/connector/ConnectorContainer;)V

    iput-object v0, p0, Landroid/support/place/connector/ConnectorContainer;->mBrokerConnection:Landroid/support/place/connector/BrokerConnection;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Landroid/support/place/connector/ConnectorContainer;->mHandler:Landroid/os/Handler;

    new-instance v0, Landroid/support/place/connector/ConnectorContainer$1;

    invoke-direct {v0, p0}, Landroid/support/place/connector/ConnectorContainer$1;-><init>(Landroid/support/place/connector/ConnectorContainer;)V

    iput-object v0, p0, Landroid/support/place/connector/ConnectorContainer;->mConnectionListener:Landroid/support/place/connector/BrokerConnection$Listener;

    return-void
.end method

.method static synthetic access$002(Landroid/support/place/connector/ConnectorContainer;Z)Z
    .locals 0

    iput-boolean p1, p0, Landroid/support/place/connector/ConnectorContainer;->mBaseCalled:Z

    return p1
.end method


# virtual methods
.method enforceBaseCalled(Ljava/lang/String;)V
    .locals 3

    iget-boolean v0, p0, Landroid/support/place/connector/ConnectorContainer;->mBaseCalled:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call super."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/ConnectorContainer;->mBrokerConnection:Landroid/support/place/connector/BrokerConnection;

    iget-object v0, v0, Landroid/support/place/connector/BrokerConnection;->mBrokerConnection:Landroid/support/place/connector/IBrokerConnection$Stub;

    return-object v0
.end method

.method public onBrokerConnected(Landroid/support/place/connector/Broker;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/place/connector/ConnectorContainer;->mBaseCalled:Z

    return-void
.end method

.method public onBrokerDisconnected()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/place/connector/ConnectorContainer;->mBaseCalled:Z

    return-void
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    iget-object v0, p0, Landroid/support/place/connector/ConnectorContainer;->mBrokerConnection:Landroid/support/place/connector/BrokerConnection;

    iget-object v1, p0, Landroid/support/place/connector/ConnectorContainer;->mConnectionListener:Landroid/support/place/connector/BrokerConnection$Listener;

    invoke-virtual {v0, v1}, Landroid/support/place/connector/BrokerConnection;->connect(Landroid/support/place/connector/BrokerConnection$Listener;)Z

    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    iget-object v0, p0, Landroid/support/place/connector/ConnectorContainer;->mBrokerConnection:Landroid/support/place/connector/BrokerConnection;

    iget-object v1, p0, Landroid/support/place/connector/ConnectorContainer;->mConnectionListener:Landroid/support/place/connector/BrokerConnection$Listener;

    invoke-virtual {v0, v1}, Landroid/support/place/connector/BrokerConnection;->disconnect(Landroid/support/place/connector/BrokerConnection$Listener;)V

    return-void
.end method

.method public onMasterChanged(Landroid/support/place/rpc/EndpointInfo;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/place/connector/ConnectorContainer;->mBaseCalled:Z

    return-void
.end method

.method public onPlaceConnected(Landroid/support/place/connector/PlaceInfo;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/place/connector/ConnectorContainer;->mBaseCalled:Z

    return-void
.end method

.method public onPlaceDisconnected()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/place/connector/ConnectorContainer;->mBaseCalled:Z

    return-void
.end method

.method public onPlaceNameChanged(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/place/connector/ConnectorContainer;->mBaseCalled:Z

    return-void
.end method
