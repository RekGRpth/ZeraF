.class public Landroid/support/place/connector/ConnectorInfo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;
.implements Landroid/support/place/rpc/Flattenable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static KEY_ENDPOINT:Ljava/lang/String;

.field private static KEY_EXTRAS:Ljava/lang/String;

.field private static KEY_TYPE:Ljava/lang/String;

.field public static final RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;


# instance fields
.field private mEndpointInfo:Landroid/support/place/rpc/EndpointInfo;

.field private mExtras:Landroid/support/place/rpc/RpcData;

.field private mType:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "endpoint"

    sput-object v0, Landroid/support/place/connector/ConnectorInfo;->KEY_ENDPOINT:Ljava/lang/String;

    const-string v0, "type"

    sput-object v0, Landroid/support/place/connector/ConnectorInfo;->KEY_TYPE:Ljava/lang/String;

    const-string v0, "extras"

    sput-object v0, Landroid/support/place/connector/ConnectorInfo;->KEY_EXTRAS:Ljava/lang/String;

    new-instance v0, Landroid/support/place/connector/ConnectorInfo$1;

    invoke-direct {v0}, Landroid/support/place/connector/ConnectorInfo$1;-><init>()V

    sput-object v0, Landroid/support/place/connector/ConnectorInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    new-instance v0, Landroid/support/place/connector/ConnectorInfo$2;

    invoke-direct {v0}, Landroid/support/place/connector/ConnectorInfo$2;-><init>()V

    sput-object v0, Landroid/support/place/connector/ConnectorInfo;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    iput-object v0, p0, Landroid/support/place/connector/ConnectorInfo;->mExtras:Landroid/support/place/rpc/RpcData;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    iput-object v0, p0, Landroid/support/place/connector/ConnectorInfo;->mExtras:Landroid/support/place/rpc/RpcData;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v0, v0, [B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readByteArray([B)V

    new-instance v1, Landroid/support/place/rpc/RpcData;

    invoke-direct {v1}, Landroid/support/place/rpc/RpcData;-><init>()V

    invoke-virtual {v1, v0}, Landroid/support/place/rpc/RpcData;->deserialize([B)V

    invoke-virtual {p0, v1}, Landroid/support/place/connector/ConnectorInfo;->readFromRpcData(Landroid/support/place/rpc/RpcData;)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Landroid/support/place/connector/ConnectorInfo$1;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/support/place/connector/ConnectorInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Landroid/support/place/rpc/EndpointInfo;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    iput-object v0, p0, Landroid/support/place/connector/ConnectorInfo;->mExtras:Landroid/support/place/rpc/RpcData;

    iput-object p1, p0, Landroid/support/place/connector/ConnectorInfo;->mEndpointInfo:Landroid/support/place/rpc/EndpointInfo;

    iput-object p2, p0, Landroid/support/place/connector/ConnectorInfo;->mType:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/support/place/rpc/RpcData;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    iput-object v0, p0, Landroid/support/place/connector/ConnectorInfo;->mExtras:Landroid/support/place/rpc/RpcData;

    invoke-virtual {p0, p1}, Landroid/support/place/connector/ConnectorInfo;->readFromRpcData(Landroid/support/place/rpc/RpcData;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    instance-of v0, p1, Landroid/support/place/connector/ConnectorInfo;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    check-cast v0, Landroid/support/place/connector/ConnectorInfo;

    iget-object v3, p0, Landroid/support/place/connector/ConnectorInfo;->mType:Ljava/lang/String;

    if-nez v3, :cond_2

    invoke-virtual {v0}, Landroid/support/place/connector/ConnectorInfo;->getType()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    :cond_2
    iget-object v3, p0, Landroid/support/place/connector/ConnectorInfo;->mType:Ljava/lang/String;

    if-eqz v3, :cond_4

    iget-object v3, p0, Landroid/support/place/connector/ConnectorInfo;->mType:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/support/place/connector/ConnectorInfo;->getType()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    move v0, v2

    :goto_1
    check-cast p1, Landroid/support/place/connector/ConnectorInfo;

    invoke-virtual {p1}, Landroid/support/place/connector/ConnectorInfo;->getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;

    move-result-object v3

    iget-object v4, p0, Landroid/support/place/connector/ConnectorInfo;->mEndpointInfo:Landroid/support/place/rpc/EndpointInfo;

    invoke-virtual {v3, v4}, Landroid/support/place/rpc/EndpointInfo;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    move v1, v2

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public getEndpointInfo()Landroid/support/place/rpc/EndpointInfo;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/ConnectorInfo;->mEndpointInfo:Landroid/support/place/rpc/EndpointInfo;

    return-object v0
.end method

.method public getExtras()Landroid/support/place/rpc/RpcData;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/ConnectorInfo;->mExtras:Landroid/support/place/rpc/RpcData;

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Landroid/support/place/connector/ConnectorInfo;->mType:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Landroid/support/place/connector/ConnectorInfo;->mEndpointInfo:Landroid/support/place/rpc/EndpointInfo;

    invoke-virtual {v0}, Landroid/support/place/rpc/EndpointInfo;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Landroid/support/place/connector/ConnectorInfo;->mType:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p0, Landroid/support/place/connector/ConnectorInfo;->mType:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public readFromRpcData(Landroid/support/place/rpc/RpcData;)V
    .locals 2

    sget-object v0, Landroid/support/place/connector/ConnectorInfo;->KEY_ENDPOINT:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/support/place/rpc/EndpointInfo;

    sget-object v1, Landroid/support/place/connector/ConnectorInfo;->KEY_ENDPOINT:Ljava/lang/String;

    invoke-virtual {p1, v1}, Landroid/support/place/rpc/RpcData;->getRpcData(Ljava/lang/String;)Landroid/support/place/rpc/RpcData;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/place/rpc/EndpointInfo;-><init>(Landroid/support/place/rpc/RpcData;)V

    iput-object v0, p0, Landroid/support/place/connector/ConnectorInfo;->mEndpointInfo:Landroid/support/place/rpc/EndpointInfo;

    :goto_0
    sget-object v0, Landroid/support/place/connector/ConnectorInfo;->KEY_TYPE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/connector/ConnectorInfo;->mType:Ljava/lang/String;

    sget-object v0, Landroid/support/place/connector/ConnectorInfo;->KEY_EXTRAS:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/support/place/rpc/RpcData;->getRpcData(Ljava/lang/String;)Landroid/support/place/rpc/RpcData;

    move-result-object v0

    iput-object v0, p0, Landroid/support/place/connector/ConnectorInfo;->mExtras:Landroid/support/place/rpc/RpcData;

    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/place/connector/ConnectorInfo;->mEndpointInfo:Landroid/support/place/rpc/EndpointInfo;

    goto :goto_0
.end method

.method public setType(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Landroid/support/place/connector/ConnectorInfo;->mType:Ljava/lang/String;

    return-void
.end method

.method public toDebugString()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p0}, Landroid/support/place/connector/ConnectorInfo;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ConnectorInfo(endpoint="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Landroid/support/place/connector/ConnectorInfo;->mEndpointInfo:Landroid/support/place/rpc/EndpointInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/place/connector/ConnectorInfo;->mType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    invoke-virtual {p0, v0}, Landroid/support/place/connector/ConnectorInfo;->writeToRpcData(Landroid/support/place/rpc/RpcData;)V

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v0

    array-length v1, v0

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    return-void
.end method

.method public writeToRpcData(Landroid/support/place/rpc/RpcData;)V
    .locals 2

    iget-object v0, p0, Landroid/support/place/connector/ConnectorInfo;->mEndpointInfo:Landroid/support/place/rpc/EndpointInfo;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    iget-object v1, p0, Landroid/support/place/connector/ConnectorInfo;->mEndpointInfo:Landroid/support/place/rpc/EndpointInfo;

    invoke-virtual {v1, v0}, Landroid/support/place/rpc/EndpointInfo;->writeToRpcData(Landroid/support/place/rpc/RpcData;)V

    sget-object v1, Landroid/support/place/connector/ConnectorInfo;->KEY_ENDPOINT:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/support/place/rpc/RpcData;->putRpcData(Ljava/lang/String;Landroid/support/place/rpc/RpcData;)V

    :cond_0
    sget-object v0, Landroid/support/place/connector/ConnectorInfo;->KEY_TYPE:Ljava/lang/String;

    iget-object v1, p0, Landroid/support/place/connector/ConnectorInfo;->mType:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Landroid/support/place/connector/ConnectorInfo;->KEY_EXTRAS:Ljava/lang/String;

    iget-object v1, p0, Landroid/support/place/connector/ConnectorInfo;->mExtras:Landroid/support/place/rpc/RpcData;

    invoke-virtual {p1, v0, v1}, Landroid/support/place/rpc/RpcData;->putRpcData(Ljava/lang/String;Landroid/support/place/rpc/RpcData;)V

    return-void
.end method
