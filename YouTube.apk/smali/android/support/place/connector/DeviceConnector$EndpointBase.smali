.class public abstract Landroid/support/place/connector/DeviceConnector$EndpointBase;
.super Landroid/support/place/connector/Connector;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/place/connector/Broker;Landroid/support/place/connector/PlaceInfo;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/support/place/connector/Connector;-><init>(Landroid/content/Context;Landroid/support/place/connector/Broker;Landroid/support/place/connector/PlaceInfo;)V

    return-void
.end method


# virtual methods
.method public abstract factoryReset(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)Z
.end method

.method public abstract getAdbState(Landroid/support/place/rpc/RpcContext;)Z
.end method

.method public abstract getAvailableUpdate(Landroid/support/place/rpc/RpcContext;)Ljava/lang/String;
.end method

.method public abstract getBluetoothMac(Landroid/support/place/rpc/RpcContext;)Ljava/lang/String;
.end method

.method public abstract getBuildVersion(Landroid/support/place/rpc/RpcContext;)Ljava/lang/String;
.end method

.method public abstract getDebugInfo(Landroid/support/place/rpc/RpcContext;)Ljava/lang/String;
.end method

.method public abstract getDeviceName(Landroid/support/place/rpc/RpcContext;)Ljava/lang/String;
.end method

.method public abstract getDeviceSerialNumber(Landroid/support/place/rpc/RpcContext;)Ljava/lang/String;
.end method

.method public abstract getDeviceState(Landroid/support/place/rpc/RpcContext;)Landroid/support/place/rpc/RpcData;
.end method

.method public abstract getDeviceVersion(Landroid/support/place/rpc/RpcContext;)Ljava/lang/String;
.end method

.method public abstract getLegalInfo(Landroid/support/place/rpc/RpcContext;)V
.end method

.method public abstract getManufacturerName(Landroid/support/place/rpc/RpcContext;)Ljava/lang/String;
.end method

.method public abstract getMaster(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)Landroid/support/place/rpc/EndpointInfo;
.end method

.method public abstract getModelName(Landroid/support/place/rpc/RpcContext;)Ljava/lang/String;
.end method

.method public abstract getUpdateWindow(Landroid/support/place/rpc/RpcContext;)Ljava/lang/String;
.end method

.method public abstract goodbye(Landroid/support/place/rpc/EndpointInfo;Landroid/support/place/rpc/RpcContext;)V
.end method

.method public abstract helloFromHub(Landroid/support/place/rpc/EndpointInfo;Landroid/support/place/rpc/RpcContext;)Landroid/support/place/rpc/RpcData;
.end method

.method public abstract ping(Landroid/support/place/rpc/RpcContext;)V
.end method

.method public process(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;)[B
    .locals 4

    const/4 v1, 0x0

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0, p2}, Landroid/support/place/rpc/RpcData;-><init>([B)V

    const-string v2, "helloFromHub"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "hub"

    sget-object v3, Landroid/support/place/rpc/EndpointInfo;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    invoke-virtual {v0, v2, v3}, Landroid/support/place/rpc/RpcData;->getFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;

    move-result-object v0

    check-cast v0, Landroid/support/place/rpc/EndpointInfo;

    invoke-virtual {p0, v0, p3}, Landroid/support/place/connector/DeviceConnector$EndpointBase;->helloFromHub(Landroid/support/place/rpc/EndpointInfo;Landroid/support/place/rpc/RpcContext;)Landroid/support/place/rpc/RpcData;

    move-result-object v2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v0, v3, v2}, Landroid/support/place/rpc/RpcData;->putRpcData(Ljava/lang/String;Landroid/support/place/rpc/RpcData;)V

    :goto_0
    if-eqz v0, :cond_15

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    const-string v2, "ping"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0, p3}, Landroid/support/place/connector/DeviceConnector$EndpointBase;->ping(Landroid/support/place/rpc/RpcContext;)V

    move-object v0, v1

    goto :goto_0

    :cond_1
    const-string v2, "goodbye"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "hub"

    sget-object v3, Landroid/support/place/rpc/EndpointInfo;->RPC_CREATOR:Landroid/support/place/rpc/Flattenable$Creator;

    invoke-virtual {v0, v2, v3}, Landroid/support/place/rpc/RpcData;->getFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable$Creator;)Landroid/support/place/rpc/Flattenable;

    move-result-object v0

    check-cast v0, Landroid/support/place/rpc/EndpointInfo;

    invoke-virtual {p0, v0, p3}, Landroid/support/place/connector/DeviceConnector$EndpointBase;->goodbye(Landroid/support/place/rpc/EndpointInfo;Landroid/support/place/rpc/RpcContext;)V

    move-object v0, v1

    goto :goto_0

    :cond_2
    const-string v2, "getMaster"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "placeId"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p3}, Landroid/support/place/connector/DeviceConnector$EndpointBase;->getMaster(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)Landroid/support/place/rpc/EndpointInfo;

    move-result-object v2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v0, v3, v2}, Landroid/support/place/rpc/RpcData;->putFlattenable(Ljava/lang/String;Landroid/support/place/rpc/Flattenable;)V

    goto :goto_0

    :cond_3
    const-string v2, "getBluetoothMac"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0, p3}, Landroid/support/place/connector/DeviceConnector$EndpointBase;->getBluetoothMac(Landroid/support/place/rpc/RpcContext;)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v0, v3, v2}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v2, "setDeviceName"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "name"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p3}, Landroid/support/place/connector/DeviceConnector$EndpointBase;->setDeviceName(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)Z

    move-result v2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v0, v3, v2}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_5
    const-string v2, "getDeviceName"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {p0, p3}, Landroid/support/place/connector/DeviceConnector$EndpointBase;->getDeviceName(Landroid/support/place/rpc/RpcContext;)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v0, v3, v2}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    const-string v2, "getDeviceSerialNumber"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {p0, p3}, Landroid/support/place/connector/DeviceConnector$EndpointBase;->getDeviceSerialNumber(Landroid/support/place/rpc/RpcContext;)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v0, v3, v2}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    const-string v2, "getDeviceVersion"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {p0, p3}, Landroid/support/place/connector/DeviceConnector$EndpointBase;->getDeviceVersion(Landroid/support/place/rpc/RpcContext;)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v0, v3, v2}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    const-string v2, "getBuildVersion"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {p0, p3}, Landroid/support/place/connector/DeviceConnector$EndpointBase;->getBuildVersion(Landroid/support/place/rpc/RpcContext;)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v0, v3, v2}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    const-string v2, "getAvailableUpdate"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-virtual {p0, p3}, Landroid/support/place/connector/DeviceConnector$EndpointBase;->getAvailableUpdate(Landroid/support/place/rpc/RpcContext;)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v0, v3, v2}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    const-string v2, "getDebugInfo"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual {p0, p3}, Landroid/support/place/connector/DeviceConnector$EndpointBase;->getDebugInfo(Landroid/support/place/rpc/RpcContext;)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v0, v3, v2}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    const-string v2, "setAdbState"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    const-string v2, "adbEnabled"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0, p3}, Landroid/support/place/connector/DeviceConnector$EndpointBase;->setAdbState(ZLandroid/support/place/rpc/RpcContext;)Z

    move-result v2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v0, v3, v2}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_c
    const-string v2, "getAdbState"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-virtual {p0, p3}, Landroid/support/place/connector/DeviceConnector$EndpointBase;->getAdbState(Landroid/support/place/rpc/RpcContext;)Z

    move-result v2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v0, v3, v2}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_d
    const-string v2, "setUpdateWindow"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    const-string v2, "window"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p3}, Landroid/support/place/connector/DeviceConnector$EndpointBase;->setUpdateWindow(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)Z

    move-result v2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v0, v3, v2}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_e
    const-string v2, "getUpdateWindow"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-virtual {p0, p3}, Landroid/support/place/connector/DeviceConnector$EndpointBase;->getUpdateWindow(Landroid/support/place/rpc/RpcContext;)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v0, v3, v2}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_f
    const-string v2, "getDeviceState"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-virtual {p0, p3}, Landroid/support/place/connector/DeviceConnector$EndpointBase;->getDeviceState(Landroid/support/place/rpc/RpcContext;)Landroid/support/place/rpc/RpcData;

    move-result-object v2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v0, v3, v2}, Landroid/support/place/rpc/RpcData;->putRpcData(Ljava/lang/String;Landroid/support/place/rpc/RpcData;)V

    goto/16 :goto_0

    :cond_10
    const-string v2, "getManufacturerName"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-virtual {p0, p3}, Landroid/support/place/connector/DeviceConnector$EndpointBase;->getManufacturerName(Landroid/support/place/rpc/RpcContext;)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v0, v3, v2}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_11
    const-string v2, "getModelName"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-virtual {p0, p3}, Landroid/support/place/connector/DeviceConnector$EndpointBase;->getModelName(Landroid/support/place/rpc/RpcContext;)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v0, v3, v2}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_12
    const-string v2, "getLegalInfo"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    invoke-virtual {p0, p3}, Landroid/support/place/connector/DeviceConnector$EndpointBase;->getLegalInfo(Landroid/support/place/rpc/RpcContext;)V

    move-object v0, v1

    goto/16 :goto_0

    :cond_13
    const-string v2, "factoryReset"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    const-string v2, "confirmation"

    invoke-virtual {v0, v2}, Landroid/support/place/rpc/RpcData;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p3}, Landroid/support/place/connector/DeviceConnector$EndpointBase;->factoryReset(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)Z

    move-result v2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v3, "_result"

    invoke-virtual {v0, v3, v2}, Landroid/support/place/rpc/RpcData;->putBoolean(Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_14
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/place/connector/Connector;->process(Ljava/lang/String;[BLandroid/support/place/rpc/RpcContext;Landroid/support/place/rpc/RpcError;)[B

    move-result-object v0

    goto/16 :goto_1

    :cond_15
    move-object v0, v1

    goto/16 :goto_1
.end method

.method public pushOnDeviceNameChanged(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v1, "name"

    invoke-virtual {v0, v1, p1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "onDeviceNameChanged"

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Landroid/support/place/connector/DeviceConnector$EndpointBase;->pushEvent(Ljava/lang/String;[B)V

    return-void
.end method

.method public pushOnLegalInformationReady(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Landroid/support/place/rpc/RpcData;

    invoke-direct {v0}, Landroid/support/place/rpc/RpcData;-><init>()V

    const-string v1, "info"

    invoke-virtual {v0, v1, p1}, Landroid/support/place/rpc/RpcData;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "onLegalInformationReady"

    invoke-virtual {v0}, Landroid/support/place/rpc/RpcData;->serialize()[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Landroid/support/place/connector/DeviceConnector$EndpointBase;->pushEvent(Ljava/lang/String;[B)V

    return-void
.end method

.method public abstract setAdbState(ZLandroid/support/place/rpc/RpcContext;)Z
.end method

.method public abstract setDeviceName(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)Z
.end method

.method public abstract setUpdateWindow(Ljava/lang/String;Landroid/support/place/rpc/RpcContext;)Z
.end method
